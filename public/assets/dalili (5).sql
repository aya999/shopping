-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2019 at 12:22 PM
-- Server version: 5.7.22-0ubuntu0.17.10.1
-- PHP Version: 7.2.7-1+ubuntu17.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dalili`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/accounts/default.png',
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `adds` int(11) NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_countries`
--

CREATE TABLE `account_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_users`
--

CREATE TABLE `account_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `usage` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `adds`
--

CREATE TABLE `adds` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_android` tinyint(1) NOT NULL DEFAULT '0',
  `is_ios` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `page` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `mobile`, `email`, `password`, `is_active`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dalili Shop Admin', '1111', 'admin@dalili.com', '$2y$10$7MaZApSOGAosOES9JRUP0e0ChkN6f6lf6y7S8t01iieehapLTQmFm', 1, 1, 'd8DduDQFgJRdI2Ty7iSXrCHsBy8X9giITZxbwSduWC1G3G2DP3yWAdxuh2dg', '2019-01-13 14:01:51', '2019-01-13 14:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `admin_countries`
--

CREATE TABLE `admin_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_countries`
--

INSERT INTO `admin_countries` (`id`, `admin_id`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-01-12 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `apis`
--

CREATE TABLE `apis` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `required_parameters` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `and_is_active` tinyint(1) NOT NULL,
  `ios_is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apis`
--

INSERT INTO `apis` (`id`, `en_name`, `ar_name`, `method`, `path`, `ar_desc`, `en_desc`, `required_parameters`, `parameters`, `and_is_active`, `ios_is_active`, `created_at`, `updated_at`) VALUES
(1, 'Registration', 'تسجيل الاشتراك', 'POST', '/register', 'تسجيل المستخد للاشتراك', 'User make register in our application', 'name-email-password-country_code-mobile', 'lang-image-bg-address-lat-long-birth_date-facebook-twitter-google-gender', 1, 1, '2019-01-13 15:28:49', '2019-01-13 15:28:49'),
(2, 'Login', 'تسجيل الدخول', 'POST', '/login', 'تسجيل الدخول', 'User make Login in our application', 'email(can be email or mobile)-password', 'lang', 1, 1, '2019-01-13 15:28:50', '2019-01-13 15:28:50'),
(3, 'Social Login', 'تسجيل الدخول بمواقع التواصل', 'POST', '/social-login', 'تسجيل دخول المستخدم باستجدام جوجل او الفيس بوك', 'User make Login with (facebook or google) in our application', 'provider(can be \"facebook\" or \"google\") - provider_id', 'lang-provider_image (If login with google) ', 1, 1, '2019-01-13 15:28:50', '2019-01-13 15:28:50'),
(4, 'Account activation', 'تفعيل الحساب', 'POST', '/active-account', 'تاكيد ملكيه رقم الهاتف ', 'User confirm his mobile number by code that send to him as sms', 'email(can be email or mobile)-verification_code', 'lang', 1, 1, '2019-01-13 15:28:50', '2019-01-13 15:28:50'),
(5, 'User profile ', 'صفحه المستخدم', 'POST', '/profile', 'صفحة المستخدم الشخصية', 'User profile ', 'user_id', 'lang', 1, 1, '2019-01-13 15:28:51', '2019-01-13 15:28:51'),
(6, 'Update profile ', 'تعديل الحساب', 'POST', '/profile/update', 'تعديل بيانات الحساب الخاصه بالمستخدم', 'Update Profile', 'user_id', 'lang-name-email-password-country_code-mobile-,\'image\',\'bg\',\'address\',\'lat\',\'long\',\'facebook\',\'twitter\',\'instagram\',\'google\',\'gender\',', 1, 1, '2019-01-13 15:28:52', '2019-01-13 15:28:52'),
(7, 'Countries', 'البلدان', 'GET', '/countries', 'قائمه البلدان', 'List of countries', 'user_id', 'lang', 1, 1, '2019-01-13 15:28:52', '2019-01-13 15:28:52'),
(8, 'Categories', 'الاقسام', 'GET', '/categories', 'قائمه بالاقسام الرئيسية والفرعية', 'List of Parent categories with 2 level children', '', 'lang', 1, 1, '2019-01-13 15:28:52', '2019-01-13 15:28:52'),
(9, 'Home Categories', 'الاقسام الرئيسيه', 'GET', '/home-categories', 'قائمة بالاقسام الرئيسيه بمنتجاتها', 'List of Parent categories with 1 with his products to 3 level', '', 'lang', 1, 1, '2019-01-13 15:28:53', '2019-01-13 15:28:53');

-- --------------------------------------------------------

--
-- Table structure for table `balances`
--

CREATE TABLE `balances` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `balance_countries`
--

CREATE TABLE `balance_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `balance_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `en_name` text COLLATE utf8mb4_unicode_ci,
  `ar_name` text COLLATE utf8mb4_unicode_ci,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `image`, `en_name`, `ar_name`, `en_meta_desc`, `ar_meta_desc`, `en_meta_title`, `ar_meta_title`, `en_meta_tags`, `ar_meta_tags`, `is_active`, `category_id`, `created_at`, `updated_at`) VALUES
(1, '1547456435.jpg', 'Lenovo', 'لينوفو', NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2019-01-14 07:00:35', '2019-01-14 07:09:26'),
(2, '1547456466.png', 'Infinx', 'انفانكس', NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2019-01-14 07:01:06', '2019-01-14 07:01:06'),
(3, '1547456523.jpg', 'Calvin Clein', 'Calvin Clein', NULL, NULL, NULL, NULL, NULL, NULL, 0, 6, '2019-01-14 07:02:03', '2019-01-14 07:02:03'),
(4, '1547456576.jpg', 'Sharp', 'Sharp', NULL, NULL, NULL, NULL, NULL, NULL, 0, 11, '2019-01-14 07:02:56', '2019-01-14 07:02:56'),
(5, '1547456993.png', 'Iphone', 'Iphone', NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2019-01-14 07:09:53', '2019-01-14 07:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/categories/default.png',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/categories/defaultIcon.png',
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `is_color` tinyint(1) NOT NULL DEFAULT '0',
  `is_size` tinyint(1) NOT NULL DEFAULT '0',
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `image`, `icon`, `en_name`, `ar_name`, `en_desc`, `ar_desc`, `en_meta_desc`, `ar_meta_desc`, `en_meta_title`, `ar_meta_title`, `en_meta_tags`, `ar_meta_tags`, `is_online`, `is_color`, `is_size`, `is_home`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, '1547401714.JPG', '1547401714.png', 'Cars', 'مركبات', 'At mollit elit perspiciatis enim velit at et non qui aliquam qui sit', 'Autem cupidatat omnis ut consequatur rerum non inventore numquam veritatis in illo architecto voluptate elit', 'Dolor mollitia quidem dolorum quibusdam quam minim sint ex magna commodi fugiat natus eu', 'Sequi alias repellendus Autem veniam provident culpa officia dolor voluptates', 'Ullamco deserunt dolor cumque odio neque', 'Sunt consequuntur cum pariatur Mollitia inventore duis qui non dolore inventore illo qui aspernatur quis occaecat voluptatem ea sint', 'Saepe fugit aspernatur qui in reprehenderit consequatur Dolor velit consequuntur', 'Illum quas alias assumenda quia ullamco ut fugiat do cumque', 0, 0, 0, 1, NULL, '2019-01-13 15:48:34', '2019-01-13 15:48:34'),
(2, '1547401824.jpg', '1547401824.png', 'Mobils', 'موبيلات', 'Facilis est sed minim dolore ut tempore', 'Aute voluptatem aliqua Repudiandae modi quia delectus enim libero ullam', 'Id nesciunt mollitia doloribus dolorem quis cillum labore enim distinctio Pariatur Accusantium enim minus sunt quia sed vel occaecat ipsam', 'Recusandae Consequatur at laborum Atque ut anim accusamus id non accusamus', 'Nisi magnam dolores saepe tempor illum dolore quidem nesciunt alias', 'A unde et aliquip et natus pariatur Placeat earum dicta laboriosam nobis velit', 'Sit et deserunt aute amet dolorem corrupti ullamco maiores soluta et magna eos', 'Nihil esse cillum molestiae magnam voluptate nesciunt fugiat maiores Nam molestiae enim ad voluptatum itaque ut eius delectus', 0, 0, 0, 1, 3, '2019-01-13 15:50:24', '2019-01-13 18:55:51'),
(3, '1547401946.jpg', '1547401946.png', 'Electronics', 'الالكترونيات', 'Officia labore aliquip dicta aspernatur incididunt vitae qui', 'Quidem architecto rerum quis aut ratione ad aute odio', 'Exercitationem modi enim sit non dolorem proident', 'Nobis recusandae Molestias et fugiat do itaque', 'Proident quasi distinctio Nisi fugit voluptatum dolore esse est quas nulla amet', 'Ipsum eum quae commodi est officiis deserunt nemo esse error sit rerum illo quibusdam quo', 'Enim accusantium ea ut quisquam', 'Placeat magnam aperiam expedita laborum aperiam', 0, 0, 0, 0, NULL, '2019-01-13 15:52:26', '2019-01-13 15:52:26'),
(4, '1547402113.jpg', '1547402113.jpg', 'furniture', 'اثاث', 'Quo elit dolores corporis veritatis qui non alias eligendi', 'Nihil dolor illum provident quasi aliquam alias fugiat et expedita', 'Sit sed officia quia sit laboriosam neque qui ea et et est quaerat ut', 'Omnis sit exercitation eos sunt quis soluta atque iste voluptates', 'Tempora autem esse aut fugiat accusantium anim atque quia officia quod incidunt ipsa dolore et velit fuga', 'Error praesentium qui sit repudiandae deserunt possimus inventore consequat Labore rem adipisicing', 'Consequatur Quo voluptates in odit', 'Id maiores est ea et sed aut excepturi consequat Veniam', 1, 0, 0, 0, NULL, '2019-01-13 15:55:13', '2019-01-13 15:55:13'),
(5, '1547403128.jpg', '1547403129.png', 'tablet', 'تابلت', 'Officia autem qui hic temporibus sit', 'Eu unde sed laborum Ex voluptas anim amet nisi beatae nemo aliquip necessitatibus porro', 'Architecto dolores enim veritatis qui aliquip consequatur aute quibusdam incidunt tempora odit totam eveniet iste necessitatibus est mollitia ab consequatur', 'Et reprehenderit alias nostrum enim eos voluptatibus aliquip qui praesentium quia', 'Repudiandae hic debitis mollitia autem quibusdam est labore qui et sunt deleniti amet', 'Ullam dolor et voluptatem aliquip ipsam voluptates quisquam laboriosam quod aut laborum Quidem', 'Fugit ullam beatae omnis nostrud rerum omnis nihil quo laborum iure vero temporibus', 'Numquam ratione enim dolor ullamco qui et architecto voluptas obcaecati eos quis delectus doloribus at est irure quia eum', 1, 1, 0, 1, 2, '2019-01-13 16:12:09', '2019-01-13 19:00:38'),
(6, '1547413638.jpg', '1547455526.png', 'Fashsion', 'موضة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, '2019-01-13 19:07:19', '2019-01-14 06:45:26'),
(7, '1547455582.jpeg', '1547455582.png', 'Gifts & Toys', 'لعب وهدايا', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2019-01-14 06:46:22', '2019-01-14 06:46:22'),
(8, '1547455661.jpg', '1547455661.png', 'Furneture', 'اثاث منزلي', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2019-01-14 06:47:41', '2019-01-14 06:47:41'),
(9, '1547455750.jpg', '1547455750.png', 'Real Estate', 'عقارات للبيع', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, '2019-01-14 06:49:10', '2019-01-14 06:49:10'),
(10, '1547455939.jpg', '1547455939.png', 'Foods', 'صحة وطعام', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2019-01-14 06:52:20', '2019-01-14 06:52:20'),
(11, '1547456002.jpg', '1547456002.png', 'Appliances', 'أجهزة منزلية', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2019-01-14 06:53:22', '2019-01-14 06:53:22'),
(12, '1547456157.jpg', '1547456157.png', 'Supermarket', 'سوبر ماركت', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2019-01-14 06:55:57', '2019-01-14 06:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `category_properties`
--

CREATE TABLE `category_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` text COLLATE utf8mb4_unicode_ci,
  `ar_name` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_properties`
--

INSERT INTO `category_properties` (`id`, `en_name`, `ar_name`, `category_id`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 'حجم الشاشة', 'screen size', 5, 2, '2019-01-13 19:00:38', '2019-01-13 19:00:38'),
(2, 'الذاكرة الموقتته', 'Memory', 5, 2, '2019-01-13 19:00:38', '2019-01-13 19:00:38'),
(3, 'الذاكرة الداخليه', 'Storage', 5, 1, '2019-01-13 19:00:38', '2019-01-13 19:00:38'),
(4, 'الكاميرا', 'Cameta', 5, 1, '2019-01-13 19:00:39', '2019-01-13 19:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `category_property_options`
--

CREATE TABLE `category_property_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `seen_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/cites/default.png',
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `en_name`, `ar_name`, `image`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'cairo', 'القاهره', 'assets/images/cites/default.png', 1, '2019-01-12 22:00:00', NULL),
(2, 'Giza', 'الجيزة', 'assets/images/cites/default.png', 1, '2019-01-13 18:51:54', '2019-01-13 18:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `en_currency_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_currency_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_symbol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_price` double NOT NULL DEFAULT '0',
  `max_free_adds` double NOT NULL DEFAULT '0',
  `max_free_points` double NOT NULL DEFAULT '0',
  `en_title` text COLLATE utf8mb4_unicode_ci,
  `ar_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `is_online_users` tinyint(1) NOT NULL DEFAULT '0',
  `is_cash_users` tinyint(1) NOT NULL DEFAULT '0',
  `is_online_store` tinyint(1) NOT NULL DEFAULT '0',
  `is_cash_store` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `en_name`, `ar_name`, `code`, `postal_code`, `image`, `en_currency_name`, `ar_currency_name`, `currency_code`, `currency_symbol`, `points`, `lang`, `shipping_price`, `max_free_adds`, `max_free_points`, `en_title`, `ar_title`, `en_meta_desc`, `ar_meta_desc`, `en_meta_title`, `ar_meta_title`, `en_meta_tags`, `ar_meta_tags`, `is_online_users`, `is_cash_users`, `is_online_store`, `is_cash_store`, `created_at`, `updated_at`) VALUES
(1, 'Egypt', 'مصر', 'eg', '123456', 'default.png', 'pound', 'جنيه', '1', 'LE', 3, 'ar', 0, 0, 2, 'A', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 0, 0, 0, 0, '2019-01-13 14:27:56', '2019-01-13 14:27:56'),
(2, 'Saudi Arabian', 'المملكه السعوديه', 'sa', '056', 'default.png', 'Ryak', 'ريال سعودى', 'RS', 'RS', 400, 'en', 0, 0, 500, 'Eiusmod voluptatem impedit nostrum voluptate magni nemo ratione eum lorem accusantium dolore quasi ab ut est ut libero', 'Autem ut exercitation eius vitae exercitation fugit aute est veniam ut', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, '2019-01-13 18:54:30', '2019-01-13 18:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `free_adds`
--

CREATE TABLE `free_adds` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` int(11) NOT NULL,
  `object_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `user_type`, `action`, `object_id`, `object_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'added_country', 1, 'countries', '2019-01-13 14:27:56', '2019-01-13 14:27:56'),
(2, 1, 'admin', 'added_category', 1, 'categories', '2019-01-13 15:48:34', '2019-01-13 15:48:34'),
(3, 1, 'admin', 'added_category', 2, 'categories', '2019-01-13 15:50:24', '2019-01-13 15:50:24'),
(4, 1, 'admin', 'added_category', 3, 'categories', '2019-01-13 15:52:27', '2019-01-13 15:52:27'),
(5, 1, 'admin', 'added_category', 4, 'categories', '2019-01-13 15:55:14', '2019-01-13 15:55:14'),
(6, 1, 'admin', 'added_category', 5, 'categories', '2019-01-13 16:12:09', '2019-01-13 16:12:09'),
(7, 1, 'admin', 'added_city', 2, 'cities', '2019-01-13 18:51:54', '2019-01-13 18:51:54'),
(8, 1, 'admin', 'added_country', 2, 'countries', '2019-01-13 18:54:30', '2019-01-13 18:54:30'),
(9, 1, 'admin', 'updated_category', 2, 'categories', '2019-01-13 18:55:51', '2019-01-13 18:55:51'),
(10, 1, 'admin', 'updated_category', 5, 'categories', '2019-01-13 19:00:39', '2019-01-13 19:00:39'),
(11, 1, 'admin', 'added_category', 6, 'categories', '2019-01-13 19:07:19', '2019-01-13 19:07:19'),
(12, 1, 'admin', 'added_store', 1, 'stores', '2019-01-13 19:48:47', '2019-01-13 19:48:47'),
(13, 1, 'admin', 'updated_category', 6, 'categories', '2019-01-14 06:45:26', '2019-01-14 06:45:26'),
(14, 1, 'admin', 'added_category', 7, 'categories', '2019-01-14 06:46:22', '2019-01-14 06:46:22'),
(15, 1, 'admin', 'added_category', 8, 'categories', '2019-01-14 06:47:41', '2019-01-14 06:47:41'),
(16, 1, 'admin', 'added_category', 9, 'categories', '2019-01-14 06:49:10', '2019-01-14 06:49:10'),
(17, 1, 'admin', 'added_slider', 1, 'sliders', '2019-01-14 06:50:30', '2019-01-14 06:50:30'),
(18, 1, 'admin', 'added_category', 10, 'categories', '2019-01-14 06:52:20', '2019-01-14 06:52:20'),
(19, 1, 'admin', 'added_category', 11, 'categories', '2019-01-14 06:53:22', '2019-01-14 06:53:22'),
(20, 1, 'admin', 'added_category', 12, 'categories', '2019-01-14 06:55:57', '2019-01-14 06:55:57'),
(21, 1, 'admin', 'added_brand', 1, 'brands', '2019-01-14 07:00:35', '2019-01-14 07:00:35'),
(22, 1, 'admin', 'added_brand', 2, 'brands', '2019-01-14 07:01:06', '2019-01-14 07:01:06'),
(23, 1, 'admin', 'added_brand', 3, 'brands', '2019-01-14 07:02:03', '2019-01-14 07:02:03'),
(24, 1, 'admin', 'added_brand', 4, 'brands', '2019-01-14 07:02:56', '2019-01-14 07:02:56'),
(25, 1, 'admin', 'added_product', 1, 'products', '2019-01-14 07:04:59', '2019-01-14 07:04:59'),
(26, 1, 'admin', 'updated_product', 1, 'products', '2019-01-14 07:05:11', '2019-01-14 07:05:11'),
(27, 1, 'admin', 'add_Brand', 1, 'brands', '2019-01-14 07:09:22', '2019-01-14 07:09:22'),
(28, 1, 'admin', 'updated_brand', 1, 'brands', '2019-01-14 07:09:26', '2019-01-14 07:09:26'),
(29, 1, 'admin', 'add_Brand', 2, 'brands', '2019-01-14 07:09:28', '2019-01-14 07:09:28'),
(30, 1, 'admin', 'updated_brand', 2, 'brands', '2019-01-14 07:09:30', '2019-01-14 07:09:30'),
(31, 1, 'admin', 'added_brand', 5, 'brands', '2019-01-14 07:09:53', '2019-01-14 07:09:53'),
(32, 1, 'admin', 'add_Brand', 1, 'brands', '2019-01-14 07:11:09', '2019-01-14 07:11:09'),
(33, 1, 'admin', 'added_product', 2, 'products', '2019-01-14 07:11:56', '2019-01-14 07:11:56'),
(34, 1, 'admin', 'updated_product', 2, 'products', '2019-01-14 07:12:09', '2019-01-14 07:12:09'),
(35, 1, 'admin', 'updated_product', 1, 'products', '2019-01-14 07:12:26', '2019-01-14 07:12:26'),
(36, 1, 'admin', 'added_product', 3, 'products', '2019-01-14 07:13:24', '2019-01-14 07:13:24'),
(37, 1, 'admin', 'added_product', 4, 'products', '2019-01-14 07:15:00', '2019-01-14 07:15:00'),
(38, 1, 'admin', 'updated_product', 4, 'products', '2019-01-14 07:15:07', '2019-01-14 07:15:07'),
(39, 1, 'admin', 'added_product', 5, 'products', '2019-01-14 07:16:21', '2019-01-14 07:16:21'),
(40, 1, 'admin', 'updated_product', 5, 'products', '2019-01-14 07:16:31', '2019-01-14 07:16:31'),
(41, 1, 'admin', 'added_product', 6, 'products', '2019-01-14 07:21:24', '2019-01-14 07:21:24'),
(42, 1, 'admin', 'updated_product', 6, 'products', '2019-01-14 07:21:49', '2019-01-14 07:21:49'),
(43, 1, 'admin', 'added_product', 7, 'products', '2019-01-14 07:23:44', '2019-01-14 07:23:44'),
(44, 1, 'admin', 'updated_product', 7, 'products', '2019-01-14 07:23:53', '2019-01-14 07:23:53'),
(45, 1, 'admin', 'added_product', 8, 'products', '2019-01-14 07:26:56', '2019-01-14 07:26:56'),
(46, 1, 'admin', 'updated_product', 8, 'products', '2019-01-14 07:27:05', '2019-01-14 07:27:05'),
(47, 1, 'admin', 'added_product', 9, 'products', '2019-01-14 07:27:52', '2019-01-14 07:27:52'),
(48, 1, 'admin', 'added_product', 10, 'products', '2019-01-14 07:29:47', '2019-01-14 07:29:47'),
(49, 1, 'admin', 'added_user', 2, 'users', '2019-01-14 07:31:24', '2019-01-14 07:31:24'),
(50, 1, 'admin', 'added_product', 11, 'products', '2019-01-14 07:32:36', '2019-01-14 07:32:36'),
(51, 1, 'admin', 'updated_setting', 1, 'setting', '2019-01-14 07:45:33', '2019-01-14 07:45:33'),
(52, 1, 'admin', 'updated_setting', 1, 'setting', '2019-01-14 07:45:44', '2019-01-14 07:45:44'),
(53, 1, 'admin', 'updated_setting', 1, 'setting', '2019-01-14 07:45:52', '2019-01-14 07:45:52'),
(54, 1, 'admin', 'updated_setting', 1, 'setting', '2019-01-14 07:47:51', '2019-01-14 07:47:51'),
(55, 1, 'admin', 'updated_setting', 1, 'setting', '2019-01-14 07:48:22', '2019-01-14 07:48:22'),
(56, 1, 'admin', 'added_package', 1, 'packages', '2019-01-14 07:50:56', '2019-01-14 07:50:56'),
(57, 1, 'admin', 'added_package', 2, 'packages', '2019-01-14 08:02:45', '2019-01-14 08:02:45'),
(58, 1, 'admin', 'added_packagetype', 1, 'package_types', '2019-01-14 08:02:53', '2019-01-14 08:02:53'),
(59, 1, 'admin', 'added_packagetype', 2, 'package_types', '2019-01-14 08:02:59', '2019-01-14 08:02:59'),
(60, 1, 'admin', 'added_packagetype', 3, 'package_types', '2019-01-14 08:03:05', '2019-01-14 08:03:05'),
(61, 1, 'admin', 'added_packagetype', 4, 'package_types', '2019-01-14 08:04:35', '2019-01-14 08:04:35'),
(62, 1, 'admin', 'added_packagetype', 5, 'package_types', '2019-01-14 08:04:40', '2019-01-14 08:04:40'),
(63, 1, 'admin', 'updated_package', 1, 'packages', '2019-01-14 08:05:40', '2019-01-14 08:05:40'),
(64, 1, 'admin', 'added_product', 12, 'products', '2019-01-14 08:19:36', '2019-01-14 08:19:36'),
(65, 1, 'admin', 'added_product', 13, 'products', '2019-01-14 08:20:49', '2019-01-14 08:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2018_08_15_235149_create_permission_type', 1),
(7, '2018_08_15_235656_create_page', 1),
(8, '2018_08_16_123630_create_permissions_table', 1),
(9, '2018_08_16_123756_create_roles_table', 1),
(10, '2018_08_16_123829_create_role_permissions_table', 1),
(11, '2018_08_16_123857_create_countries_table', 1),
(12, '2018_08_16_123911_create_cities_table', 1),
(13, '2018_08_16_123935_create_accounts_table', 1),
(14, '2018_08_16_124005_create_users_table', 1),
(15, '2018_08_16_124019_create_user_follows_table', 1),
(16, '2018_08_16_124031_create_user_reports_table', 1),
(17, '2018_08_16_124042_create_chats_table', 1),
(18, '2018_08_16_124121_create_categories_table', 1),
(19, '2018_08_16_124203_create_stores_table', 1),
(20, '2018_08_16_124213_create_store_follows_table', 1),
(21, '2018_08_16_124224_create_store_reports_table', 1),
(22, '2018_08_17_171201_create_admins_table', 1),
(23, '2018_08_19_184529_create_admin_password_resets_table', 1),
(24, '2018_08_19_225254_create_tokens_table', 1),
(25, '2018_09_13_122903_create_property_types_table', 1),
(26, '2018_09_13_123027_create_category_properties_table', 1),
(27, '2018_09_13_123054_create_category_property_options_table', 1),
(28, '2018_09_18_231732_create_settings_table', 1),
(29, '2018_09_20_212100_create_sliders_table', 1),
(30, '2018_09_21_165628_create_log_table', 1),
(31, '2018_09_21_211946_add_stores_categories', 1),
(32, '2018_09_24_132803_create_account_countries_table', 1),
(33, '2018_09_24_134058_create_account_users_table', 1),
(34, '2018_09_24_134659_create_balances_table', 1),
(35, '2018_09_24_134716_create_balance_countries_table', 1),
(36, '2018_09_24_135035_create_packages_table', 1),
(37, '2018_09_24_135252_create_package_types_table', 1),
(38, '2018_09_24_135332_create_package_type_countries_table', 1),
(39, '2018_09_25_102648_create_brands_table', 1),
(40, '2018_09_25_163505_create_products_table', 1),
(41, '2018_09_25_171150_create_product_images_table', 1),
(42, '2018_09_25_171204_create_product_sizes_table', 1),
(43, '2018_09_25_171224_create_product_property_values_table', 1),
(44, '2018_09_25_174736_create_product_colors_table', 1),
(45, '2018_09_28_210508_create_website_pages_table', 1),
(46, '2018_10_11_091045_create_admin_countries_table', 1),
(47, '2018_10_11_101617_create_free_adds_table', 1),
(48, '2018_10_12_180519_create_product_likes_table', 1),
(49, '2018_10_12_180612_create_product_rates_table', 1),
(50, '2018_12_31_084511_create_mobiles_table', 1),
(51, '2018_12_31_084751_create_apis_table', 1),
(52, '2018_18_16_124006_create_password_resets_table', 1),
(53, '2019_01_05_133908_create_textsliders_table', 1),
(54, '2019_01_05_173036_create_adds_table', 1),
(55, '2019_01_13_083526_create_store_rates_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `video` text COLLATE utf8mb4_unicode_ci,
  `categories_background` text COLLATE utf8mb4_unicode_ci,
  `buyonline_background` text COLLATE utf8mb4_unicode_ci,
  `stores_background` text COLLATE utf8mb4_unicode_ci,
  `ads_background` text COLLATE utf8mb4_unicode_ci,
  `about_background` text COLLATE utf8mb4_unicode_ci,
  `en_about` text COLLATE utf8mb4_unicode_ci,
  `ar_about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `video`, `categories_background`, `buyonline_background`, `stores_background`, `ads_background`, `about_background`, `en_about`, `ar_about`, `created_at`, `updated_at`) VALUES
(1, NULL, '1547459144.jpg', '1547459133.jpg', '1547459271.png', NULL, '1547459302.jpg', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy', 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ..', '2019-01-13 15:28:53', '2019-01-14 07:48:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` double NOT NULL DEFAULT '0',
  `balance` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `ar_name`, `en_name`, `views`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'تيربو', 'Terpo', 30, 200, '2019-01-14 07:50:56', '2019-01-14 08:05:40'),
(2, 'الباقة الذهبية', 'Golden', 100, 3000, '2019-01-14 08:02:45', '2019-01-14 08:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `package_types`
--

CREATE TABLE `package_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `package_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_types`
--

INSERT INTO `package_types` (`id`, `days`, `package_id`, `created_at`, `updated_at`) VALUES
(1, 3, 2, '2019-01-14 08:02:53', '2019-01-14 08:02:53'),
(2, 7, 2, '2019-01-14 08:02:59', '2019-01-14 08:02:59'),
(3, 15, 2, '2019-01-14 08:03:05', '2019-01-14 08:03:05'),
(4, 5, 1, '2019-01-14 08:04:35', '2019-01-14 08:04:35'),
(5, 7, 1, '2019-01-14 08:04:40', '2019-01-14 08:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `package_type_countries`
--

CREATE TABLE `package_type_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_type_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_type_countries`
--

INSERT INTO `package_type_countries` (`id`, `package_type_id`, `country_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 300, '2019-01-14 08:03:24', '2019-01-14 08:03:24'),
(2, 1, 2, 100, '2019-01-14 08:03:24', '2019-01-14 08:03:24'),
(3, 2, 1, 500, '2019-01-14 08:03:58', '2019-01-14 08:03:58'),
(4, 2, 2, 300, '2019-01-14 08:03:58', '2019-01-14 08:03:58'),
(5, 3, 1, 700, '2019-01-14 08:04:15', '2019-01-14 08:04:15'),
(6, 3, 2, 600, '2019-01-14 08:04:15', '2019-01-14 08:04:15'),
(7, 4, 1, 50, '2019-01-14 08:04:53', '2019-01-14 08:04:53'),
(8, 4, 2, 30, '2019-01-14 08:04:53', '2019-01-14 08:04:53'),
(9, 5, 1, 100, '2019-01-14 08:05:08', '2019-01-14 08:05:08'),
(10, 5, 2, 50, '2019-01-14 08:05:08', '2019-01-14 08:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'City', '2019-01-13 14:01:26', '2019-01-13 14:01:26'),
(2, 'Country', '2019-01-13 14:01:27', '2019-01-13 14:01:27'),
(3, 'Admin', '2019-01-13 14:01:27', '2019-01-13 14:01:27'),
(4, 'User', '2019-01-13 14:01:27', '2019-01-13 14:01:27'),
(5, 'Roles', '2019-01-13 14:01:28', '2019-01-13 14:01:28'),
(6, 'Account', '2019-01-13 14:01:28', '2019-01-13 14:01:28'),
(7, 'Category', '2019-01-13 14:01:28', '2019-01-13 14:01:28'),
(8, 'Store', '2019-01-13 14:01:28', '2019-01-13 14:01:28'),
(9, 'Setting', '2019-01-13 14:01:29', '2019-01-13 14:01:29'),
(10, 'Slider', '2019-01-13 14:01:29', '2019-01-13 14:01:29'),
(11, 'Log', '2019-01-13 14:01:29', '2019-01-13 14:01:29'),
(12, 'Product', '2019-01-13 14:01:29', '2019-01-13 14:01:29'),
(13, 'Balance', '2019-01-13 14:01:30', '2019-01-13 14:01:30'),
(14, 'Package', '2019-01-13 14:01:30', '2019-01-13 14:01:30'),
(15, 'Brand', '2019-01-13 14:01:31', '2019-01-13 14:01:31'),
(16, 'WebsitePage', '2019-01-13 14:01:31', '2019-01-13 14:01:31'),
(17, 'Mobile Settings', '2019-01-13 14:01:31', '2019-01-13 14:01:31'),
(18, 'Mobile Api', '2019-01-13 14:01:31', '2019-01-13 14:01:31'),
(19, 'Mobile Tutorial', '2019-01-13 14:01:32', '2019-01-13 14:01:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `permission_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `key`, `page_id`, `permission_type_id`, `created_at`, `updated_at`) VALUES
(1, 'اضافه بلد', 'add_country', 2, 1, '2019-01-13 14:01:32', '2019-01-13 14:01:32'),
(2, 'تعديل بلد', 'update_country', 2, 2, '2019-01-13 14:01:32', '2019-01-13 14:01:32'),
(3, 'مسح بلد', 'delete_country', 2, 3, '2019-01-13 14:01:33', '2019-01-13 14:01:33'),
(4, 'قائمه البلدان', 'list_country', 2, 5, '2019-01-13 14:01:33', '2019-01-13 14:01:33'),
(5, 'تفاصيل البلدان', 'show_country', 2, 4, '2019-01-13 14:01:34', '2019-01-13 14:01:34'),
(6, 'اضافه مدينه', 'add_city', 1, 1, '2019-01-13 14:01:34', '2019-01-13 14:01:34'),
(7, 'تعديل مدينه', 'update_city', 1, 2, '2019-01-13 14:01:34', '2019-01-13 14:01:34'),
(8, 'مسح مدينه', 'delete_city', 1, 3, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(9, 'قائمه المدن', 'list_city', 1, 5, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(10, 'تفاصيل المدن', 'show_city', 1, 4, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(11, 'اضافه المديرين', 'add_admin', 3, 1, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(12, 'تعديل المديرين', 'update_admin', 3, 2, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(13, 'مسح المديرين', 'delete_admin', 3, 3, '2019-01-13 14:01:35', '2019-01-13 14:01:35'),
(14, 'قائمه المديرين', 'list_admin', 3, 5, '2019-01-13 14:01:36', '2019-01-13 14:01:36'),
(15, 'تفاصيل المديرين', 'show_admin', 3, 4, '2019-01-13 14:01:36', '2019-01-13 14:01:36'),
(16, 'اضافه المستخدمين', 'add_user', 4, 1, '2019-01-13 14:01:36', '2019-01-13 14:01:36'),
(17, 'تعديل المستخدمين', 'update_user', 4, 2, '2019-01-13 14:01:36', '2019-01-13 14:01:36'),
(18, 'مسح المستخدمين', 'delete_user', 4, 3, '2019-01-13 14:01:37', '2019-01-13 14:01:37'),
(19, 'قائمه المستخدمين', 'list_user', 4, 5, '2019-01-13 14:01:37', '2019-01-13 14:01:37'),
(20, 'تفاصيل المستخدمين', 'show_user', 4, 4, '2019-01-13 14:01:37', '2019-01-13 14:01:37'),
(21, 'اضافه الصلاحيات', 'add_role', 5, 1, '2019-01-13 14:01:38', '2019-01-13 14:01:38'),
(22, 'تعديل الصلاحيات', 'update_role', 5, 2, '2019-01-13 14:01:38', '2019-01-13 14:01:38'),
(23, 'مسح الصلاحيات', 'delete_role', 5, 3, '2019-01-13 14:01:38', '2019-01-13 14:01:38'),
(24, 'قائمه الصلاحيات', 'list_role', 5, 5, '2019-01-13 14:01:38', '2019-01-13 14:01:38'),
(25, 'تفاصيل الصلاحيات', 'show_role', 5, 4, '2019-01-13 14:01:38', '2019-01-13 14:01:38'),
(26, 'اضافه حساب', 'add_account', 6, 1, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(27, 'تعديل حساب', 'update_account', 6, 2, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(28, 'مسح حساب', 'delete_account', 6, 3, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(29, 'قائمه حساب', 'list_account', 6, 5, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(30, 'تفاصيل حساب', 'show_account', 6, 4, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(31, 'اضافه صنف', 'add_category', 7, 1, '2019-01-13 14:01:39', '2019-01-13 14:01:39'),
(32, 'تعديل صنف', 'update_category', 7, 2, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(33, 'مسح صنف', 'delete_category', 7, 3, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(34, 'قائمه صنف', 'list_category', 7, 5, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(35, 'تفاصيل صنف', 'show_category', 7, 4, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(36, 'اضافه صنف', 'add_store', 8, 1, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(37, 'تعديل متجر', 'update_store', 8, 2, '2019-01-13 14:01:40', '2019-01-13 14:01:40'),
(38, 'مسح متجر', 'delete_store', 8, 3, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(39, 'قائمه متجر', 'list_store', 8, 5, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(40, 'تفاصيل متجر', 'show_store', 8, 4, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(41, 'اضافه السلايدر', 'add_slider', 10, 1, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(42, 'تعديل السلايدر', 'update_slider', 10, 2, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(43, 'مسح السلايدر', 'delete_slider', 10, 3, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(44, 'قائمه السلايدر', 'list_slider', 10, 5, '2019-01-13 14:01:41', '2019-01-13 14:01:41'),
(45, 'تفاصيل  السلايدر', 'show_slider', 10, 4, '2019-01-13 14:01:42', '2019-01-13 14:01:42'),
(46, 'قائمه لوج', 'list_log', 11, 5, '2019-01-13 14:01:42', '2019-01-13 14:01:42'),
(47, 'اضافه المنتج', 'add_product', 12, 1, '2019-01-13 14:01:42', '2019-01-13 14:01:42'),
(48, 'تعديل المنتج', 'update_product', 12, 2, '2019-01-13 14:01:42', '2019-01-13 14:01:42'),
(49, 'مسح المنتج', 'delete_product', 12, 3, '2019-01-13 14:01:43', '2019-01-13 14:01:43'),
(50, 'قائمه المنتج', 'list_product', 12, 5, '2019-01-13 14:01:43', '2019-01-13 14:01:43'),
(51, 'تفاصيل  المنتج', 'show_product', 12, 4, '2019-01-13 14:01:44', '2019-01-13 14:01:44'),
(52, 'اضافه رصيد', 'add_balance', 13, 1, '2019-01-13 14:01:44', '2019-01-13 14:01:44'),
(53, 'تعديل رصيد', 'update_balance', 13, 2, '2019-01-13 14:01:44', '2019-01-13 14:01:44'),
(54, 'مسح رصيد', 'delete_balance', 13, 3, '2019-01-13 14:01:45', '2019-01-13 14:01:45'),
(55, 'قائمه رصيد', 'list_balance', 13, 5, '2019-01-13 14:01:45', '2019-01-13 14:01:45'),
(56, 'تفاصيل  رصيد', 'show_balance', 13, 4, '2019-01-13 14:01:45', '2019-01-13 14:01:45'),
(57, 'اضافه حزمه', 'add_package', 14, 1, '2019-01-13 14:01:45', '2019-01-13 14:01:45'),
(58, 'تعديل حزمه', 'update_package', 14, 2, '2019-01-13 14:01:46', '2019-01-13 14:01:46'),
(59, 'مسح حزمه', 'delete_package', 14, 3, '2019-01-13 14:01:46', '2019-01-13 14:01:46'),
(60, 'قائمه حزم', 'list_package', 14, 5, '2019-01-13 14:01:46', '2019-01-13 14:01:46'),
(61, 'تفاصيل  حزمه', 'show_package', 14, 4, '2019-01-13 14:01:46', '2019-01-13 14:01:46'),
(62, 'اضافه ماركة', 'add_brand', 15, 1, '2019-01-13 14:01:46', '2019-01-13 14:01:46'),
(63, 'تعديل ماركة', 'update_brand', 15, 2, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(64, 'مسح ماركة', 'delete_brand', 15, 3, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(65, 'قائمه الماركات', 'list_brand', 15, 5, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(66, 'تفاصيل  براند', 'show_brand', 15, 4, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(67, ' الاعدادات', 'update_setting', 9, 2, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(68, 'اضافه صفحات الموقع', 'add_web_page', 16, 1, '2019-01-13 14:01:47', '2019-01-13 14:01:47'),
(69, 'تعديل صفحات الموقع', 'update_web_page', 16, 2, '2019-01-13 14:01:48', '2019-01-13 14:01:48'),
(70, 'مسح صفحات الموقع', 'delete_web_page', 16, 3, '2019-01-13 14:01:48', '2019-01-13 14:01:48'),
(71, 'قائمه صفحات الموقع', 'list_web_page', 16, 5, '2019-01-13 14:01:48', '2019-01-13 14:01:48'),
(72, 'تفاصيل  صفحات الموقع', 'show_web_page', 16, 4, '2019-01-13 14:01:48', '2019-01-13 14:01:48'),
(73, ' الاعدادات moble', 'update_setting_mobile', 17, 2, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(74, 'text slider', 'add_text_slider', 19, 1, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(75, 'text slider', 'update_text_slider', 19, 2, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(76, 'text slider', 'delete_text_slider', 19, 3, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(77, 'text slider', 'list_text_slider', 19, 5, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(78, 'text slider', 'show_text_slider', 19, 4, '2019-01-13 14:01:49', '2019-01-13 14:01:49'),
(79, 'api', 'update_api', 18, 2, '2019-01-13 14:01:49', '2019-01-13 14:01:49');

-- --------------------------------------------------------

--
-- Table structure for table `permission_types`
--

CREATE TABLE `permission_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_types`
--

INSERT INTO `permission_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ADD', '2019-01-13 14:01:25', '2019-01-13 14:01:25'),
(2, 'Update', '2019-01-13 14:01:25', '2019-01-13 14:01:25'),
(3, 'Delete', '2019-01-13 14:01:25', '2019-01-13 14:01:25'),
(4, 'Show', '2019-01-13 14:01:26', '2019-01-13 14:01:26'),
(5, 'List', '2019-01-13 14:01:26', '2019-01-13 14:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `en_name` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `ar_name` text COLLATE utf8mb4_unicode_ci,
  `en_desc` text COLLATE utf8mb4_unicode_ci,
  `en_brief_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_brief_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `offer` double NOT NULL DEFAULT '0',
  `lat` double NOT NULL DEFAULT '0',
  `long` double NOT NULL DEFAULT '0',
  `viewers` double NOT NULL DEFAULT '0',
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `is_avail` tinyint(1) NOT NULL DEFAULT '0',
  `is_flexible_date` tinyint(1) NOT NULL DEFAULT '0',
  `during_arrive` int(11) DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(10) UNSIGNED DEFAULT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `package_type_country_id` int(10) UNSIGNED DEFAULT NULL,
  `special_due_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `image`, `en_name`, `code`, `ar_name`, `en_desc`, `en_brief_desc`, `ar_brief_desc`, `ar_desc`, `en_meta_desc`, `ar_meta_desc`, `en_meta_title`, `ar_meta_title`, `en_meta_tags`, `ar_meta_tags`, `price`, `quantity`, `offer`, `lat`, `long`, `viewers`, `is_online`, `is_active`, `is_special`, `is_avail`, `is_flexible_date`, `during_arrive`, `category_id`, `user_id`, `store_id`, `brand_id`, `package_type_country_id`, `special_due_date`, `created_at`, `updated_at`) VALUES
(1, '1547456699.jpg', 'Smart Watch', '2123', 'اسمارت واتش الذكيه', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 250, 22, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 3, 1, NULL, 5, NULL, NULL, '2019-01-14 07:04:59', '2019-01-14 07:12:26'),
(2, '1547457116.jpg', 'Prifix Mini receiver full HD 8000H1 - Black', '541', 'Prifix Mini receiver full HD 8000H1 - Black', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 255, 30, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 2, 1, NULL, 2, NULL, NULL, '2019-01-14 07:11:56', '2019-01-14 07:12:09'),
(3, '1547457204.jpeg', 'Apple iPhone 7', '654564', 'Apple iPhone 7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 600, 36, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 2, 1, NULL, NULL, NULL, NULL, '2019-01-14 07:13:24', '2019-01-14 07:13:24'),
(4, '1547457299.jpeg', 'mobile Honor 10', '7876', 'mobile Honor 10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5245, 54, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 2, 1, NULL, 1, NULL, NULL, '2019-01-14 07:14:59', '2019-01-14 07:15:07'),
(5, '1547457381.jpeg', 'mobile Gionee A1 (4GB, 64 GB)', '5645', 'mobile Gionee A1 (4GB, 64 GB)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 366, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 2, 1, NULL, 2, NULL, NULL, '2019-01-14 07:16:21', '2019-01-14 07:16:31'),
(6, '1547457683.jpg', 'Amazon Fire HD 10 (2017)', '600', 'Amazon Fire HD 10 (2017)', NULL, 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 600, 600, 0, 0, 0, 0, 1, 1, 0, 1, 0, NULL, 5, 1, NULL, 1, NULL, NULL, '2019-01-14 07:21:24', '2019-01-14 07:21:49'),
(7, '1547457824.jpg', 'Ipad', '666', 'Ipad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, 60, 0, 0, 0, 0, 1, 1, 0, 1, 0, NULL, 5, 1, NULL, 5, NULL, NULL, '2019-01-14 07:23:44', '2019-01-14 07:23:53'),
(8, '1547458016.jpg', 'Kenwood 7 Kg 1000 RPM Front Load Washing Machine, White -', '97954', 'Kenwood 7 Kg 1000 RPM Front Load Washing Machine, White -', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 600, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 11, 1, NULL, 4, NULL, NULL, '2019-01-14 07:26:56', '2019-01-14 07:27:05'),
(9, '1547458072.jpg', 'LG 24 Class HD TV Monitor', '434', 'LG 24 Class HD TV Monitor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 900, 933, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 11, 1, NULL, NULL, NULL, NULL, '2019-01-14 07:27:52', '2019-01-14 07:27:52'),
(10, '1547458187.jpg', 'Unionaire C6060SS-AP-447-L, 4 Gas Burner Stainless Steel', '343', 'بوتجاز 4 شعلة ستانلس كامل يونير اير', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 900, 300, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, 11, 1, NULL, NULL, NULL, NULL, '2019-01-14 07:29:47', '2019-01-14 07:29:47'),
(11, '1547458356.jpg', 'شقة مميزة 284م بكمبوند اوشن بلو', '5006', 'شقة مميزة 284م بكمبوند اوشن بلو', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60000, 90, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, 9, 2, NULL, NULL, NULL, NULL, '2019-01-14 07:32:36', '2019-01-14 07:32:36'),
(12, '1547461176.jpg', 'BMW Approved Used Cars', '545', 'BMW Approved Used Cars', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90000, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, 1, 2, NULL, NULL, NULL, NULL, '2019-01-14 08:19:36', '2019-01-14 08:19:36'),
(13, '1547461249.jpg', 'Super Car - Vehicles on Display', '5456', 'Super Car - Vehicles on Display', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 900000, 60, 0, 0, 0, 0, 0, 1, 0, 1, 0, NULL, 1, 2, NULL, NULL, NULL, NULL, '2019-01-14 08:20:49', '2019-01-14 08:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `product_colors`
--

CREATE TABLE `product_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `color` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `image`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '94HqYQ_w8F4Ux_f11c3d254f36571d5a9178c14f04c0293ec01f6d243b4cce6b24426b3d3d7a3f.jpg.jpg', 1, '2019-01-14 07:04:59', '2019-01-14 07:04:59'),
(2, '5LHsfp_download (5).jpeg', 2, '2019-01-14 07:11:56', '2019-01-14 07:11:56'),
(3, 'DITv7C_download (4).jpeg', 2, '2019-01-14 07:11:56', '2019-01-14 07:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `product_likes`
--

CREATE TABLE `product_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_property_values`
--

CREATE TABLE `product_property_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_property_values`
--

INSERT INTO `product_property_values` (`id`, `product_id`, `property_id`, `option_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 6, 1, NULL, '10 inch', '2019-01-14 07:21:24', '2019-01-14 07:21:24'),
(2, 6, 2, NULL, '5 G', '2019-01-14 07:21:24', '2019-01-14 07:21:24'),
(3, 6, 3, NULL, NULL, '2019-01-14 07:21:24', '2019-01-14 07:21:24'),
(4, 6, 4, NULL, NULL, '2019-01-14 07:21:24', '2019-01-14 07:21:24'),
(5, 7, 1, NULL, '10 inch', '2019-01-14 07:23:44', '2019-01-14 07:23:44'),
(6, 7, 2, NULL, '64 G', '2019-01-14 07:23:44', '2019-01-14 07:23:44'),
(7, 7, 3, NULL, NULL, '2019-01-14 07:23:44', '2019-01-14 07:23:44'),
(8, 7, 4, NULL, NULL, '2019-01-14 07:23:45', '2019-01-14 07:23:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_rates`
--

CREATE TABLE `product_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `rate` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `size` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE `property_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `en_name`, `ar_name`, `created_at`, `updated_at`) VALUES
(1, 'list', 'قائمة خيارات', '2019-01-13 14:01:51', '2019-01-13 14:01:51'),
(2, 'Text', 'مقاليه', '2019-01-13 14:01:51', '2019-01-13 14:01:51'),
(3, 'Number', 'رقم', '2019-01-13 14:01:52', '2019-01-13 14:01:52'),
(4, 'date', 'تاريخ', '2019-01-13 14:01:52', '2019-01-13 14:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2019-01-13 14:01:50', '2019-01-13 14:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-01-13 14:01:50', '2019-01-13 14:01:50'),
(2, 1, 2, '2019-01-13 14:01:50', '2019-01-13 14:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `web_about_us` text COLLATE utf8mb4_unicode_ci,
  `about_us` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `mobile` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `google` text COLLATE utf8mb4_unicode_ci,
  `youtube` text COLLATE utf8mb4_unicode_ci,
  `instagram` text COLLATE utf8mb4_unicode_ci,
  `pin` text COLLATE utf8mb4_unicode_ci,
  `welcome_title` text COLLATE utf8mb4_unicode_ci,
  `welcome_text` text COLLATE utf8mb4_unicode_ci,
  `about_app` text COLLATE utf8mb4_unicode_ci,
  `trust_price` double NOT NULL DEFAULT '0',
  `star_price` double NOT NULL DEFAULT '0',
  `agent` double NOT NULL DEFAULT '0',
  `owner` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `web_about_us`, `about_us`, `address`, `email`, `mobile`, `facebook`, `twitter`, `google`, `youtube`, `instagram`, `pin`, `welcome_title`, `welcome_text`, `about_app`, `trust_price`, `star_price`, `agent`, `owner`, `created_at`, `updated_at`) VALUES
(1, NULL, 'admin', NULL, 'administrator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-01-13 14:01:52', '2019-01-13 14:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/slider/default.png',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `url`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '1547455830.jpg', 'https://eg.opensooq.com', 1, '2019-01-14 06:50:30', '2019-01-14 06:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/stores/default.png',
  `bg` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/stores/default.png',
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_views` int(11) NOT NULL DEFAULT '0',
  `no_follows` int(11) NOT NULL DEFAULT '0',
  `no_adds` int(11) NOT NULL DEFAULT '0',
  `lat` double NOT NULL DEFAULT '0',
  `long` double NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `en_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_desc` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `image`, `bg`, `en_name`, `ar_name`, `mobile`, `address`, `no_views`, `no_follows`, `no_adds`, `lat`, `long`, `user_id`, `en_desc`, `ar_desc`, `facebook`, `twitter`, `instagram`, `google`, `youtube`, `en_meta_desc`, `ar_meta_desc`, `en_meta_title`, `ar_meta_title`, `en_meta_tags`, `ar_meta_tags`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '1547416127.jpg', '1547416127.jpg', 'Mobile Shop', 'موبايل شوب', '1204682604', '6933-7251 Chadbourne Rd, Brentwood, CA 94513، الولايات المتحدة', 0, 0, 0, 37.93701818188627, -121.83269495000002, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-01-13 19:48:47', '2019-01-13 19:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `store_categories`
--

CREATE TABLE `store_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_categories`
--

INSERT INTO `store_categories` (`id`, `category_id`, `store_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2019-01-13 19:48:47', '2019-01-13 19:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `store_follows`
--

CREATE TABLE `store_follows` (
  `id` int(10) UNSIGNED NOT NULL,
  `follower_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `is_follow` tinyint(1) NOT NULL DEFAULT '0',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_rates`
--

CREATE TABLE `store_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_reports`
--

CREATE TABLE `store_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `textsliders`
--

CREATE TABLE `textsliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/users/default.png',
  `provider_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bg` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/images/users/default.png',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '9665',
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` double NOT NULL DEFAULT '0',
  `long` double NOT NULL DEFAULT '0',
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` double NOT NULL DEFAULT '0',
  `no_views` int(11) NOT NULL DEFAULT '0',
  `no_follows` int(11) NOT NULL DEFAULT '0',
  `no_followings` int(11) NOT NULL DEFAULT '0',
  `no_adds` int(11) NOT NULL DEFAULT '0',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `is_notification` tinyint(1) NOT NULL DEFAULT '0',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `image`, `provider_image`, `bg`, `name`, `email`, `country_code`, `mobile`, `birth_date`, `password`, `address`, `lat`, `long`, `facebook`, `twitter`, `instagram`, `google`, `youtube`, `verification_code`, `wallet`, `no_views`, `no_follows`, `no_followings`, `no_adds`, `provider`, `provider_id`, `gender`, `is_active`, `is_confirm`, `is_notification`, `is_public`, `city_id`, `account_id`, `created_at`, `updated_at`) VALUES
(1, 'assets/images/users/default.png', '', 'assets/images/users/default.png', 'Angham', 'anghammahamed16@gmail.com', '123456', '123456', NULL, '$2y$10$EL93I7/HfGPZQVuLU0IyIeqcF6c37GJdJajvYDBjmWmF1UohFFHjW', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 1, 1, NULL, '2019-01-13 15:42:18', '2019-01-13 15:42:18'),
(2, 'assets/images/users/default.png', '', 'assets/images/users/default.png', 'John', 'john@dalili.com', '9665', '0456465', NULL, '$2y$10$RiHqBED41cLWoUpKGOySzOi0aS6JzAWncQT8SyzVLa/ZAyTZlDxAe', '13 Nozha St. - Nasr City - Cairo', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 0, 0, 0, 1, NULL, '2019-01-14 07:31:24', '2019-01-14 07:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_follows`
--

CREATE TABLE `user_follows` (
  `id` int(10) UNSIGNED NOT NULL,
  `follower_id` int(10) UNSIGNED NOT NULL,
  `following_id` int(10) UNSIGNED NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `is_follow` tinyint(1) NOT NULL DEFAULT '0',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reported_id` int(10) UNSIGNED NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_pages`
--

CREATE TABLE `website_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `en_name` text COLLATE utf8mb4_unicode_ci,
  `ar_name` text COLLATE utf8mb4_unicode_ci,
  `en_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_desc` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_desc` text COLLATE utf8mb4_unicode_ci,
  `en_meta_title` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_title` text COLLATE utf8mb4_unicode_ci,
  `en_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `ar_meta_tags` text COLLATE utf8mb4_unicode_ci,
  `is_header` tinyint(1) NOT NULL DEFAULT '0',
  `is_footer` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_countries`
--
ALTER TABLE `account_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_countries_account_id_foreign` (`account_id`),
  ADD KEY `account_countries_country_id_foreign` (`country_id`);

--
-- Indexes for table `account_users`
--
ALTER TABLE `account_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_users_account_id_foreign` (`account_id`),
  ADD KEY `account_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `adds`
--
ALTER TABLE `adds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_mobile_unique` (`mobile`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD KEY `admins_role_id_foreign` (`role_id`);

--
-- Indexes for table `admin_countries`
--
ALTER TABLE `admin_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_countries_admin_id_foreign` (`admin_id`),
  ADD KEY `admin_countries_country_id_foreign` (`country_id`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indexes for table `apis`
--
ALTER TABLE `apis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balances`
--
ALTER TABLE `balances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balance_countries`
--
ALTER TABLE `balance_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `balance_countries_balance_id_foreign` (`balance_id`),
  ADD KEY `balance_countries_country_id_foreign` (`country_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brands_category_id_foreign` (`category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `category_properties`
--
ALTER TABLE `category_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_properties_category_id_foreign` (`category_id`),
  ADD KEY `category_properties_type_id_foreign` (`type_id`);

--
-- Indexes for table `category_property_options`
--
ALTER TABLE `category_property_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_property_options_property_id_foreign` (`property_id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_sender_id_foreign` (`sender_id`),
  ADD KEY `chats_receiver_id_foreign` (`receiver_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_adds`
--
ALTER TABLE `free_adds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `free_adds_category_id_foreign` (`category_id`),
  ADD KEY `free_adds_country_id_foreign` (`country_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_types`
--
ALTER TABLE `package_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_types_package_id_foreign` (`package_id`);

--
-- Indexes for table `package_type_countries`
--
ALTER TABLE `package_type_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_type_countries_package_type_id_foreign` (`package_type_id`),
  ADD KEY `package_type_countries_country_id_foreign` (`country_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_permission_type_id_foreign` (`permission_type_id`),
  ADD KEY `permissions_page_id_foreign` (`page_id`);

--
-- Indexes for table `permission_types`
--
ALTER TABLE `permission_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_user_id_foreign` (`user_id`),
  ADD KEY `products_store_id_foreign` (`store_id`),
  ADD KEY `products_brand_id_foreign` (`brand_id`),
  ADD KEY `products_package_type_country_id_foreign` (`package_type_country_id`);

--
-- Indexes for table `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_colors_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_likes`
--
ALTER TABLE `product_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_likes_product_id_foreign` (`product_id`),
  ADD KEY `product_likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_property_values`
--
ALTER TABLE `product_property_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_property_values_product_id_foreign` (`product_id`),
  ADD KEY `product_property_values_property_id_foreign` (`property_id`),
  ADD KEY `product_property_values_option_id_foreign` (`option_id`);

--
-- Indexes for table `product_rates`
--
ALTER TABLE `product_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_rates_product_id_foreign` (`product_id`),
  ADD KEY `product_rates_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_sizes_product_id_foreign` (`product_id`);

--
-- Indexes for table `property_types`
--
ALTER TABLE `property_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `role_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stores_user_id_foreign` (`user_id`);

--
-- Indexes for table `store_categories`
--
ALTER TABLE `store_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_categories_category_id_foreign` (`category_id`),
  ADD KEY `store_categories_store_id_foreign` (`store_id`);

--
-- Indexes for table `store_follows`
--
ALTER TABLE `store_follows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_follows_follower_id_foreign` (`follower_id`),
  ADD KEY `store_follows_store_id_foreign` (`store_id`);

--
-- Indexes for table `store_rates`
--
ALTER TABLE `store_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_rates_user_id_foreign` (`user_id`),
  ADD KEY `store_rates_store_id_foreign` (`store_id`);

--
-- Indexes for table `store_reports`
--
ALTER TABLE `store_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `textsliders`
--
ALTER TABLE `textsliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_city_id_foreign` (`city_id`),
  ADD KEY `users_account_id_foreign` (`account_id`);

--
-- Indexes for table `user_follows`
--
ALTER TABLE `user_follows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_follows_follower_id_foreign` (`follower_id`),
  ADD KEY `user_follows_following_id_foreign` (`following_id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_reports_user_id_foreign` (`user_id`),
  ADD KEY `user_reports_reported_id_foreign` (`reported_id`);

--
-- Indexes for table `website_pages`
--
ALTER TABLE `website_pages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `account_countries`
--
ALTER TABLE `account_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `account_users`
--
ALTER TABLE `account_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adds`
--
ALTER TABLE `adds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_countries`
--
ALTER TABLE `admin_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apis`
--
ALTER TABLE `apis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `balances`
--
ALTER TABLE `balances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `balance_countries`
--
ALTER TABLE `balance_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category_properties`
--
ALTER TABLE `category_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category_property_options`
--
ALTER TABLE `category_property_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `free_adds`
--
ALTER TABLE `free_adds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `package_types`
--
ALTER TABLE `package_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `package_type_countries`
--
ALTER TABLE `package_type_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `permission_types`
--
ALTER TABLE `permission_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_likes`
--
ALTER TABLE `product_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_property_values`
--
ALTER TABLE `product_property_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_rates`
--
ALTER TABLE `product_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_types`
--
ALTER TABLE `property_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store_categories`
--
ALTER TABLE `store_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store_follows`
--
ALTER TABLE `store_follows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_rates`
--
ALTER TABLE `store_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_reports`
--
ALTER TABLE `store_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `textsliders`
--
ALTER TABLE `textsliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_follows`
--
ALTER TABLE `user_follows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_pages`
--
ALTER TABLE `website_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_countries`
--
ALTER TABLE `account_countries`
  ADD CONSTRAINT `account_countries_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `account_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `account_users`
--
ALTER TABLE `account_users`
  ADD CONSTRAINT `account_users_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `account_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `admin_countries`
--
ALTER TABLE `admin_countries`
  ADD CONSTRAINT `admin_countries_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `balance_countries`
--
ALTER TABLE `balance_countries`
  ADD CONSTRAINT `balance_countries_balance_id_foreign` FOREIGN KEY (`balance_id`) REFERENCES `balances` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `category_properties`
--
ALTER TABLE `category_properties`
  ADD CONSTRAINT `category_properties_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_properties_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `property_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_property_options`
--
ALTER TABLE `category_property_options`
  ADD CONSTRAINT `category_property_options_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `category_properties` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chats_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `free_adds`
--
ALTER TABLE `free_adds`
  ADD CONSTRAINT `free_adds_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `free_adds_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_types`
--
ALTER TABLE `package_types`
  ADD CONSTRAINT `package_types_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_type_countries`
--
ALTER TABLE `package_type_countries`
  ADD CONSTRAINT `package_type_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_type_countries_package_type_id_foreign` FOREIGN KEY (`package_type_id`) REFERENCES `package_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permissions_permission_type_id_foreign` FOREIGN KEY (`permission_type_id`) REFERENCES `permission_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_package_type_country_id_foreign` FOREIGN KEY (`package_type_country_id`) REFERENCES `package_type_countries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `products_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_colors`
--
ALTER TABLE `product_colors`
  ADD CONSTRAINT `product_colors_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_likes`
--
ALTER TABLE `product_likes`
  ADD CONSTRAINT `product_likes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_property_values`
--
ALTER TABLE `product_property_values`
  ADD CONSTRAINT `product_property_values_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `category_property_options` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `product_property_values_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_property_values_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `category_properties` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_rates`
--
ALTER TABLE `product_rates`
  ADD CONSTRAINT `product_rates_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_rates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD CONSTRAINT `product_sizes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD CONSTRAINT `role_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `stores_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `store_categories`
--
ALTER TABLE `store_categories`
  ADD CONSTRAINT `store_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_categories_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `store_follows`
--
ALTER TABLE `store_follows`
  ADD CONSTRAINT `store_follows_follower_id_foreign` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_follows_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `store_rates`
--
ALTER TABLE `store_rates`
  ADD CONSTRAINT `store_rates_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_rates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tokens`
--
ALTER TABLE `tokens`
  ADD CONSTRAINT `tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `user_follows`
--
ALTER TABLE `user_follows`
  ADD CONSTRAINT `user_follows_follower_id_foreign` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_follows_following_id_foreign` FOREIGN KEY (`following_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD CONSTRAINT `user_reports_reported_id_foreign` FOREIGN KEY (`reported_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
