///----------------category details -------------------//
$('.color li.color_li').click(function(){
    $(this).toggleClass("active")
    if( $(this).children('input[type="checkbox"]').is(":checked"))
    {
        $(this).children('input[type="checkbox"]').prop('checked', false);
    }else{
        $(this).children('input[type="checkbox"]').attr("checked", "checked");
    }

});
$('.size li.size_li').click(function(){
    $(this).toggleClass("active")

    if( $(this).children('input[type="checkbox"]').is(":checked"))
    {

        $(this).children('input[type="checkbox"]').prop('checked', false);
    }else{
        $(this).children('input[type="checkbox"]').prop("checked", "checked");
    }

});

$(document).on('ready',function() {
    $('.slidersClass').each ( function() {
        debugger;
        var miiin=$(this).attr('data-min');
        var maxx=$(this).attr('data-max');
        if( $(this).length) {

            window.startRangeValues = [1, maxx];
            $(this).slider({

                range: true,
                min: 1,
                max: maxx,
                values:  window.startRangeValues,
                step: 1,

                slide: function (event, ui) {

                    var min = ui.values[0].toFixed(2),
                        max = ui.values[1].toFixed(2),
                        range = $(this).siblings('.range');


                    range.children('.min_value').val(min).next().val(max);

                    range.children('.min_val').text('$' + min).next().text('$' + max);
                    range.children('#min_val').val(min);
                    range.children('#max_val').val(max);

                },

                create: function (event, ui) {
                    var ff=$(this).slider("values", 0);
                    var $this = $(this),
                        min = $this.slider("values", 0),
                        max = $this.slider("values", 1),
                        range = $this.siblings('.range');

                    range.children('.min_value').val(min).next().val(max);
                    range.children('#min_val').val(min);
                    range.children('#max_val').val(max);

                    range.children('.min_val').text('$' + min).next().text('$' + max);

                }

            });

        }

    });
});

$('.datepicker').datepicker();
///----------------end category details -------------------//