//============== for print div  contant ================//
function PrintElem(elem) {
    Popup($(elem).html());
}

function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>my div</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();
    return true;
}


$(document).ready(function () {

//=================== ajax  pagnation table  ========================//

    $(document).on('submit', '.rate-form-request', function (event) {
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');
        var thisForm = $(this);
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        // var formData   =  thisForm.serialize();
        var formData = new FormData(this);

        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled', 'disabled');
            },
            success: function (data) {
                $('.message').prepend('<div class="alert alert-success">' + data.message + '</div>');
            },
            error: function (data) {
                var errors = data.responseJSON;
                errors = errors.message;
                $.each(errors, function (index, val) {
                    $(thisForm).find('[name=' + '' + index + '' + ']').parent().addClass('has-error');
                    $(thisForm).find('[name=' + '' + index + '' + ']').parent().append('<small class="error ajaxMessage">' + val + '</small>');
                });

            },
            complete: function () {
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled', false);
            },
        });//ajax

        return false;


    });//ajax-form-request

        $(document).on('click', '.ajax-pagination .pagination a', function (event) {
            event.preventDefault();
            var pageinate = $(this).attr('href');
            $.get(pageinate, function (data) {
                $('.loop').html(data);
            });
            return false;
        });//pagination
//////////////////  add by ajax //////////////
        $(document).on('submit', '.ajax-form-request', function (event) {
            event.preventDefault();
            $('.ajaxMessage').remove();
            $('*').removeClass('has-error');
            var thisForm = $(this);
            var formAction = thisForm.attr('action');
            var formMethod = thisForm.attr('method');
            // var formData   =  thisForm.serialize();
            var formData = new FormData(this);

            $.ajax({
                url: formAction,
                type: formMethod,
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled', 'disabled');
                },
                success: function (data) {
                    if (data.status == 'true') {
                        $('.message').prepend('<div class=" ajaxMessage alert alert-success rtl">' + data.message + '</div>');
                        $(thisForm).trigger("reset");
                    }
                    else {
                        $('.message').prepend('<div class="alert alert-danger ajaxMessage rtl">' + data.message + '</div>');
                    }

                },
                error: function (data) {
                    var errors = data.responseJSON;
                    errors = errors.errors;
                    $.each(errors, function (index, val) {
                        $(thisForm).find('[name=' + '' + index + '' + ']').parent().addClass('has-error');
                        $(thisForm).find('[name=' + '' + index + '' + ']').parent().parent().append('<small class="error ajaxMessage">' + val + '</small>');
                    });

                },
                complete: function () {
                    $(thisForm).find('#submit').find('.fa').remove();
                    $(thisForm).find('#submit').prop('disabled', false);
                },
            });//ajax

            return false;


        });//ajax-form-request
//========= serarch input ================//
        $(document).on('keyup', '.searchInput', function (event) {
            event.preventDefault();
            var thisInput = $(this);
            var thisForm = $(this).closest('form');
            var formAction = thisForm.attr('action');
            var formMethod = thisForm.attr('method');
            var formData = thisForm.serialize();
            $.ajax({
                url: formAction,
                type: formMethod,
                dataType: 'json',
                data: formData,
                success: function (data) {
                    $('.data').html(data);
                },
            });
            return false;

        });
    });
    $(document).on('click', '.cc', function (event) {
        var thisid = $(this).attr('id');
        if (!$(this).is(':checked')) {
            $("." + thisid).prop("checked", false);
        }
        else {
            $("." + thisid).prop("checked", true);
        }

    });






