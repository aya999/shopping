/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
    var pathArray = window.location.pathname.split( '/' );
    var segment_1 = pathArray[1];
    if(segment_1 == 'en' || segment_1 == 'ar'){
        config.language = segment_1;
    }
    else{
        config.language = 'en';

    }
	// config.uiColor = '#AADC6E';
};
