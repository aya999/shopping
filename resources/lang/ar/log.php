<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Api Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'added_account'=>'اضاف حساب جديده',
    'updated_account'=>'حدث بيانات حساب',
    'deleted_account'=>'حذف حساب',

    'added_product'=>'اضاف منتج جديد',
    'updated_product'=>'حدث بيانات المنتج',
    'deleted_product'=>'خذف المنتج ',

    'added_country'=>'اضاف بلد جديده',
    'updated_country'=>'حدث البلد',
    'deleted_country'=>'حذف البلد',

    'added_city'=>'اضاف مدينه جديده',
    'updated_city'=>'حدث بيانات المدينه',
    'deleted_city'=>'حذف المدينه',

    'added_admin'=>'اضاف مستخدم جديد',
    'updated_admin'=>'حدث بيانات مستخدم',
    'deleted_admin'=>'حذف مستخدم',

    'added_role'=>'اضاف صلاحيه جديده',
    'updated_role'=>'حدث بيانات صلاحيه',
    'deleted_role'=>'حذف صلاحيه',

    'added_user'=>'اضاف عميل جديد',
    'updated_user'=>'حدث بيانات عميل',
    'deleted_user'=>'حذف عميل',

    'added_store'=>'اضاف متجر جديد',
    'updated_store'=>'حدث بيانات متجر',
    'deleted_store'=>'حذف متجر',

    'added_balance'=>'اضاف باقه جديده',
    'updated_balance'=>'حدث بيانات باقه',
    'deleted_balance'=>'حذف باقه',

    'added_brand'=>'اضاف براند جديده',
    'updated_brand'=>'حدث بيانات براند',
    'deleted_brand'=>'حذف براند',

    'added_category'=>'اضاف صنف جديد',
    'updated_category'=>'حدث بيانات صنف',
    'deleted_category'=>'حذف صنف',

    'added_package'=>'اضاف حزمه جديده',
    'updated_package'=>'حدث بيانات حزمه',
    'deleted_package'=>'حذف حزمه',

    'updated_setting'=>'حدث بيانات الاعدادات',

    'added_slider'=>'اضاف سليدر جديد',
    'updated_slider'=>'حدث بيانات سليدر',
    'deleted_slider'=>'حذف سليدر',

    'added_web_page'=>'اضاف صفحه جديده',
    'updated_web_page'=>'حدث بيانات صفحه',
    'deleted_web_page'=>'حذف صفحه',


]
?>