<?php

return [
    'done_add'=>'تم اضافه عنصر جديد',
    'done_edit'=>'تم تعديل بيانات العنصر بنجاح',
    'done_delete'=>'تم حذف العنصر',

    'error_add'=>'هناك مشكله فى اضافه العنصر',
    'error_edit'=>'هناك فشل فى تعديل البيانات',
    'error_delete'=>'هناك مشكله فى حذف العنصر',
];
