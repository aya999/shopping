<?php

return [
    'added_account' => 'Added new Account',
    'updated_account' => 'Updated Account',
    'deleted_account' => 'Deleted Account',

    'added_product' => 'Added Product',
    'updated_product' => 'Updated Product',
    'deleted_product' => 'Deleted Product',

    'updated_api' => 'Updated Api',

    'added_country' => 'Added Country',
    'updated_country' => 'Updated Country',
    'deleted_country' => 'Deleted Country',

    'added_city' => 'Added City',
    'updated_city' => 'Updated City',
    'deleted_city' => 'Deleted City',

    'added_admin' => 'Added new  Admin',
    'updated_admin' => 'Updated Admin',
    'deleted_admin' => 'Deleted Admin',

    'added_role' => 'Added Role',
    'updated_role' => 'Updated Role',
    'deleted_role' => 'Deleted Role',

    'added_user' => 'Added User',
    'updated_user' => 'Updated User',
    'deleted_user' => 'Deleted User',

    'added_store' => 'Added Store',
    'updated_store' => 'Updated Store',
    'deleted_store' => 'Deleted Store',

    'added_balance' => 'Added Balance',
    'updated_balance' => 'Updated Balance',
    'deleted_balance' => 'Deleted Balance',

    'added_brand' => 'Added Brand',
    'updated_brand' => 'Updated Brand',
    'deleted_brand' => 'Deleted Brand',

    'added_category' => 'Added Category',
    'updated_category' => 'Updated Category',
    'deleted_category' => 'Deleted Category',

    'added_package' => 'Added Package',
    'updated_package' => 'Updated Package',
    'deleted_package' => 'Deleted Package',

    'updated_setting' => 'Updated Setting',

    'added_slider' => 'Added Slider',
    'updated_slider' => 'Updated Slider',
    'deleted_slider' => 'Deleted Slider',

    'added_web_page' => 'Added Website Page',
    'updated_web_page' => 'Updated Website Page',
    'deleted_web_page' => 'Deleted Website Page',



    'updated_api'=>'Updated Api'
]
?>