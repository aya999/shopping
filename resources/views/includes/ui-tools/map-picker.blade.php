<!--
   to use this tool please :
   - add [ id=us2-address ] to the field contains address
   - add [ id=us2-lat ] to the field contains latitude
   - add [ id=us2-long ] to the field contains longitude
-->

@php
    if(isset($row))
    {
       $long=$row->long;
       $lat=$row->lat;
    }
    else{

       $long="31.239034881591806";
       $lat="30.053025005431437";
    }
@endphp

<div class="form-group col-xs-12 has-float-label">
    <div id="map-pick-location" style="height: 400px"></div>
</div>


@section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN2d1fqFelylJlBQAD0a0r80FUPSXw7Aw&libraries=places&sensor=false"></script>
    {!! Html::script('assets/js/admin/locationpicker.jquery.js') !!}
    <script>
        $(document).ready(function () {

            var autocomplete = new google.maps.places.Autocomplete($("#us2-address")[0], {});

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                var lat = place.geometry.location.lat(),
                    lng = place.geometry.location.lng();
                $('#us2-lat').val(lat);
                $('#us2-long').val(lng);
            });

        });


        $('#map-pick-location').locationpicker({
            location: {

                latitude: "{{ $lat }}",
                longitude: "{{ $long }}"
            },
            locationName: "",
            radius: 500,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [],
            mapOptions: {},
            scrollwheel: true,
            inputBinding: {
                latitudeInput: $('#us2-lat'),
                longitudeInput: $('#us2-long'),
                radiusInput: $(300),
                @if(!isset($row))
                  locationNameInput: $('#us2-address')
                @endif
            },
            enableAutocomplete: false,
            enableAutocompleteBlur: false,
            autocompleteOptions: null,
            addressFormat: 'postal_code',
            enableReverseGeocode: true,
            draggable: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
            },
            onlocationnotfound: function (locationName) {
            },
            oninitialized: function (component) {
            },
            // must be undefined to use the default gMaps marker
            markerIcon: undefined,
            markerDraggable: true,
            markerVisible: true
        });

    </script>
@stop