<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic page needs
            ============================================ -->
    <title>{{ $main[$local.'_title'] }} @yield('title')</title>
    <meta charset="utf-8">
     <meta name="robots" content="index, follow"/>

    @if(isset($meta))
        <meta name="title" content="{{ $meta->meta_title }}">
        <meta name="description" content="{{ $meta->meta_desc }}">
        <meta name="keywords" content="{{ $meta->meta_tags }}">
        <meta property="og:title" content="{{ $meta->meta_title }}">
        <meta property="og:description" content="{{ $meta->meta_desc }}">
    @else
        <meta name="title" content="{{ $main[$local . '_meta_title'] }}">
        <meta name="description" content="{{ $main[$local . '_meta_desc'] }}">
        <meta name="keywords" content="{{ $main[$local . '_meta_tags'] }}">
        <meta property="og:title" content="{{ $main[$local . '_meta_title'] }}">
        <meta property="og:description" content="{{ $main[$local . '_meta_desc'] }}">
    @endif
    <!-- Mobile specific metas
            ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon
            ============================================ -->
    <link rel="shortcut icon" type="{{ asset('assets/frontend') }}/image/png" href="ico/favicon-16x16.png"/>
    <!-- Libs CSS
            ============================================ -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/'.$local) }}/bootstrap/css/bootstrap.min.css">
    <link href="{{ asset('assets/frontend/') }}/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/') }}/js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/lib.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/') }}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Theme CSS

           ============================================ -->
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so_searchpro.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so_megamenu.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-categories.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-newletter-popup.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/footer/footer3.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/wizard/css') }}/jquery.multiselect.css" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/'.$local) }}/header/header3.css" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/'.$local) }}/theme.css" rel="stylesheet">
    @yield('styles')
    <link href="{{ asset('assets/frontend/css/'.$local) }}/responsive.css" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/') }}/datepicker.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/main.css" rel="stylesheet">
    <!-- Google web fonts
            ============================================ -->
    <link href="{{ asset('assets/frontend/css/'.$local) }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    {!! Html::style('assets/common/css/sweetalert.css') !!}
    <link href="{{ asset('assets/frontend/css/') }}/main.css" rel="stylesheet">

    <style>


    </style>
</head>
<body class="common-home res layout-3">
<div id="wrapper" class="wrapper-fluid banners-effect-10">
    <!-- Header Container  -->
    <header id="header" class=" typeheader-3">

        <!-- Header Top -->
    {{--
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-4 col-md-4 col-sm-6 col-xs-4 hidden-xs">
                            <ul class="socials top-link list-inline ">
                                @if(isset($main->facebook))
                                    <li class="facebook"><a href="{{ $main->facebook }}" class="facebook" target="_blank"><i
                                                    class="fa fa-facebook"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->twitter))
                                    <li class="twitter"><a href="{{ $main->twitter }}" target="_blank"><i
                                                    class="fa fa-twitter"></i></a></li>
                                @endif
                                @if(isset($main->instagram))
                                    <li class="twitter"><a href="{{ $main->instagram }}" target="_blank"><i
                                                    class="fa fa-instagram"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->google))
                                    <li class="google"><a href="{{ $main->google }}" target="_blank"><i
                                                    class="fa fa-google"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->youtube))
                                    <li class="google"><a href="{{ $main->youtube }}" target="_blank"><i
                                                    class=" fa fa-youtube"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->pin))
                                    <li class="google"><a href="{{ $main->pin }}" target="_blank"><i
                                                    class="fa fa-pinterest"></i></a>
                                    </li>
                                @endif
                            </ul>

                        </div>
                        <div class="header-top-right collapsed-block col-lg-8 col-md-8 col-sm-6 col-xs-12">
                            <ul class="my-account top-link list-inline hidden-lg hidden-md ">
                                <li class="account" id="my_account">
                                    <a href="#" title="My Account " class="btn-xs dropdown-toggle" data-toggle="dropdown">
                                        <span class="hidden-xs">{{ trans('front.my_account') }}</span> <span
                                                class="fa fa-caret-down"></span>
                                    </a>
                                    <ul class="dropdown-menu ">
                                        <li><a href="register.html"><i class="fa fa-user"></i> {{ trans('front.register') }}
                                            </a></li>
                                        <li><a href="login.html"><i
                                                        class="fa fa-pencil-square-o"></i> {{ trans('front.login') }}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="top-link list-inline lang-curr">
                                @if(Auth::check())
                                    <li class="welcome hidden-xs hidden-sm">
                                        <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                           data-toggle="dropdown">
                                            <i class="fa fa-user"></i>
                                            {{ trans('front.welcome') }} {{ Auth::user()->name }}! <span
                                                    class="fa fa-caret-down"></span></a>
                                        <ul class="dropdown-menu ">
                                            <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-user-circle-o"></i> {{ trans('front.profile') }}
                                                </a>
                                            </li>
                                            <li><a href="{{url($localization.'/profile/products')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-shopping-bag"></i> {{ trans('front.my_adds') }}</a>
                                            </li>
                                            <li><a href="{{url($localization.'/profile/followers')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-users"></i>{{ trans('front.my_followers') }}</a>
                                            </li>
                                            <li><a href="{{url($localization.'/profile/following')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-user-plus"></i>{{ trans('front.my_followings') }}
                                                </a>
                                            </li>
                                            <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-list"></i>{{ trans('front.my_orders') }}</a>
                                            </li>
                                            <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                            class="fa fa-lock"></i>{{ trans('front.change_password') }}</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" hidden-sm hidden-xs">
                                        <a href="{{url($localization.'/profile/likes')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}" id="wishlist-total" class="top-link-wishlist" title="Wish List (0) ">
                                            <i class="fa fa-heart-o"></i> {{ trans('front.wishlist') }}
                                        </a>
                                    </li>
                                    <li class=" hidden-sm hidden-xs">
                                        <a href="{{ url($localization.'/logout') }}" id="wishlist-total" class="top-link-wishlist" title="Wish List (0) ">
                                            <i class="fa fa-sign-out"></i> {{ trans('front.logout') }}
                                        </a>
                                    </li>
                                @else
                                    <li class="hidden-xs  hidden-sm"><a  href="{{ url($localization.'/login') }}">{{ trans('front.login') }}</a>
                                    </li>
                                    <li class="hidden-xs  hidden-sm"><a   href="{{ url($localization.'/register') }}" >{{ trans('front.register') }}</a>
                                    </li>
                                @endif

                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="index.php" method="post" enctype="multipart/form-data"
                                              id="bt-language">
                                            <a class="btn-link dropdown-toggle" data-toggle="dropdown" >
                                                <img  data-toggle="dropdown" @if($local == "en") src="{{ asset('assets/frontend/') }}/image/catalog/flags/gb.png" @else src="{{ asset('assets/frontend/') }}/image/catalog/flags/ar.png" @endif
                                                     alt="{{ trans('front.lang') }}"
                                                     title="{{ trans('front.lang') }}">
                                                <span class="">{{ trans('front.lang') }}</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{ url('/trans/en/ar') }}"><img class="image_flag"
                                                                             src="{{ asset('assets/frontend') }}/image/catalog/flags/gb.png"
                                                                             alt="{{ trans('front.english') }}"
                                                                             title="{{ trans('front.english') }}"/> {{ trans('front.english') }}
                                                    </a></li>
                                                <li><a href="{{ url('/trans/ar/en') }}"> <img class="image_flag"
                                                                                             src="{{ asset('assets/frontend') }}/image/catalog/flags/ar.png"
                                                                                             alt="{{ trans('front.arabic') }}"
                                                                                             title="{{ trans('front.arabic') }}"/>
                                                        {{ trans('front.arabic') }} </a></li>
                                            </ul>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    --}}
    <!-- //Header Top -->
    @include('includes.frontend.navbar')
    <!-- Header Bottom -->
        <div class="header-bottom hidden-compact">
            <div class="container">
                <div class="row">
                    <div class="bottom1 menu-vertical col-lg-3 col-md-3 ">
                        <!-- Secondary menu -->
                        <div class="responsive so-megamenu  megamenu-style-dev all_cat_">
                            <div class="so-vertical-menu ">
                                <nav class="navbar-default">
                                    <div class="container-megamenu vertical">
                                        <div id="menuHeading">
                                            <div class="megamenuToogle-wrapper">
                                                <div class="megamenuToogle-pattern">
                                                    <div class="container">
                                                        <div>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        {{ trans('front.all_categories') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="navbar-header">
                                            <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                <i class="fa fa-bars"></i>
                                                <span> {{ trans('front.all_categories') }}     </span>
                                            </button>
                                        </div>
                                        @yield('all_categories','')
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- // end Secondary menu -->
                    </div>
                    <!-- Main menu -->
                    <div class="main-menu col-lg-9 col-md-9">
                        <div class="responsive so-megamenu megamenu-style-dev ">
                            <nav class="navbar-default">
                                <div class=" container-megamenu  horizontal open ">
                                    <div class="navbar-header">
                                        <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="megamenu-wrapper">
                                        <span id="remove-megamenu" class="fa fa-times"></span>
                                        <div class="megamenu-pattern">
                                            <div class="container-mega" style="width:104%">
                                                <ul class="megamenu" data-transition="slide" data-animationtime="250">
                                                    <li class="home hover">
                                                        <a href="{{ url('/') }}"><i  style="padding-right:0px" class="fa fa-home"></i> {{ trans('front.home') }}</a>
                                                    </li>
                                                    <li class="home hover">
                                                        <a href="#"><i style="padding-right:0px"  class="fa fa-shopping-basket"></i>
                                                            <b class="caret"></b>{{ trans('front.buy_online') }}</a>
                                                        @if(count($online_cats) > 0)

                                                            <div class="sub-menu" style="width:100%;">
                                                                <div class="content">
                                                                    <div class="row">
                                                                        @foreach($online_cats as $cat)
                                                                            <div class="col-md-2">
                                                                                <a href="index.html" class="image-link">
                                                                                    <span class="thumbnail">
                                                                                    <img class="img-responsive img-border"
                                                                                         src="{{ asset('/assets/images/categories/web/'.$cat->image) }}"
                                                                                         alt="">
                                                                                    </span>
                                                                                    <h3 class="figcaption">{{ $cat->name }}</h3>
                                                                                </a>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </li>
                                                    <li class="home hover">
                                                        <a href="#"><i style="padding-right:0px" class="fa fa-bank"></i> <b
                                                                    class="caret"></b>{{ trans('front.stores') }}</a>
                                                        {{--   @php
                                                               dd($stores_cats);
                                                           @endphp--}}
                                                        @if(count($stores_cats) > 0)
                                                            <div class="sub-menu" style="width:100%;">
                                                                <div class="content">
                                                                    <div class="row">
                                                                        @foreach($stores_cats as $cat)
                                                                            <div class="col-md-2">
                                                                                <a href="{{url($localization.'')}}/stores/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}" class="image-link">
                                                                                    <span class="thumbnail">
                                                                                    <img class="img-responsive img-border"
                                                                                         src="{{ asset('/assets/images/categories/web/'.$cat->image) }}"
                                                                                         alt="">
                                                                                    </span>
                                                                                    <h3 class="figcaption">{{ $cat->name }}</h3>
                                                                                </a>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </li>
                                                    @if(Auth::check())
                                                        <li class="home hover">
                                                            <a href="{{url($localization.'/profile/products/'. Auth::id() .'/'. str_replace(' ', '_', Auth::user()->name))  }}">
                                                                <i class="fa fa-product-hunt"> {{ trans('front.user_adds') }}</i></a>

                                                        </li>
                                                    @endif


                                                </ul>

                                            </div>
                                            <div class="header-top-right collapsed-block">
                                                <ul class="my-account top-link list-inline hidden-lg hidden-md ">
                                                    <li class="account" id="my_account">
                                                        <a href="#" title="My Account " class="btn-xs dropdown-toggle" data-toggle="dropdown">
                                                            <span class="hidden-xs">{{ trans('front.my_account') }}</span> <span
                                                                    class="fa fa-caret-down"></span>
                                                        </a>
                                                        <ul class="dropdown-menu ">
                                                            <li><a href="register.html"><i class="fa fa-user"></i> {{ trans('front.register') }}
                                                                </a></li>
                                                            <li><a href="login.html"><i
                                                                            class="fa fa-pencil-square-o"></i> {{ trans('front.login') }}</a>
                                                            </li>
                                                        </ul>
                                                    </li>

                                                </ul>
                                                <ul class="top-link list-inline lang-curr nav_float "  style="">
                                                    @if(Auth::check())
                                                        <li class="welcome hidden-xs hidden-sm">
                                                            <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                                               data-toggle="dropdown">
                                                                <i class="fa fa-user"></i>
                                                                {{ trans('front.welcome') }} {{ Auth::user()->name }}! <span
                                                                        class="fa fa-caret-down"></span></a>
                                                            <ul class="dropdown-menu ">
                                                                <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-user-circle-o"></i> {{ trans('front.profile') }}
                                                                    </a>
                                                                </li>
                                                                <li><a href="{{url($localization.'/profile/products')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-shopping-bag"></i> {{ trans('front.my_adds') }}</a>
                                                                </li>
                                                                <li><a href="{{url($localization.'/profile/followers')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-users"></i>{{ trans('front.my_followers') }}</a>
                                                                </li>
                                                                <li><a href="{{url($localization.'/profile/following')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-user-plus"></i>{{ trans('front.my_followings') }}
                                                                    </a>
                                                                </li>
                                                                <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-list"></i>{{ trans('front.my_orders') }}</a>
                                                                </li>
                                                                <li><a href="{{url($localization.'/profile')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}"><i
                                                                                class="fa fa-lock"></i>{{ trans('front.change_password') }}</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class=" hidden-sm hidden-xs">
                                                            <a href="{{url($localization.'/profile/likes')}}/{{ Auth::user()->id }}/{{ str_replace(' ', '_',  Auth::user()->name)  }}" id="wishlist-total" class="top-link-wishlist" title="Wish List (0) ">
                                                                <i class="fa fa-heart-o"></i> {{ trans('front.wishlist') }}
                                                            </a>
                                                        </li>
                                                        <li class=" hidden-sm hidden-xs">
                                                            <a href="{{ url($localization.'/logout') }}" id="wishlist-total" class="top-link-wishlist" title="Wish List (0) ">
                                                                <i class="fa fa-sign-out"></i> {{ trans('front.logout') }}
                                                            </a>
                                                        </li>
                                                    @else
                                                        <li class="hidden-xs  hidden-sm"><a  href="{{ url($localization.'/login') }}">{{ trans('front.login') }}</a>
                                                        </li>
                                                        <li class="hidden-xs  hidden-sm"><a   href="{{ url($localization.'/register') }}" >{{ trans('front.register') }}</a>
                                                        </li>
                                                    @endif

                                                    <li class="language">
                                                        <div class="btn-group languages-block ">
                                                            <form action="index.php" method="post" enctype="multipart/form-data"
                                                                  id="bt-language">
                                                                <a class="btn-link dropdown-toggle" data-toggle="dropdown" >
                                                                    <img  data-toggle="dropdown" @if($local == "en") src="{{ asset('assets/frontend/') }}/image/catalog/flags/gb.png" @else src="{{ asset('assets/frontend/') }}/image/catalog/flags/ar.png" @endif
                                                                    alt="{{ trans('front.lang') }}"
                                                                          title="{{ trans('front.lang') }}">
                                                                    <span class="">{{ trans('front.lang') }}</span>
                                                                    <span class="fa fa-angle-down"></span>
                                                                </a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="{{ url('/trans/en/ar') }}"><img class="image_flag"
                                                                                                                 src="{{ asset('assets/frontend') }}/image/catalog/flags/gb.png"
                                                                                                                 alt="{{ trans('front.english') }}"
                                                                                                                 title="{{ trans('front.english') }}"/> {{ trans('front.english') }}
                                                                        </a></li>
                                                                    <li><a href="{{ url('/trans/ar/en') }}"> <img class="image_flag"
                                                                                                                  src="{{ asset('assets/frontend') }}/image/catalog/flags/ar.png"
                                                                                                                  alt="{{ trans('front.arabic') }}"
                                                                                                                  title="{{ trans('front.arabic') }}"/>
                                                                            {{ trans('front.arabic') }} </a></li>
                                                                </ul>
                                                            </form>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!-- //end Main menu -->
                </div>
            </div>
        </div>
    </header>
    <!-- //Header Container  -->