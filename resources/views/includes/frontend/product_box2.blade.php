<div class="product-item-container">
    <div class="left-block">
        <div class="product-image-container second_img">
            <a href="{{ url($localization.'')}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product[$local . '_name']) }}" target="_self" title="{{$product[$local.'_name']}}">
                @if($product->image == "default.png")
                    <img  style="height: 157px;" src="{{asset('assets/images/products/thumbnail/')}}/{{$product->image }}" class="img-1 img-responsive" alt="{{$product->name}}">

                @else

                    <img src="{{asset('assets/images/products/thumbnail/')}}/{{$product->image }}" class="img-1 img-responsive" alt="{{$product->name}}">
                @endif
                <img src="{{asset('assets/images/products/thumbnail/')}}/{{$product->image }}" class="img-2 img-responsive" alt="{{$product->name}}">
            </a>
        </div>
        <div class="box-label"> <span class="label-product label-sale"> -16% </span></div>
        <div class="button-group so-quickview cartinfo--left ">
            <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('{{ $product->id }}',1,'{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($localization.'/products/cart/add') }}','{{ url($localization.'/products/cart/small') }}');">  <i class="fa fa-shopping-basket"></i>
                <span>Add to cart </span>
            </button>

            @if(Auth::check())
                @if(Auth::user()->isProductFavorite($product->id))
                    <button type="button"
                            class="wishlist btn-button "
                            title="Add to Wish List"
                            onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ trans("front.product_added_wishlist_desc")  }}','{{ url($local.'/products/like') }}');">
                        <i
                                class="fa fa-heart" id="span{{ $product->id }}"></i><span
                                id="message{{ $product->id }}">{{ trans('add_to_wishlist') }}</span>
                    </button>
                @else
                    <button type="button"
                            class="wishlist btn-button "
                            title="Add to Wish List"
                            onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ trans("front.product_added_wishlist_desc")  }}','{{ url($local.'/products/like') }}');">
                        <i
                                class="fa fa-heart-o" id="span{{ $product->id }}"></i><span
                                id="message{{ $product->id }}">{{ trans('remove_from_wishlist') }}</span>
                    </button>
                @endif
            @else
                <button type="button"
                        class="wishlist btn-button "
                        title="Add to Wish List"
                        onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ trans("front.product_added_wishlist_desc")  }}','{{ url($local.'/products/like') }}');">
                    <i
                            class="fa fa-heart" id="span{{ $product->id }}"></i><span
                            id="message{{ $product->id }}">{{ trans('add_to_wishlist') }}</span>
                </button>
            @endif

            <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
            </button>
            <!--quickview-->
            <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="{{ url($localization.'/product_quick_view/'.$product->id) }}" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
            <!--end quickview-->
        </div>
    </div>
    <div class="right-block">
        <div class="caption">
            <h4><a href="{{ url($localization.'')}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product[$local .'_name']) }}" title="{{$product[$local.'_name']}}" target="_self">{{$product[$local.'_name']}}</a></h4>
            <div class="rating">
                @for($i=0;$i <intval($product->rate());$i++)
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                    @if($i==5)
                        @break
                    @endif
                @endfor
                @for($j=$i ;$j<5 ;$j++)
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                @endfor

            </div>
            <div class="price">

                <span class="price-new">$ {{$product->price}}</span>
            </div>
            <div class="description item-desc">
                <p>{{$product[$local.'_brief_desc']}}</p> </div>
        </div>
        <div class="list-block">
            <div class="item-available">
                <div class="row">
                    <p class="col-xs-12 a1">Available: <b>{{$product->quantity}}</b> </p>

                </div>
            </div>
            <div class="price">
                <span class="price-new">$ {{$product->price}}</span>
            </div>

            <button class="addToCart btn-button  btn-block" type="button" title="Add to Cart" onclick="cart.add('{{ $product->id }}',1,'{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($localization.'/products/cart/add') }}','{{ url($localization.'/products/cart/small') }}');">
                <i class="fa fa-shopping-basket"></i>
                <span> {{trans('front.add_cart')}}</span>
            </button>


            <button class="compare btn-button btn-block" type="button" title="Add to Wish List" onclick="compare.add('101');"><i class="fa fa-heart"></i>
                {{trans('front.wish_list')}}
            </button>

        </div>
        <a class="iframe-link btn-button quickview quickview_handler visible-lg hidden" href="{{ url($localization.'/product_quick_view/'.$product->id) }}"  title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span></span></a>
    </div>
</div>