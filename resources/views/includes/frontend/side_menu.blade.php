
    <!-- Secondary menu -->
    <div class="responsive so-megamenu  ">
        <div class=" ">
            <nav class="navbar-default">
                <div class="container-megamenu vertical">
                    <div class="vertical-wrapper">
                        <span id="remove-verticalmenu" class="fa fa-times"></span>
                        <div class="megamenu-pattern">
                            <div class="container-mega">
                                <ul class="megamenu">
                                    @foreach($cats as $i=>$cat)
                                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                                            @if($i > 9) style="display: none" @endif>
                                            <p class="close-menu"></p>
                                            <a href="{{url($localization.'')}}/category/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}" class="clearfix">
                                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                                     alt="icon">
                                                <span>{{ $cat->name }}</span>
                                                <b class="caret"></b>
                                            </a>
                                            @if(count($cat->categories) > 0)
                                                <div class="sub-menu" data-subwidth="60">
                                                    <div class="content">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    @foreach($cat->categories as $category)
                                                                        <div class="col-md-4 static-menu">
                                                                            <div class="menu">
                                                                                <ul>
                                                                                    <li>
                                                                                        <a href="{{url($localization.'')}}/category/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                                        <ul>
                                                                                            @foreach($category->categories as $child)
                                                                                                <li>
                                                                                                    <a href="{{url($localization.'')}}/category/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                                </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                    @if(count($cats) > 10)
                                        <li class="loadmore">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="more-view">More Categories</span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- // end Secondary menu -->
