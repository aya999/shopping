<!-- Footer Container -->
<footer class="footer-container typefooter-3">
    <!-- Footer Top Container -->
    <section class="footer-top ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-style">
                    <div class="infos-footer box-footer">
                        <div class="module">
                            <h3 class="modtitle">{{trans('front.about_store')}}</h3>
                            <ul>
                                <li class="adres">{{ $main->address }}.</li>
                                <li class="mail">
                                    <a href="mailto:{{ $main->email }}">{{ $main->email }}</a>
                                </li>
                                <li class="phone">{{ $main->mobile }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-style">
                    <div class="box-information box-footer">
                        <div class="module clearfix">
                            <h3 class="modtitle">{{trans('front.pages')}}</h3>
                            <div class="modcontent">
                                <ul class="menu">
                                    @foreach($footer_pages as $page)
                                        <div class="col-lg-6 col-md-6">
                                            <li><a href="{{ url($localization. '/' . $page->id . '/' .str_replace(' ', '-', $page->name)) }}">{{ $page->name }}</a></li>
                                        </div>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-style">
                    <div class="module  newsletter box-footer">
                        <h3 class=" modtitle"><span>{{trans('front.newsletter')}}</span></h3>
                        <p></p>
                        <div class="block_content">
                            <form method="post" id="signup" name="signup"
                                  class="form-group form-inline signup send-mail">
                                <div class="form-group">
                                    <div class="subcribe">
                                        <button class="btn btn-primary btn-default font-title" type="submit"
                                                onclick="return subscribe_newsletter();" name="submit">
                                            {{trans('front.sign_in')}}
                                        </button>
                                    </div>
                                    <div class="input-box">
                                        <input type="email" placeholder="{{trans('dashboard.email')}}" value=""
                                               class="form-control" id="txtemail" name="txtemail" size="55">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box-socials ">
                            <h3 class="modtitle">{{trans('front.follow_social')}}</h3>
                            <ul class="socials">
                                @if(isset($main->facebook) && !empty($main->facebook))
                                    <li class="facebook"><a class="_blank" href="{{ $main->facebook }}"
                                                            target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->twitter) && !empty($main->twitter))

                                    <li class="twitter"><a class="_blank" href="{{ $main->twitter }}"
                                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->google) && !empty($main->google))

                                    <li class="google_plus"><a class="_blank"
                                                               href="{{ $main->google }}"
                                                               target="_blank"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->instagram) && !empty($main->instagram))

                                    <li class="twitter"><a class="_blank"
                                                             href="{{ $main->instagram }}"
                                                             target="_blank"><i class="fa fa-instagram"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->pin) && !empty($main->pin))

                                    <li class="pinterest"><a class="_blank"
                                                             href="{{ $main->pin }}"
                                                             target="_blank"><i class="fa fa-pinterest"></i></a>
                                    </li>
                                @endif
                                @if(isset($main->youtube) && !empty($main->youtube))

                                    <li class="pinterest"><a class="_blank"
                                                             href="{{ $main->youtube }}"
                                                             target="_blank"><i class="fa fa-youtube"></i></a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Footer Top Container -->

    <!-- Footer Bottom Container -->
    <section class="footer-bottom ">
        <div class="container">
            <div class="row">
                <div class="content-footer-bottom">
                    <div class="col-lg-6 col-md-7 col-sm-12 col-xs-12 copyright-b">
                        <div class="copyright">© 2018 Bestsmarket. All Rights Reserved.
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 payment-w pull-right">
                        <img src="{{ asset('assets/frontend') }}/image/catalog/demo/payment/payment.png"
                             alt="imgpayment">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Footer Bottom Container -->
    <!--Back To Top-->
    <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
</footer>
<!-- //end Footer Container -->
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Skin CSS</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="LoginFirst" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Login </h4>

            </div>
            <div class="modal-body text-center">
                <p>Please . you should login first .</p>
                <a href="{{ url('login') }}" class="btn btn-primary ">Login</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- End Color Scheme
            ============================================ -->
@include('includes.frontend.scripts')
</body>
</html>
