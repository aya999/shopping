<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-2.2.4.min.js"></script>



<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/bootstrap.min.js"></script>
@yield('scripts3')

<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/libs.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript"
        src="{{ asset('assets/frontend') }}/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/datetimepicker/moment.js"></script>
<script type="text/javascript"
        src="{{ asset('assets/frontend') }}/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/modernizr/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/minicolors/jquery.miniColors.min.js"></script>
<!-- Theme files
            ============================================ -->
@yield('scripts2')

<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/application.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/homepage.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/toppanel.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/addtocart.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/cpanel.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/ajax.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/main.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/custom.js"></script>
{!! Html::script('assets/common/js/sweetalert.min.js') !!}
<script type="text/javascript" src="{{ asset('assets/frontend/wizard') }}/js/jquery.multiselect.js"></script>

<script>
    $('select[multiple]').multiselect({
        columns: 4,
        placeholder: 'Select options'
    });
</script>
<script>
    $(document).ready(function () {
        token = "<?php echo e(csrf_token()); ?>"
        $("#customLi").css('width', '20%');

        $(document).on('click', '.ajax-btn', function (event) {
            event.preventDefault();
            id = $(this).data('id');
            link = $(this).data('link');
            type = $(this).data('type');
            swal({
                    title: "هل انت متأكد ؟",
                    text: "اذا قلت نعم فلا يمكنك التراجع!",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "لا . تراجع",

                    confirmButtonClass: "btn-danger",

                    confirmButtonText: "نعم",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        url: link ,
                        type: 'POST',
                        data: {_token: token,_method: type ,id: id}
                    }).done(function (data){
                        swal({
                                title: "تم",
                                text: "",
                                type: "success",
                            },
                            function () {
                                window.location.reload();

                            });
                    });
                });
        });


    });
</script>
@yield('scripts')
