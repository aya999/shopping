<!-- Header center -->
<div class="header-middle">
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="logo"><a href="{{ url('/') }}"><img
                                src="{{ asset('assets/frontend') }}/image/catalog/logo.png" title="Your Store"
                                alt="DaliliStore"/></a></div>
            </div>
            <!-- //end Logo -->
            <!-- Search -->
            <div class="middle2 col-lg-7 col-md-7">
                <div class="search-header-w">
                    <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                        <form method="GET" action="{{ url($localization . '/search') }}">
                            <div id="search0" class="search input-group form-group">

                                <div class="select_category filter_type  icon-select hidden-sm hidden-xs">
                                    <select multiple class="no-border" name="cities[]">
                                        {{--
                                                                                <option value="">{{ trans('front.all_cities') }}</option>
                                        --}}
                                        @foreach( $cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="select_category filter_type  icon-select hidden-sm hidden-xs">
                                    <select multiple class="no-border" name="categories[]">
                                        {{--
                                                                                <option value="">{{ trans('front.all_categories') }}</option>
                                        --}}
                                        @foreach( $cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <input class="autosearch-input form-control" type="text" value="" size="50"
                                       autocomplete="off" placeholder="{{trans('front.search')}}" name="search">
                                <span class="input-group-btn">
                                                <button style="top: 1px;" type="submit" class="button-search btn btn-primary"><i
                                                            class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- //end Search -->
            <div class="middle3 col-lg-3 col-md-3">
                <!--cart-->
                <div class="shopping_cart">
                    <div id="cart" class="btn-shopping-cart">
                        <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                           data-toggle="dropdown" aria-expanded="true">
                            <div class="shopcart">
                                                <span class="icon-c">
                                                </span>
                                <div class="shopcart-inner">
                                    <p class="text-shopping-cart">
                                        {{ trans('front.my_cart') }}
                                    </p>
                                    <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart num_cart">{{ $cart_cont }}</span> <span
                                                class="items_cart2"> <i
                                                    class="num_cart">{{ $cart_cont }} </i></span> {{ trans('front.items') }}</span>
                                    {{--
                                                                                        <span class="items_cart num_cart">2</span>
                                    --}}
                                    </span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu pull-right shoppingcart-box" role="menu" id="small_cart">
                            @include('frontend.ajax.small_cart')
                        </ul>
                    </div>
                </div>

                <!--//cart-->
                <!-- call -->
            {{-- <div class="telephone   hidden-md">
                 <span>{{ trans('front.call_us') }}</span>
                 <a href="#">{{ $main->mobile }}</a>
             </div>--}}
            <!-- //call -->
            </div>
        </div>
    </div>
</div>
<!-- //Header center -->