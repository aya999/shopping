<ul class="list-item">
    <li><a href="{{url($localization.'/profile/'. $user->id .'/'.str_replace(' ', '_', $user->name))  }}">{{ trans('front.user_profile') }}</a>
    </li>
    <li><a href="{{url($localization.'/profile/followers/'. $user->id .'/'.str_replace(' ', '_', $user->name))  }}">{{ trans('front.user_followers') }}</a>
    </li>
    <li><a href="{{url($localization.'/profile/following/'. $user->id .'/'. str_replace(' ', '_', $user->name))  }}">{{ trans('front.user_followings') }}</a>
    </li>
    <li><a href="">Change Password</a>
    </li>
    <li><a href="{{url($localization.'/profile/products/'. $user->id .'/'. str_replace(' ', '_', $user->name))  }}">{{ trans('front.user_adds') }}</a>
    <li><a href="{{url($localization.'/profile/products/add/'. $user->id .'/'. str_replace(' ', '_', $user->name))  }}">Add add</a>
    <li><a href="{{url($localization.'/profile/stores')}}/{{ $user->id }}/{{ str_replace(' ', '_', $user->name)  }}">{{ trans('front.user_stores') }}</a>
    </li>
    <li><a href="{{url($local.'/profile/likes/'. $user->id .'/'. str_replace(' ', '_', $user->name))  }}">{{ trans('front.wishlist') }}</a>
    </li>
    <li><a href="">Order History</a>
    </li>
    <li><a href="">Reward Points</a>
    </li>
</ul>