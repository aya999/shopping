@extends('layouts.email')

@section('content')

    @component('emails.plugin.greeting',['align'=>'right']) مرحبا بك  ! @endcomponent

    @component('emails.plugin.paragraph',['align'=>'right'])
لقد استلمت هذه الرسالة  لطلبك لاستعادة حسابك
    @endcomponent


    @component('emails.plugin.button', ['bg_color' => '#42A9D3', 'color' => '#FFF', 'link' =>  $actionUrl  ])
       " اعادة تعين كلمة المرور"
    @endcomponent

    @component('emails.plugin.paragraph',['align'=>'right'])
        اذا لم تقم باستعادة حسابك لايمكنك التفاعل مع تطبيقات ريسيبشن
    @endcomponent

    @component('emails.plugin.paragraph',['align'=>'right']) شكرا,<br/>
ريسيبشن
    @endcomponent

    @component('emails.plugin.paragraph',['align'=>'right'])
        اذا كان هناك مشكله فى الضغط على الزر من فضلك انسخ الرابط التالى وضعة فى المتصفح
       :<a href="{{ $actionUrl }}"> {{ $actionUrl }}</a>
    @endcomponent
@stop
@section('copy', 'جميع الحقوق محفوظة رسيسبشن.')