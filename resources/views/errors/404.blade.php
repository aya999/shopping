<html><!-- Skins Theme -->
<head>
    <title> 404 Error</title>
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/') }}/bootstrap/css/bootstrap.min.css">
    <link href="{{ asset('assets/frontend/css/') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>

<!-- 404 Error Start -->
<section id="error-404" class="padding">

    <div class="container rtl">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-4">
                <div class="error-image">
                    <img src="{{ asset('assets/images/frontend') }}/404.png" alt="image" class="img-responsive"/>
                </div>
                <div class="text-center">
                    <h1>خطأ 404 !!</h1>
                     <p>الصفحة المطلوبه غير موجودة.</p>
                    <div class="erro-button">
                        <a href="{{ url('/') }}" class="btn-blue">العودة للرئيسية</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- 404 Error End -->

</body>
</html>
