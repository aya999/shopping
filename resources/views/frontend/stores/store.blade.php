@extends('layouts.frontend.store')
@section('profile_content')
    <div id="review">
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td style="width: 50%;"><strong>{{ $store[$local.'_name'] }}</strong></td>
                <td class="text-right">
                    @foreach($store->categories as $category)
                        <a data-toggle="tab" href="#tab-1" class="st">{{ $category[$local.'_name'] }}</a>
                    @endforeach

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>{!!  $store[$local.'_desc'] !!} </p>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="text-right"></div>
    </div>
    <div class="row">
        <div class="row-product">
            <!--Left Part Start -->
            <div id="content" class="col-md-12 col-sm-12">

                <div class="products-category">

                    <div class="module">
                        <h3 class="modtitle2">{{trans('dashboard.products')}}</h3>
                    </div>
                @if(count($products)>0)
                    <!-- Filters -->
                        <div class="product-filter product-filter-top filters-panel">
                            <div class="row">


                                <div class="box-pagination col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                    <div class="content-pagination-2">
                                        {{ $products->render() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- //end Filters -->
                @endif
                <!--changed listings-->
                    <div class="products-list row nopadding-xs so-filter-list grid">
                        @foreach($products as $product)
                            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                                @include('includes.frontend.product_box2')
                            </div>
                        @endforeach
                    </div>
                    <!--// End Changed listings-->
                @if(count($products)>0)
                    <!-- Filters -->
                        <div class="product-filter product-filter-top filters-panel">
                            <div class="row">


                                <div class="box-pagination col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                    <div class="content-pagination-2">
                                        {{ $products->render() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- //end Filters -->
                    @endif
                </div>
            </div>
        </div>
    </div>



@endsection