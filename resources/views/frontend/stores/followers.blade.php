@extends('layouts.frontend.store')
@section('profile_content')
    @foreach($users as $user)
        <div class="col col-md-4">
            <div class="userBlock">
                <div class="backgrounImg">
                    <img src="{{ asset($user->bg) }}">
                </div>
                <div class="userImg">
                    <img src="{{asset($user->image) }}">
                </div>
                <div class="userDescription">
                    <h5>
                        <a href="{{url($local.'/eg/profile/'. $user->id .'/'. str_replace(' ', '_', $user->name)) }}">{{ $user->name }}</a>
                    </h5>
                    <p>{{ $user->mobile }}</p>
                    <div class="followrs">
                        <span class="number number{{ $user->id }}">{{  $user->no_follows  }}</span>
                        <span> {{ trans('front.follower') }}</span>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->isUseFollow($user->id))
                            <button class="btn btn-danger"
                                    onclick="follow.add('{{  $user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $user->id }}">{{ trans('front.unfollow') }}
                            </button>
                        @else
                            <button class="btn btn-primary"
                                    onclick="follow.add('{{  $user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $user->id }}">{{ trans('front.follow') }}
                            </button>
                        @endif
                    @else
                        <button class="btn btn-primary" data-toggle="modal"
                                data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                    @endif

                </div>

            </div>
        </div>
    @endforeach
@endsection