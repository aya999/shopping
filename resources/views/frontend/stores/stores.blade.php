@extends('layouts.frontend.master')
@section('content')
    <div class=" box-content1 ">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-left">
                @include('includes.frontend.side_menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-right">
                <ul class="breadcrumb">
                    <li><a href="{{url($localization)}}">{{trans('front.home')}}</a></li>
                    <li>
                        <a href="{{url($localization)}}/category/{{$category->id}}/{{  str_replace(' ','-',$category[$local.'_name'])}}">{{$category[$local.'_name']}}</a>
                    </li>
                </ul>
                <div class="row">
                    <div class="row-product">
                        <div id="content">
                            <div class="products-category">

                                <!--changed listings-->
                                <div class="products-list row nopadding-xs so-filter-gird list">
                                    @foreach($stores as $store)
                                        <div class="product-layout col-lg-15 col-md-4 col-sm-6 col-xs-12">
                                            <div class="product-item-container">
                                                <div class="left-block" style=" height: 200px;">
                                                    <div class="product-image-container second_img">
                                                        <a href="{{url($localization)}}/store/{{$store->id}}/{{  str_replace(' ','-',$store->name)}}"
                                                           target="_self" title="{{ $store->name }}">
                                                            <img src="{{ asset('/assets/images/stores/web/'.$store->image) }}"
                                                                 style=" height: 200px;width: 100%;"
                                                                 class="img-1 img-responsive" alt="image">
                                                            <img src="{{ asset('/assets/images/stores/web/'.$store->image) }}"
                                                                 style=" height: 200px;width: 100%;"
                                                                 class="img-2 img-responsive" alt="image">
                                                        </a>
                                                    </div>

                                                </div>
                                                <div class="right-block" style=" height: 200px;">
                                                    <div class="caption">
                                                        <h4>
                                                            <a href="{{url($localization)}}/store/{{$store->id}}/{{  str_replace(' ','-',$store->name)}}"
                                                               title="{{ $store->name }}"
                                                               target="_self">{{ $store->name }}</a></h4>
                                                        <div class="rating">
                                                            @for($i=0;$i <intval($store->rate());$i++)
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                @if($i==5)
                                                                    @break
                                                                @endif
                                                            @endfor
                                                            @for($j=$i ;$j<5 ;$j++)
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            @endfor
                                                        </div>
                                                        <div class="description item-desc">
                                                            <p>
                                                            <ul>
                                                                @if(!empty($store->address))
                                                                    <li class="adres"><i
                                                                                class="fa fa-home"></i> {{ $store->address }}
                                                                        .
                                                                    </li>
                                                                @endif
                                                                @if(!empty($store->city_id))
                                                                    <li class="adres"><i
                                                                                class="fa fa-map-marker"></i> {{ $store->city[$local .'_name'] }}
                                                                        .
                                                                    </li>
                                                                @endif
                                                                @if(!empty($store->email))

                                                                    <li class="mail">
                                                                        <a href="mailto:{{ $store->email }}"><i
                                                                                    class="fa fa-envelope-open"></i> {{ $store->email }}
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                                @if(!empty($store->mobile))

                                                                    <li class="phone"><i
                                                                                class="fa fa-phone"></i> {{ $store->mobile }}
                                                                    </li>
                                                                @endif
                                                                <li class="phone"><i
                                                                            class="fa fa-user"></i> {{ $store->user->name }}
                                                                </li>

                                                            </ul>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="list-block">
                                                        <div class="item-available">
                                                            <div class="row">
                                                                <p class="col-xs-12 a1"> {{ trans('followers') }}:
                                                                    <b>{{ $store->follows }}</b>
                                                                <p class="col-xs-12 a1">{{ trans('adds') }}:
                                                                    <b>{{ $store->addsCount() }}</b>
                                                                </p>

                                                            </div>
                                                        </div>

                                                        <button class="addToCart btn-button  btn-block" type="button"
                                                                title="Add to Cart" onclick="cart.add('101', '1');">
                                                            <i class="fa fa-shopping-basket"></i>
                                                            <span> Chat </span>
                                                        </button>

                                                        <button class="compare btn-button  btn-block" type="button"
                                                                title="Compare this Product"
                                                                onclick="compare.add('101');">
                                                            <i class="fa fa-refresh"></i>
                                                            <span>Follow</span>
                                                        </button>
                                                    </div>
                                                    <!--quickview-->
                                                    {{--<a class="iframe-link btn-button quickview quickview_handler visible-lg hidden"--}}
                                                    {{--href="quickview.html" title="Quick view"--}}
                                                    {{--data-fancybox-type="iframe"><i--}}
                                                    {{--class="fa fa-eye"></i><span></span></a>--}}
                                                    {{--<!--end quickview-->--}}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                <!--// End Changed listings-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
