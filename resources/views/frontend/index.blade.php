@extends('layouts.frontend.master')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/home3.css" rel="stylesheet">
@endsection
@php
    $c=0;
@endphp
@section('content')

    <!-- Main Container  -->

    <div class=" box-content1 ">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-left">
                @include('includes.frontend.side_menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-right">
                <div class="module sohomepage-slider ">
                    <div class="yt-content-slider" data-rtl="yes" data-autoplay="yes" data-autoheight="no"
                         data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1"
                         data-items_column1="1" data-items_column2="1" data-items_column3="1"
                         data-items_column4="1" data-arrows="no" data-pagination="yes" data-lazyload="yes"
                         data-loop="yes" data-hoverpause="yes">
                        @foreach($slider as $slide)
                            <div class="yt-content-slide">
                                <a target="_blank" href="{{$slide->url}}"><img
                                            src="{{ asset('assets/images/sliders/web/'.$slide->image) }}"
                                            alt="slider1" class="img-responsive"></a>
                            </div>
                        @endforeach
                    </div>
                    <div class="loadeding"></div>
                </div>
                <div class="block-policy3">
                    <ul>
                        <li class="item-1">
                            <div class="item-inner">
                                <div class="icon icon1"></div>
                                <div class="content">
                                    <a href="#">Fast & Free Shipping</a>
                                    <p>on all orders $99</p>
                                </div>
                            </div>
                        </li>
                        <li class="item-2">
                            <div class="item-inner">
                                <div class="icon icon2"></div>
                                <div class="content">
                                    <a href="#">100% Money G.uarantee</a>
                                    <p>30 days mone.y back</p>
                                </div>
                            </div>
                        </li>
                        <li class="item-5">
                            <div class="item-inner">
                                <div class="icon icon5"></div>
                                <div class="content">
                                    <a href="#">Safe Shopping</a>
                                    <p>Safe Shopping Guarantee</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="banners banners1">
        <div class="row">
            <div class="col-md-6">
                <a href="#"><img
                            src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-11.jpg"
                            alt="Banner"></a>
            </div>
            <div class="col-md-6">
                <a href="#"><img
                            src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-12.jpg"
                            alt="Banner"></a>
            </div>
        </div>
    </div>
    <div class="module categoties3 padd_top45">
        <div class="box-title">
            <h3 class="modtitle"><span>{{trans('front.selected_features')}}</span></h3>
            {{--    <a class="view-all " href="#">{{trans('front.view_all')}}
                    <i class="fa fa-caret-right" aria-hidden="true"></i></a>--}}
        </div>
        <div class="module listingtab-icons3">
            <div class="modcontent">
                <div id="so_listing_tabs_1" class="so-listing-tabs first-load">
                    <div class="loadeding"></div>
                    <div class="ltabs-wrap">
                        <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300"
                             data-duration="600" data-effect="starwars"
                             data-ajaxurl="{{ url($localization. '/') }}/" data-type_source="0" data-lg="5"
                             data-md="4" data-sm="2" data-xs="1" data-margin="30">
                            <!--Begin Tabs-->
                            <div class="ltabs-tabs-wrap">
                                <span class="ltabs-tab-selected"> </span>
                                <span class="ltabs-tab-arrow">▼</span>
                                <ul class="ltabs-tabs cf list-sub-cat font-title">

                                    <li class="ltabs-tab tab-sel tab-loaded "
                                        data-category-id="new_arrivals"
                                        data-active-content=".items-category-new_arrivals">
                                        <span class="ltabs-tab-label hidden-md hidden-lg">{{trans('front.new_arrivals')}}</span>
                                        <div class="category-title">
                                            <img class="hidden-xs" src="" alt="">
                                            <span class="ltabs-tab-label">{{trans('front.new_arrivals')}}</span>
                                        </div>
                                    </li>
                                    <li class="ltabs-tab " data-category-id="featured_products"
                                        data-active-content=".items-category-featured_products">
                                        <span class="ltabs-tab-label hidden-md hidden-lg">{{trans('front.featured_products')}}</span>
                                        <div class="category-title">
                                            <img class="hidden-xs" src="" alt="">
                                            <span class="ltabs-tab-label"> {{trans('front.featured_products')}}</span>
                                        </div>
                                    </li>
                                    <li class="ltabs-tab " data-category-id="best_sellers"
                                        data-active-content=".items-category-best_sellers">
                                        <span class="ltabs-tab-label hidden-md hidden-lg">{{trans('front.best_sellers')}}</span>
                                        <div class="category-title">
                                            <img class="hidden-xs" src="" alt="">
                                            <span class="ltabs-tab-label">{{trans('front.best_sellers')}}</span>
                                        </div>
                                    </li>
                                    <li class="ltabs-tab " data-category-id="popular_products"
                                        data-active-content=".items-category-popular_products">
                                        <span class="ltabs-tab-label hidden-md hidden-lg">{{trans('front.popular_product')}}</span>
                                        <div class="category-title">
                                            <img class="hidden-xs" src="" alt="">
                                            <span class="ltabs-tab-label">{{trans('front.popular_product')}}</span>
                                        </div>
                                    </li>

                                    <li class="view-all">
                                        <a href="#">
                                            <span class="dot hidden-xs hidden-sm hidden-md">....</span>
                                            <span class="text"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Tabs-->
                        </div>
                        <div class=" ltabs-items-container products-list">

                            <div class="ltabs-items ltabs-items-selected  items-category-new_arrivals"
                                 data-total="10">

                                <div class="ltabs-items-inner3 ">

                                    @foreach($newest as $k=>$product)
                                        @if($c == 0  || $c == 2  || $c == 4)
                                            <div class="col-product">

                                                @endif
                                                <div class="product">
                                                    <div class="left-block">
                                                        <a href="{{ url($localization.'')}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product->name) }}">
                                                            <img src="{{asset('assets/images/products/web/')}}/{{$product->image }}"
                                                                 alt=""></a>
                                                    </div>
                                                    <div class="right-block">
                                                        <h4 class="title-product"><a
                                                                    href="{{ url($localization.'')}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product->name) }}">{{ $product->name }}</a>
                                                        </h4>
                                                        <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                    class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i></span>
                                                        </div>
                                                        <div class="price">$ {{$product->price}}</div>
                                                    </div>
                                                </div>
                                                @if($c == 1  || $c == 3  || $c == 5)
                                            </div>
                                        @endif
                                        @php
                                            $c++;
                                        @endphp
                                    @endforeach


                                </div>

                            </div>

                            <div class="ltabs-items    items-category-featured_products" data-total="10">
                                <div class="ltabs-loading"></div>
                            </div>

                            <div class="ltabs-items   items-category-best_sellers" data-total="10">
                                <div class="ltabs-loading"></div>
                            </div>

                            <div class="ltabs-items   items-category-popular_products" data-total="10">
                                <div class="ltabs-loading"></div>
                            </div>

                            <div class="categories-text">
                                <ul>
                                    @foreach($cats as $i=>$cat)
                                        <li>
                                            <a href="{{ url($localization.'/category/'.$cat->id.'/'.str_replace(' ', '-', $cat->name)) }}">{{$cat->name}}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="banners banner4">
        <div class="row">
            <div class="col-md-12">
                <a href="#"><img
                            src="{{ asset('assets/frontend/image/') }}/catalog/banners/home3/banner-4.jpg"
                            alt="Banner"></a>
            </div>
        </div>
    </div>
    <div class="best-selling module">
        <div class="block-top-selling">
            <h3 class="modtitle"><span>{{trans('front.best_selling')}}</span></h3>
        </div>
        <div class="module layout3-listingtab3">
            <div id="so_listing_tabs_3" class="so-listing-tabs first-load">
                <div class="loadeding"></div>
                <div class="ltabs-wrap">
                    <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300" data-duration="600"
                         data-effect="starwars" data-ajaxurl="{{url($local ."/category_tab") }}/"
                         data-type_source="0"
                         data-lg="5" data-md="4"
                         data-sm="2" data-xs="2" data-xxs="1" data-margin="30">
                        <!--Begin Tabs-->
                        <div class="ltabs-tabs-wrap">
                            <span class='ltabs-tab-selected'></span>
                            <span class="ltabs-tab-arrow">▼</span>
                            <ul class="ltabs-tabs cf list-sub-cat font-title">
                                @foreach($home_cats as $i=>$cat)
                                    <li class="ltabs-tab @if($i == 0) tab-sel @endif"
                                        data-category-id="{{ $cat->id }}"
                                        data-active-content=".items-category-{{ $cat->id }}">
                                        <span class="ltabs-tab-label">{{ $cat->name }}</span>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                        <!-- End Tabs-->
                    </div>
                    <div class="wap-listing-tabs ltabs-items-container products-list grid">
                        <!--Begin Items-->
                        @foreach($home_cats as $i=>$cat)

                            <div class="ltabs-items  @if($i == 0) ltabs-items-selected @endif items-category-{{ $cat->id }}"
                                 data-total="10">
                                @if($i == 0)
                                    <div class="ltabs-items-inner ltabs-slider"> @endif
                                        @if($i == 0)
                                            @foreach($cat->products as $product)
                                                @include('includes.frontend.product_box')
                                            @endforeach
                                        @else
                                            <div class="ltabs-loading"></div>
                                        @endif
                                        @if($i == 0)  </div> @endif
                            </div>

                    @endforeach
                    <!--End Items-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($brands) > 0)
        <div class="slider-brands clearfix">

            <div class="yt-content-slider contentslider" data-rtl="no" data-loop="yes" data-autoplay="no"
                 data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6" data-margin="0"
                 data-items_column0="7" data-items_column1="6" data-items_column2="3" data-items_column3="2"
                 data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes"
                 data-hoverpause="yes">

                @foreach($brands as $brand)

                    <div class="item">
                        <a href="{{url($local.'')}}/brand_view/{{$brand->id}}"><img
                                    src="{{ asset('assets/images/brands/web')}}/{{$brand->image}}"
                                    alt="{{$brand->name}}"></a>
                    </div>

                @endforeach

            </div>

        </div>
    @endif
    <div class="box-content3">
        <div class="row">

         @foreach($blocks as $block)
                <div class="col-md-4">
                    <div class="new-arrivals module block-product">
                        <h3 class="modtitle"><span>{{ $block->name }}</span></h3>
                        <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                             data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6" data-margin="0"
                             data-items_column0="2" data-items_column1="2" data-items_column2="2" data-items_column3="2"
                             data-items_column4="1" data-arrows="yes" data-lazyload="yes" data-loop="no"
                             data-buttonpage="top">
                            @foreach($block->showProducts($local) as $i=>$pp)
                                @if($i %2  == 0) <div class="item"> @endif
                                       @include('includes.frontend.product_box',['product'=>$pp])

                                        @if($i%2  != 0) </div> @endif
                            @endforeach
                                @if($i%2  == 0) </div> @endif
                        </div>
                    </div>
                </div>
            @endforeach
            {{--        <div class="col-md-4">
                        <div class="new-arrivals module block-product">
                            <h3 class="modtitle"><span>Freelanser</span></h3>
                            <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes" data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="2" data-items_column1="2" data-items_column2="2" data-items_column3="2" data-items_column4="1" data-arrows="yes" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$64.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$47.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$84.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$74.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$51.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$84.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$81.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$49.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="most-viewed module block-product">
                            <h3 class="modtitle"><span>Presents</span></h3>
                            <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes" data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="2" data-items_column1="2" data-items_column2="2" data-items_column3="2" data-items_column4="1" data-arrows="yes" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$71.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$65.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$54.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$44.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$73.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$96.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$87.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-item">
                                        <div class="item-inner product-layout transition product-grid">
                                            <div class="product-item-container">
                                                <div class="left-block">
                                                    <div class="product-image-container second_img">
                                                        <a href="product.html" target="_self" title="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg" class="img-1 img-responsive" alt="Pastrami bacon">
                                                            <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg" class="img-2 img-responsive" alt="Pastrami bacon">
                                                        </a>
                                                    </div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <button type="button" class="addToCart btn-button" title="Add to cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket"></i>
                                                            <span>Add to cart </span>
                                                        </button>
                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List" onclick="wishlist.add('60');"><i class="fa fa-heart"></i><span>Add to Wish List</span>
                                                        </button>
                                                        <button type="button" class="compare btn-button" title="Compare this Product " onclick="compare.add('60');"><i class="fa fa-refresh"></i><span>Compare this Product</span>
                                                        </button>
                                                        <!--quickview-->
                                                        <a class="iframe-link btn-button quickview quickview_handler visible-lg" href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                        <!--end quickview-->
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <div class="caption">
                                                        <div class="rating">    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                        <h4><a href="product.html" title="Pastrami bacon" target="_self">Pastrami bacon</a></h4>
                                                        <div class="price">$68.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
        </div>
    </div>
@endsection