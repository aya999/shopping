@extends('layouts.frontend.master')
@section('content')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection
@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($local.'/eg')}}/category_view/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}" class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $category)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($local.'/eg')}}/category_view/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($category->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($local.'/eg')}}/category_view/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
    <!-- Main Container  -->
    <div class="main-container container">
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">{{ trans('front.home') }}</a></li>
            <li><a >{{ trans('front.shopping_cart') }}</a></li>
        </ul>

        <div class="row">
            <!--Middle Part Start-->
            <div id="content" class="col-sm-12">
                <h2 class="title">{{ trans('front.shopping_cart') }}</h2>
                <div class="table-responsive form-group">
                    @php
                        $cart = $_SESSION['cart'];
                    @endphp
                    @if(count($cart)> 0)
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td class="text-center">{{ trans('front.image') }}</td>
                                <td class="text-center">{{ trans('front.product_name') }}</td>
                                <td class="text-center">{{ trans('front.category') }}</td>
                                <td class="text-center">{{ trans('front.quantity') }}</td>
                                <td class="text-center">{{ trans('front.unit_price') }}</td>
                                <td class="text-center">{{ trans('front.total') }}</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart as $p)
                                <?php
                                $pro = \App\Models\Product::find($p['productid']);
                                ?>
                                <tr id="product_row{{ $pro->id }}">
                                    <td class="text-center">
                                        <a href="{{ url($local.'/eg/product_view/'.$pro->id . '/' . $pro[$local.'_name']) }}"><img width="70px"
                                                                    src="{{ asset('assets/images/products/mobile/'.$pro->image) }}"
                                                                    alt="{{ $pro[$local.'_name'] }}"
                                                                    title="{{ $pro[$local.'_name'] }}"
                                                                    class="img-thumbnail"/></a></td>
                                    <td class="text-center"><a href="{{ url($local.'/eg/product_view/'.$pro->id . '/' . $pro[$local.'_name']) }}">{{ $pro[$local.'_name'] }}</a><br/>
                                    </td>
                                    <td class="text-center">{{ $pro->category[$local.'_name'] }}</td>
                                    <td class="text-center" width="200px">
                                        <div class="input-group btn-block quantity">
                                            <input type="text" name="quantity" value="{{ $p['quantity'] }}" id="new_quantity{{ $pro->id }}" size="1" class="form-control"/>
                                            <span class="input-group-btn">
                        <button type="submit" data-toggle="tooltip" title="Update" class="btn btn-primary" onclick="cart.edit('{{ $pro->id }}','{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$pro->image) }}','{{ url($local.'/eg/products/cart/edit') }}','{{ url($local.'/eg/products/cart/small') }}');""><i
                                    class="fa fa-clone"></i></button>
                        <button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger" onclick="cart.delete('{{ $pro->id }}','{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$pro->image) }}','{{ url($local.'/eg/products/cart/delete') }}','{{ url($local.'/eg/products/cart/small') }}');
                                ;element.remove('product_row{{ $pro->id }}')

                                "><i
                                    class="fa fa-times-circle"></i></button>
                        </span></div>
                                    </td>
                                    <td class="text-center">{{ $pro->price  }}</td>
                                    <td class="text-center">{{ $pro->price *   $p['quantity'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>

{{--
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                            <tbody>

                            <tr>
                                <td class="text-right">
                                    <strong>{{ trans('front.total') }}:</strong>
                                </td>
                                <td class="text-right total_price" >${{ $_SESSION['total'] }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>--}}

                <div class="buttons">
                    <div class="pull-left"><a href="{{ url($local.'/eg') }}" class="btn btn-primary">{{ trans('front.continue_shopping') }}</a></div>
                    <div class="pull-right"><a href="checkout.html" class="btn btn-primary">{{ trans('front.checkout') }} $<span class="total_price">{{ $_SESSION['total'] }}</span></a></div>
                </div>
            </div>
            <!--Middle Part End -->

        </div>
    </div>
    <!-- //Main Container -->
@endsection