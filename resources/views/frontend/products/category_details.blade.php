@extends('layouts.frontend.master')
@section('content')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection

@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($localization)}}/category/{{$cat->id}}/{{ str_replace(' ', '-', $cat->name) }}"
                               class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $cate)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($localization)}}/category/{{$cate->id}}/{{ str_replace(' ', '-', $cate[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $cate[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($cate->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($localization)}}/category/{{ $child->id }}/{{ str_replace(' ', '-',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
<ul class="breadcrumb">
    <li><a href="{{url($local.'/eg/en')}}">{{trans('front.home')}}</a></li>
    <li><a href="{{url($local.'/eg/en')}}/category_view/{{$category->id}}">{{$category[$local.'_name']}}</a></li>
</ul>
<style>
    #content .module h3.modtitle2{
        margin-top: 20px !important;
    }
</style>
<div class="row">
    <div class="row-product">
        <!--Left Part Start -->
        <aside class="col-sm-4 col-md-3 content-aside" id="column-left">
            <div class="module category-style">
                <h3 class="modtitle">{{trans('front.filter_by')}}</h3>
                <div class="modcontent ">
                    <form class="type_2">
                        <div class="table_layout filter-shopby">
                            <div class="table_row">
                                <div class="table_cell">
                                    <fieldset>
                                        <legend>{{trans('front.all_categories')}}</legend>

                                        <ul class="checkboxes_list">
                                             @foreach($category->categories as $cat)
                                                <li>
                                                    <input type="checkbox" name="categories[]" value="{{ $cat->id }}"
                                                           id="{{$cat[$local.'_name'].'_'.$cat->id}}">
                                                    <label for="{{$cat[$local.'_name'].'_'.$cat->id}}">{{$cat[$local.'_name']}}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </fieldset>
                                </div>
                                <!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->
                                <div class="table_cell ">
                                    <fieldset>
                                        <legend>{{trans('front.price')}}</legend>
                                        <div id="slider" data-min="{{$min_price}}" data-max="{{$max_price}}"
                                             class="slidersClass ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                            <span class="data"></span>
                                            <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                            <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                  style="left: 3.15795%;"></span>
                                            <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                  style="left: 96.8438%;"></span>
                                            <div class="ui-slider-range ui-widget-header ui-corner-all"
                                                 style="left: 3.15795%; width: 93.6859%;"></div>
                                        </div>
                                        <div class="bock-range range">
                                            <input type="text" name="min_price" id="min_val" value=""
                                                   class="min_val input input-group">
                                            <input type="text" name="max_price" id="max_val" value=""
                                                   class="max_val input input-group">
                                        </div>
                                        <div class="range">
                                            {{trans('front.range')}} :
                                            <span class="min_val">{{$min_price}}</span> -
                                            <span class="max_val">{{$max_price}}</span>

                                        </div>


                                    </fieldset>
                                </div>
                                <!--/ .table_cell -->
                                <!-- - - - - - - - - - - - - - End price - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - SIZE - - - - - - - - - - - - - - - - -->
                                <div class="table_cell">
                                    <fieldset>
                                        <legend>{{trans('front.size')}}</legend>
                                        <ul class="size">
                                            <li class="size_li">
                                                <span>XS</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]"
                                                       value="XS">

                                            </li>
                                            <li class="size_li">
                                                <span>S</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]" value="S">
                                            </li>
                                            <li class="size_li">
                                                <span>M</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]" value="M">
                                            </li>
                                            <li class="size_li">
                                                <span>L</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]" value="L">
                                            </li>
                                            <li class="size_li">
                                                <span>XL</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]"
                                                       value="XL">
                                            </li>
                                            <li class="size_li">
                                                <span>XXL</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]"
                                                       value="XXL">
                                            </li>
                                            <li class="size_li">
                                                <span>XXXL</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]"
                                                       value="XXXL">
                                            </li>
                                            <li class="size_li">
                                                <span>XXXL</span>
                                                <input class="image_radio" type="checkbox" name="sizes[]"
                                                       value="XXXL">
                                            </li>

                                        </ul>
                                    </fieldset>
                                </div>
                                <!--/ .table_cell -->
                                <!-- - - - - - - - - - - - - - End Size - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - Color - - - - - - - - - - - - - - - - -->

                            @foreach($properties as $property)
                                @if($property->type_id == 1) <!-- list -->
                                    <div class="table_cell ">
                                        <fieldset>
                                            <legend>{{$property[$local.'_name']}}</legend>
                                            <ul class="checkboxes_list">
                                                @foreach($property->values as $PropertyOption)
                                                    <li>
                                                        <input type="checkbox" name="{{ $local. '_'.$property->name}}"
                                                               id="{{$PropertyOption->name.'_'.$PropertyOption->id}}">
                                                        <label for="{{$PropertyOption->name.'_'.$PropertyOption->id}}">{{$PropertyOption->name}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>

                                        </fieldset>
                                    </div>
                                @elseif($property->type_id == 2)  <!-- text -->

                                    <div class="table_cell ">
                                        <fieldset>
                                            <legend>{{$property[$local.'_name']}}</legend>
                                            <ul class="checkboxes_list">

                                                @foreach($property->values as $Propertyption)

                                                    <li>
                                                        <input type="checkbox" name="{{ $local. '_'.$property->name}}"
                                                               id="{{$Propertyption->name.'_'.$Propertyption->id}}">
                                                        <label for="{{$Propertyption->name.'_'.$Propertyption->id}}">{{$Propertyption->name}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>

                                        </fieldset>
                                    </div>
                                @elseif($property->type_id == 3)  <!-- number -->

                                    <div class="table_cell ">
                                        <fieldset>
                                            <legend>{{$property[$local.'_name']}}</legend>
                                            <div data-min="{{$property->min_value}}"
                                                 data-max="{{$property->max_value}}" id="{{$property->id}}"
                                                 class=" slidersClass ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">

                                                <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                                <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                      style="left: 3.15795%;"></span>
                                                <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                      style="left: 96.8438%;"></span>
                                                <div class="ui-slider-range ui-widget-header ui-corner-all"
                                                     style="left: 3.15795%; width: 93.6859%;"></div>
                                            </div>
                                            <div class="bock-range range">
                                                <input type="text" name="min_{{ $local. '_'.$property[$local.'_name']}}" id="min_val" value=""
                                                       class="min_val input input-group">
                                                <input type="text" name="max_{{ $local. '_'.$property[$local.'_name']}}" id="max_val" value=""
                                                       class="max_val input input-group">
                                            </div>
                                            <div class="range">
                                                {{trans('front.range')}} :
                                                <span class="min_val">{{$property->min_value}}</span> -
                                                <span class="max_val">{{$property->max_value}}</span>
                                                <input type="hidden" class="min_value"
                                                       value="{{$property->min_value}}">
                                                <input type="hidden" class="max_value"
                                                       value="{{$property->max_value}}">
                                            </div>
                                        </fieldset>
                                    </div>
                                @elseif($property->type_id == 4)   <!-- date -->
                                    <div class="table_cell ">
                                        <fieldset>
                                            <legend>{{$property[$local.'_name']}}</legend>
                                            <div class="row bock-range range">
                                                <input type="text" name="{{ $local. '_'.$property[$local.'_name']}}" value=""
                                                       class="col-md-6 datepicker input input-group">
                                                <input type="text" name="{{ $local. '_'.$property[$local.'_name']}}" value=""
                                                       class="col-md-6 datepicker input input-group">
                                            </div>


                                        </fieldset>
                                    </div>
                                    @endif

                                @endforeach
                                <div class="table_cell">
                                    <fieldset>
                                        <legend>{{trans('front.colors')}}</legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <ul class="color">
                                                    @foreach($colors as $color)
                                                        <li class="color_li" style="background-color:{{$color}}">
                                                            <input class="image_radio" type="checkbox"
                                                                   name="color[]" value="{{$color}}">
                                                            <i class="fa fa-check"></i>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <!--/ .table_cell -->
                                <!-- - - - - - - - - - - - - - End color - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - SUB CATEGORY - - - - - - - - - - - - - - - - -->
                                <div class="table_cell">
                                    <fieldset>
                                        <legend>{{trans('dashboard.brand')}}</legend>
                                        <ul class="checkboxes_list">
                                            @foreach($category->brand as $brand)
                                                <li>
                                                    <input type="checkbox" name="brands[]" id="{{$brand->id}}">
                                                    <label for="{{$brand->id}}">{{$brand[$local.'_name']}}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <button class="btn btn-group filter">{{trans('front.filter')}}</button>
                                    </fieldset>
                                </div>
                                <!--/ .table_cell -->
                                <!-- - - - - - - - - - - - - - End SUB CATEGORY - - - - - - - - - - - - - - - - -->
                            </div>
                            <!--/ .table_row -->
                        </div>
                        <!--/ .table_layout -->
                    </form>

                </div>
            </div>
        </aside>
        <div id="content" class="col-md-9 col-sm-8">

            <div class="products-category">
                <div class="category-derc">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="banners">
                                <div>
                                    <a><img src="{{ asset($category->banner) }}" alt="img cate"><br></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="module">
                    <h3 class="modtitle2">{{trans('dashboard.products')}}</h3>
                </div>
            @if(count($products)>0)
                <!-- Filters -->
                    <div class="product-filter product-filter-top filters-panel">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 view-mode">
                                <div class="list-view">
                                    <button class="btn btn-default grid active" data-view="grid" data-toggle="tooltip"
                                            data-original-title="Grid"><i class="fa fa-th"></i></button>
                                    <button class="btn btn-default list " data-view="list"
                                            data-toggle="tooltip" data-original-title="List"><i
                                                class="fa fa-th-list"></i></button>
                                </div>
                            </div>

                            <div class="box-pagination col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                <div class="content-pagination-2">
                                    {{ $products->render() }}
                                </div>
                            </div>
                            <div class="short-by-show form-inline text-right col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group short-by">
                                    <select id="input-sort" class="form-control"
                                            onchange="location = this.value;">
                                        <option value="" selected="selected">Sort By Default</option>
                                        <option value="">Sort By Name (A - Z)</option>
                                        <option value="">Sort By Name (Z - A)</option>
                                        <option value="">Sort By Price (Low &gt; High)</option>
                                        <option value="">Sort By Price (High &gt; Low)</option>
                                        <option value="">Sort By Rating (Highest)</option>
                                        <option value="">Sort By Rating (Lowest)</option>
                                        <option value="">Sort By Model (A - Z)</option>
                                        <option value="">Sort By Model (Z - A)</option>
                                    </select>
                                    <button class="btn btn-group"><i class="fa fa-long-arrow-down"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //end Filters -->
            @endif
            <!--changed listings-->
                <div class="products-list row nopadding-xs so-filter-list grid">
                    @foreach($products as $product)
                        <div class="product-layout  col-md-3 col-sm-6 col-xs-12">
                            @include('includes.frontend.product_box2')
                        </div>
                    @endforeach
                </div>
                <!--// End Changed listings-->
            @if(count($products)>0)
                <!-- Filters -->
                    <div class="product-filter product-filter-top filters-panel">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 view-mode">
                                <div class="list-view">
                                    <button class="btn btn-default grid active " data-view="grid" data-toggle="tooltip"
                                            data-original-title="Grid"><i class="fa fa-th"></i></button>
                                    <button class="btn btn-default list " data-view="list"
                                            data-toggle="tooltip" data-original-title="List"><i
                                                class="fa fa-th-list"></i></button>
                                </div>
                            </div>

                            <div class="box-pagination col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                <div class="content-pagination-2">
                                    {{ $products->render() }}
                                </div>
                            </div>
                            <div class="short-by-show form-inline text-right col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group short-by">
                                    <select id="input-sort" class="form-control"
                                            onchange="location = this.value;">
                                        <option value="" selected="selected">Sort By Default</option>
                                        <option value="">Sort By Name (A - Z)</option>
                                        <option value="">Sort By Name (Z - A)</option>
                                        <option value="">Sort By Price (Low &gt; High)</option>
                                        <option value="">Sort By Price (High &gt; Low)</option>
                                        <option value="">Sort By Rating (Highest)</option>
                                        <option value="">Sort By Rating (Lowest)</option>
                                        <option value="">Sort By Model (A - Z)</option>
                                        <option value="">Sort By Model (Z - A)</option>
                                    </select>
                                    <button class="btn btn-group"><i class="fa fa-long-arrow-down"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //end Filters -->
             @endif
            </div>
        </div>
    </div>
</div>


@endsection
