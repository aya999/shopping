@extends('layouts.frontend.master')
@section('content')




    <div class="row">
        <aside class="row-product">
            <!--Left Part Start -->
                <aside class="col-sm-4 col-md-3 content-aside" id="column-left">
                         <div class="module category-style">
                                <div class="modcontent  ">
                                        <form class="type_2">
                                            <div class="table_layout filter-shopby">
                                                <div class="table_row">
                                                    <!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->
                                                    <div class="table_cell "  >
                                                        <fieldset>
                                                            <legend>Price</legend>
                                                            <div id="slider" data-min="{{$min_price}}" data-max="{{$max_price}}" class="slidersClass ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                                                <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                                                <span class="ui-slider-handle ui-state-default ui-corner-all" style="left: 3.15795%;"></span>
                                                                <span class="ui-slider-handle ui-state-default ui-corner-all" style="left: 96.8438%;"></span>
                                                                <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 3.15795%; width: 93.6859%;"></div></div>
                                                            <div class="bock-range range">
                                                                <input type="text" name="min_val" id="min_val" value="" class="min_val input input-group">
                                                                <input type="text" name="max_val" id="max_val" value="" class="max_val input input-group">
                                                            </div>
                                                            <div class="range">
                                                                Range :
                                                                <span class="min_val">$ {{$min_price}}</span> -
                                                                <span class="max_val">$ {{$max_price}}</span>
                                                                <input type="hidden" class="min_value" value="{{$min_price}}">
                                                                <input type="hidden" class="max_value" value="{{$max_price}}">
                                                            </div>

                                                        </fieldset>
                                                    </div>
                                                    <!--/ .table_cell -->
                                                    <!-- - - - - - - - - - - - - - End price - - - - - - - - - - - - - - - - -->
                                                    <!-- - - - - - - - - - - - - - SIZE - - - - - - - - - - - - - - - - -->
                                                    <div class="table_cell">
                                                        <fieldset>
                                                            <legend>{{trans('front.size')}}</legend>
                                                            <ul class="size">
                                                                <li class="size_li">
                                                                    <span>XS</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="XS">

                                                                </li>
                                                                <li class="size_li">
                                                                    <span>S</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="S">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>M</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="M">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>L</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="L">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>XL</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="XL">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>XXL</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="XXL">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>XXXL</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="XXXL">
                                                                </li>
                                                                <li class="size_li">
                                                                    <span>XXXL</span>
                                                                    <input class="image_radio" type="checkbox" name="sizes[]" value="XXXL">
                                                                </li>

                                                            </ul>
                                                        </fieldset>
                                                    </div>
                                                    <!--/ .table_cell -->
                                                    <!-- - - - - - - - - - - - - - End Size - - - - - - - - - - - - - - - - -->
                                                    <!-- - - - - - - - - - - - - - Color - - - - - - - - - - - - - - - - -->
                                                    <div class="table_cell">
                                                        <fieldset>
                                                            <legend>{{trans('front.colors')}}</legend>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="color">
                                                                        @foreach($colors as $color)
                                                                            <li class="color_li" style="background-color:{{$color}}">
                                                                                <input class="image_radio" type="checkbox" name="color[]" value="{{$color}}">
                                                                                <i class="fa fa-check"></i>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <!--/ .table_cell -->
                                                    <!-- - - - - - - - - - - - - - End color - - - - - - - - - - - - - - - - -->
                                                    <!-- - - - - - - - - - - - - - SUB CATEGORY - - - - - - - - - - - - - - - - -->

                                                    <!--/ .table_cell -->
                                                    <!-- - - - - - - - - - - - - - End SUB CATEGORY - - - - - - - - - - - - - - - - -->
                                                </div>
                                                <!--/ .table_row -->
                                            </div>
                                            <!--/ .table_layout -->
                                        </form>
                                    </div>
                                <div class="module banner-left hidden-xs ">
                                    <div class="banner-sidebar banners">
                                        <div>
                                            <a title="Banner Image" href="#">
                                                <img src="http://via.placeholder.com/250x390" alt="Banner Image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </aside>


            <div id="content" class="col-md-9 col-sm-8">

                                 <ul class="breadcrumb">
                                     <li><a href="{{url($local.'/eg/en')}}">Home</a></li>
                                     <li><a href="{{url($local.'/eg/en')}}/brand_view/{{$brand->id}}">{{$brand[$local.'_name']}}</a></li>
                                 </ul>
                                 <div class="products-category">
                                     <div class="category-derc">
                                         <div class="row">
                                             <div class="col-sm-12">
                                                 <div class="banners">
                                                     <div>
                                                         <a  href="#"><img src="http://via.placeholder.com/1370x300" alt="img cate"><br></a>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <!-- All Categories -->
                                     <div class="all-categories module">
                                         <h3 class="modtitle2">All Products</h3>
                                         <div class="box-categories">
                                             @foreach($brand->products as $product)

                                             <div class="category">
                                                 <a href="{{url($local.'/eg')}}/product_view/{{$product->id}}">
                                                     <img src="{{asset('assets/images/products/thumbnail')}}/{{$product->image }}" alt="">
                                                     <h4>{{$product[$local.'_name']}}</h4>
                                                 </a>
                                             </div>
                                           @endforeach
                                         </div>
                                     </div>
                                     <!-- end All Categories -->

                                 </div>
                            </div>
                         </div>
                    </div>



@endsection