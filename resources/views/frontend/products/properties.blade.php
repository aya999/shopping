<div class="">
    @if($category->is_online == 1)
        <div class="row">
            <div class="form-group col-md-12 has-float-label">
                <div class="checkbox" style="margin-bottom: 40px">
                    <label>
                        {{ Form::checkbox('is_online',1,null,['class'=>'ace']) }}
                        <span class="lbl"> اون لاين  ؟ </span>
                    </label>
                </div>

            </div>
        </div>
    @endif
    @foreach($properties as $property)
        <div class="form-group col-md-12 has-float-label">
            <label for="name">{{ $property->en_name }}</label>
            @if($property->type->id == 1)
                {{ Form::select($property->name.$property->id, $property->options->pluck('en_name','id'),null, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
            @elseif($property->type->id == 2)
                {{ Form::text($property->name.$property->id, null, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
            @elseif($property->type->id == 3)
                {{ Form::number($property->name.$property->id, null, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}

            @elseif($property->type->id == 4)
                {{ Form::date($property->name.$property->id, null, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
            @endif
            <label for="en_meta_tags">{{ $property->name }}</label>
            <small class="text-danger">{{ $errors->first('en_meta_tags') }}</small>
        </div>
    @endforeach
        @if($category->is_size == 1)
            <div class="row">
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox" >
                        <label>
                            {{ Form::checkbox('sizes[]',"xs",null,['class'=>'ace']) }}
                            <span class="lbl"> XS </span>
                        </label>
                    </div>

                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox" >
                        <label>
                            {{ Form::checkbox('sizes[]',"s",null,['class'=>'ace']) }}
                            <span class="lbl"> S </span>
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox" >
                        <label>
                            {{ Form::checkbox('sizes[]',"l",null,['class'=>'ace']) }}
                            <span class="lbl"> L </span>
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('sizes[]',"xl",null,['class'=>'ace']) }}
                            <span class="lbl"> XL </span>
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('sizes[]',"xxl",null,['class'=>'ace']) }}
                            <span class="lbl"> XXL </span>
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('sizes[]',"xxxl",null,['class'=>'ace']) }}
                            <span class="lbl"> XXXL </span>
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-3 has-float-label">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('sizes[]',"xxxxl",null,['class'=>'ace']) }}
                            <span class="lbl"> XXXXL </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="other" style="margin-top:10px;">
                <div class=" row" id="oneline">
                    <div class="col-xs-1" style="margin-bottom: 30px">
                        <a class="btn btn-primary btn-large add-other"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="fields">

                        <div class="form-group  has-float-label col-md-11 col-xs-11">
                            {{ Form::color('colors[]', null, ['class'=>'form-control ' . ($errors->has('specs') ? 'redborder' : '')  , 'id'=>'specs' , 'placeholder'=>'الصفة']) }}
                            <label for="specs">{{ trans('dashboard.color') }}<span
                                        class="astric">*</span></label>
                            <small class="text-danger">{{ $errors->first('specs') }}</small>
                        </div>

                    </div>

                </div>
            </div>
        @endif
</div>
