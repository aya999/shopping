@extends('layouts.frontend.master')
@section('title',$product[$local.'_name'])
@section('content')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection
@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($localization)}}/category/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}"
                               class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $category)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($localization)}}/category/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($category->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($localization)}}/category/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
<div class="row">
    <ul class="breadcrumb">
        <li><a href="{{url($localization)}}">{{trans('front.home')}}</a></li>
        <li>
            <a href="{{url($localization)}}/category/{{$product->category->id}}/{{ str_replace(' ', '_', $product->category[$local.'_name']) }}">{{$product->category[$local.'_name']}}</a>
        </li>
        <li><a href="{{url($localization)}}/product_view/{{$product->id}}/{{ str_replace(' ', '_', $product[$local.'_name']) }}">{{$product[$local.'_name']}}</a></li>
    </ul>
    <!--Middle Part Start-->
    <div id="content" class="col-md-9 col-sm-8">

        <div class="product-view row">
            <div class="left-content-product">
                <div class="content-product-left class-honizol col-md-5 col-sm-12 col-xs-12">
                    <div class="large-image  ">
                        <img id="lrgImg" itemprop="image" class="product-image-zoom"
                             src="{{ asset('assets/images/products/detail')}}/{{$product->image }}"
                             data-zoom-image="{{ asset('assets/images/products/detail')}}/{{$product->image }}"
                             title="{{$product[$local.'_name']}}" alt="{{$product[$local.'_name']}}">
                    </div>

                    <div id="thumb-slider" class="yt-content-slider full_slider owl-drag" data-rtl="yes"
                         data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="10"
                         data-items_column0="4" data-items_column1="3" data-items_column2="4" data-items_column3="4"
                         data-items_column4="3" data-arrows="yes" data-pagination="no" data-lazyload="yes"
                         data-loop="no" data-hoverpause="yes">
                        <a data-index="0" class="img thumbnail active"
                           data-image="{{asset('assets/images/products/detail')}}/{{$product->image }}"
                           title="{{$product[$local.'_name']}}">
                            <img src="{{asset('assets/images/products/detail/')}}/{{$product->image }}"
                                 title="Chicken swinesha" alt="{{$product[$local.'_name']}}">
                        </a>
                        @php
                            $srcs=[asset('assets/images/products/detail').'/'.$product->image];
                        @endphp
                        @foreach($product->images as $i=>$img)
                            <a data-index="{{$i+1}}" class="img thumbnail "
                               data-image="{{asset('assets/images/products')}}/{{$img->image }}"
                               title="{{$product[$local.'_name']}}">
                                <img src="{{asset('assets/images/products')}}/{{$img->image }}"
                                     title="{{$product[$local.'_name']}}" alt="{{$product[$local.'_name']}}">
                            </a>
                            @php
                                $srcs[$i+1]=asset('assets/images/products').'/'.$img->image ;
                            @endphp
                        @endforeach


                    </div>
                </div>
                <div class="content-product-right col-md-6 col-sm-6 col-xs-12">

                    <div class="title-product">
                        <h1>{{$product[$local.'_name']}}</h1>
                    </div>
                    <!-- Review -->
                    <div class="box-review form-group">
                        <div class="ratings">
                            <div class="rating-box">
                                @for($i=0;$i <intval($product->rate());$i++)
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                    @if($i==5)
                                        @break
                                    @endif
                                @endfor
                                @for($j=$i ;$j<5 ;$j++)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                @endfor
                            </div>
                        </div>

                        <a class="reviews_button" href=""
                           onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">0 {{trans('front.review')}}</a>
                        |
                        <a class="write_review_button" href=""
                           onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">{{trans('front.write_review')}}</a>
                    </div>
                    <div class="product-label form-group">
                        <div class="product_page_price price" itemprop="offerDetails" itemscope=""
                             itemtype="http://data-vocabulary.org/Offer">
                            <span class="price-old"></span>
                            <span class="price-new" itemprop="price">$ {{$product->price}}</span>

                        </div>
                    </div>
                    <div class="stock">
                        @if($product->	is_avail)
                            <span class="status-stock">{{trans('front.in_stock')}}</span>
                        @endif
                        @if(isset($product->code))
                            <span class="code-product">{{trans('front.product_code')}} : {{$product->code}}</span>
                        @endif

                    </div>


                    @if(isset($product->brand))
                        <div class="brand-product">
                            <span>{{trans('front.produce_by')}}: </span>
                            <a href="{{url($localization)}}/brand_view/{{$product->brand->id}}"><img
                                        src="{{ asset('assets/images/brands/web/') }}/{{$product->brand->image}}"
                                        alt="Brand"></a>

                        </div>
                    @endif
                    <div class="short_description form-group">
                        {{$product[$local.'_brief_desc']}}
                    </div>
                    <div id="product">
                        @if(count($product->colors->all()) > 0)
                            <div class="image_option_type form-group required">
                                <label>Colors</label>
                                <ul class="product-options clearfix" id="input-option231">
                                    @foreach($product->colors as $color)
                                        <li class="radio">
                                            <label>
                                                <input class="image_radio" type="radio" name="color"
                                                       value="{{$color->color}}">
                                                <div class="img-thumbnail icon icon-color" data-original-title=""
                                                     style="background-color:{{$color->color}}">

                                                </div>
                                                <i class="fa fa-check"></i>
                                                <label> </label>
                                            </label>
                                        </li>
                                    @endforeach
                                    <li class="selected-option">
                                    </li>

                                </ul>
                            </div>
                        @endif
                            @if(count($product->sizes->all()) > 0)
                            <div class="image_option_type form-group required">
                                <label>Colors</label>
                                <ul class="product-options clearfix si" id="input-option231">
                                    @foreach($product->sizes as $size)
                                        <li class="radio">
                                            <label>
                                                <input class="image_radio" type="radio" name="size"
                                                       value="{{$size->size}}">
                                                <div class="img-thumbnail icon icon-size" data-original-title=""
                                                     >{{$size->size}}

                                                </div>
                                                <i class="fa fa-check"></i>
                                                <label> </label>
                                            </label>
                                        </li>
                                    @endforeach
                                    <li class="selected-option">
                                    </li>

                                </ul>
                            </div>
                        @endif
                        <div class="form-group box-info-product">
                            <div class="option quantity">
                                <label class="hidden-xs">{{trans('front.quantity')}}</label>
                                <div class="input-group quantity-control" unselectable="on"
                                     style="-webkit-user-select: none;">

                                    <input class="form-control" type="text" name="quantity"
                                           value="1">
                                    <input type="hidden" name="product_id" value="50">
                                    <span class="input-group-addon product_quantity_down">−</span>
                                    <span class="input-group-addon product_quantity_up">+</span>
                                </div>
                            </div>

                            <div class="add-to-links wish_comp">
                                <ul class="blank list-inline">
                                    <li>
                                        <a type="button" class="addToCart btn-button btn" title="Add to cart"
                                           onclick="cart.add('60 ');"> <i class="fa fa-shopping-basket">

                                            </i>
                                            <span>{{trans('front.add_cart')}} </span>
                                        </a>
                                    </li>
                                    @if(Auth::check())
                                        @if(Auth::user()->isProductFavorite($product->id))
                                            <li class="compare">
                                                <a class="icon" data-toggle="tooltip" title=""
                                                   onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($local.'/products/like') }}');"
                                                   data-original-title="Compare this Product">
                                                    <i class="fa fa-heart like" id="span{{ $product->id }}"></i>
                                                    <span id="message{{ $product->id }}"> {{trans('front.remove_from_wishlist')}}</span>
                                                </a>
                                            </li>
                                        @else
                                            <li class="compare">
                                                <a class="icon" data-toggle="tooltip" title=""
                                                   onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($local.'/products/like') }}');"
                                                   data-original-title="Compare this Product">
                                                    <i class="fa fa-heart" id="span{{ $product->id }}"></i>
                                                    <span id="message{{ $product->id }}"> {{trans('front.add_to_wishlist')}}</span>
                                                </a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="compare">
                                            <a class="icon"  title="" data-toggle="modal" data-target="#LoginFirst"
                                               data-original-title="Compare this Product">
                                                <i class="fa fa-heart"></i>
                                                <span > {{trans('front.add_to_wishlist')}}</span>
                                            </a>
                                        </li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end box info product -->

                    <div class="content-product-bottom">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="share">
                                    <span>{{trans('front.share')}}: </span>
                                    <div class="icon-share">
                                        <ul class="socials">
                                            <li class="facebook"><a class="_blank"
                                                                    href="https://www.facebook.com/MagenTech"
                                                                    target="_blank"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="twitter"><a class="_blank" href="https://twitter.com/smartaddons"
                                                                   target="_blank"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="google_plus"><a class="_blank"
                                                                       href="https://plus.google.com/u/0/+Smartaddons/posts"
                                                                       target="_blank"><i class="fa fa-google-plus"></i></a>
                                            </li>
                                            <li class="pinterest"><a class="_blank"
                                                                     href="https://www.pinterest.com/smartaddons/"
                                                                     target="_blank"><i class="fa fa-skype"></i></a>
                                            </li>
                                            <li class="pinterest"><a class="_blank"
                                                                     href="https://www.pinterest.com/smartaddons/"
                                                                     target="_blank"><i class="fa fa-pinterest"></i></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Product Tabs -->
        <div class="producttab ">
            <div class="tabsslider horizontal-tabs  col-xs-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1">{{trans('front.desc')}}</a></li>
                    <li class="item_nonactive"><a data-toggle="tab" href="#tab-prop">{{trans('front.properties')}}</a>
                    </li>
                    <li class="item_nonactive"><a data-toggle="tab" href="#tab-review">{{trans('front.review')}}</a>
                    </li>


                </ul>
                <div class="tab-content col-xs-12">
                    <div id="tab-1" class="tab-pane fade active in " style="margin-left: 5%;">
                        {!! $product[$local.'_desc'] !!}

                    </div>
                    <div id="tab-prop" class="tab-pane fade " style="margin-left: 5%;">
                        @foreach($product->properties as $property)
                            @php
                                $categoryProperty = App\Models\CategoryProperty::all()->find($property->property_id);                                   ;

                            @endphp
                            @if($property->value != null)

                                <h1>{{$categoryProperty[$local.'_name']}}</h1>
                                <p>{{$property->value}}</p>
                            @elseif(isset($property->option_id))
                                @php
                                    $option = App\Models\CategoryPropertyOption::all()->find($property->option_id);
                                @endphp
                                <h1>{{$categoryProperty[$local.'_name']}}</h1>
                                <p>{{$option[$local.'_name']}}</p>
                            @endif
                        @endforeach
                    </div>
                    <div id="tab-review" class="tab-pane fade" style="margin-left: 5%;">
                        @if(Auth::check())
                            <form action="{{ url($local.'/products/rate') }}" method="post" class="rate-form-request">
                                {!! Form::token() !!}
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <div id="review">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <td style="width: 50%;"><strong>Have you used this product before?</strong></td>
                                            <td class="text-right">(25) Review</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>Best this product opencart</p>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        @for($i=0;$i <intval($product->rate());$i++)
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-2x"></i></span>
                                                            @if($i==5)
                                                                @break
                                                            @endif
                                                        @endfor
                                                        @for($j=$i ;$j<5 ;$j++)
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-2x"></i></span>
                                                        @endfor
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="text-right"></div>
                                </div>
                                <h2 id="review-title">{{trans('front.write_review')}}</h2>
                                <div class="message"></div>
                                <div class="contacts-form">
                                    <div class="form-group"><span class="icon icon-bubbles-2"></span>
                                        <textarea class="form-control" name="comment"
                                                  placeholder="{{trans('front.review')}}"></textarea>
                                    </div>
                                    <span style="font-size: 11px;"><span class="text-danger">Note:</span>                       HTML is not translated!</span>
                                    <div class="form-group">
                                        <b>Rating</b> <span>Bad</span>&nbsp;
                                        <input type="radio" name="rate" value="1"> &nbsp;
                                        <input type="radio" name="rate"
                                               value="2"> &nbsp;
                                        <input type="radio" name="rate"
                                               value="3"> &nbsp;
                                        <input type="radio" name="rate"
                                               value="4"> &nbsp;
                                        <input type="radio" name="rate"
                                               value="5"> &nbsp;<span>Good</span>
                                    </div>

                                    <div class="buttons clearfix">
                                        <button id="button-review" class="btn buttonGray">Continue</button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div id="review">
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                    <tr>
                                        <td style="width: 50%;"><strong>Have you used this product before? </strong><a  data-toggle="modal" data-target="#LoginFirst" class="" > Write a review  </a></td>
                                        <td class="text-right">(25) Review</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" >
                                            <p>Average Rating</p>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    @for($i=0;$i <intval($product->rate());$i++)
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star fa-stack-2x"></i></span>
                                                        @if($i==5)
                                                            @break
                                                        @endif
                                                    @endfor
                                                    @for($j=$i ;$j<5 ;$j++)
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star-o fa-stack-2x"></i></span>
                                                    @endfor
                                                </div>
                                            </div>

                                        </td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="text-right"></div>
                            </div>

                        @endif
                    </div>


                </div>
            </div>
        </div>
        <!-- //Product Tabs -->
        <!-- Related Products -->
        <div class="related titleLine products-list grid module " style="margin-left: 5%;">
            <div id="so_listing_tabs_3" class="so-listing-tabs first-load">
                <div class="loadeding"></div>
                <div class="ltabs-wrap">
                    <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300" data-duration="600"
                         data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="4" data-md="4" data-sm="2"
                         data-xs="1" data-xxs="1" data-margin="30">
                        <!--Begin Tabs-->
                        <div class="ltabs-tabs-wrap">
                            <span class='ltabs-tab-selected'></span>
                            <span class="ltabs-tab-arrow">▼</span>
                            <ul class="ltabs-tabs cf list-sub-cat font-title">
                                <li class="ltabs-tab tab-sel" data-category-id="61"
                                    data-active-content=".items-category-61">
                                    <span class="ltabs-tab-label">{{trans('front.related_product')}}</span></li>

                            </ul>
                        </div>
                        <!-- End Tabs-->
                    </div>
                    <div class="wap-listing-tabs ltabs-items-container products-list grid">
                        <!--Begin Items-->
                        <div class="ltabs-items ltabs-items-selected items-category-61" data-total="10">
                            <div class="ltabs-items-inner ltabs-slider">
                                @foreach($related as $rel)
                                    @include('includes.frontend.product_box',['product'=>$rel])
                                @endforeach
                            </div>
                        </div>


                        <!--End Items-->
                    </div>
                </div>
            </div>
        </div>
        <!-- end Related  Products-->
    </div>
    <!--Middle Part End-->
    <!--Left Part Start -->
    <aside class="col-sm-4 col-md-3 content-aside">
        <div class="">
            <div class="userBlock">
                <div class="backgrounImg">
                    <img src="{{ asset($product->user->bg) }}">
                </div>
                <div class="userImg">
                    <img src="{{asset($product->user->image) }}">
                </div>
                <div class="userDescription">
                    <h5><a href="{{url($local.'/eg/profile/'. $product->user->id .'/'. str_replace(' ', '_', $product->user->name)) }}">{{ $product->user->name }}</a></h5>
                    <p>{{ $product->user->mobile }}</p>
                    <div class="followrs">
                        <span class="number number{{ $product->user->id }}">{{  $product->user->no_follows  }}</span>
                        <span> {{ trans('front.follower') }}</span>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->isUseFollow($product->user->id))
                            <button class="btn btn-danger"
                                    onclick="follow.add('{{  $product->user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $product->user->id }}">{{ trans('front.unfollow') }}
                            </button>
                        @else
                            <button class="btn btn-primary"
                                    onclick="follow.add('{{  $product->user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $product->user->id }}">{{ trans('front.follow') }}
                            </button>
                        @endif
                    @else
                        <button class="btn btn-primary" data-toggle="modal"
                                data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                    @endif

                </div>

            </div>
        </div>
    </aside>

    <aside class="col-sm-4 col-md-3 content-aside">

        <div class=" product-simple" style="">
            <h3 class="modtitle">
                <span>{{trans('front.latest_product')}}</span>
            </h3>
            <div class="modcontent">
                <div class="so-extraslider">
                    <!-- Begin extraslider-inner -->
                    <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes" data-pagination="no"
                         data-autoplay="no" data-delay="4" data-speed="0.6" data-margin="30" data-items_column0="1"
                         data-items_column1="1" data-items_column2="1" data-items_column3="1" data-items_column4="1"
                         data-arrows="no" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                        <div class="item ">
                            @foreach($latest as $lat)
                                <div class="product-layout item-inner style1 ">
                                    <div class="item-image">
                                        <div class="item-img-info">
                                            <a href="{{ url($localization)}}/product_view/{{$lat->id}}/{{ str_replace(' ', '_', $lat->name) }}" target="_self" title="{{$lat->name}}">
                                                <img src="{{ asset('assets/images/products/mobile') }}/{{$lat->image}}"
                                                     alt="{{$lat->name}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="rating">
                                            @for($i=0;$i <intval($lat->rate());$i++)
                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                @if($i==5)
                                                    @break
                                                @endif
                                            @endfor
                                            @for($j=$i ;$j<5 ;$j++)
                                                <span class="fa fa-stack"><i
                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                            @endfor
                                        </div>
                                        <div class="content_price price">
                                            <span class="price-new product-price">$ {{$lat->price}} </span>&nbsp;&nbsp;
                                        </div>
                                        <div class="item-title">
                                            <a href="{{ url($localization)}}/product_view/{{$lat->id}}/{{ str_replace(' ', '_', $lat->name) }}"
                                               target="_self" title="{{$lat->name}}">{{$lat->name}} </a>
                                        </div>
                                    </div>
                                    <!-- End item-info -->
                                    <!-- End item-wrap-inner -->
                                </div>
                        @endforeach
                        <!-- End item-wrap -->

                            <!-- End item-wrap -->
                        </div>

                    </div>
                    <!--End extraslider-inner -->
                </div>
            </div>
        </div>
    </aside>
    <!--Left Part End -->
</div>

@endsection
@section('scripts')


    <script>

        $(document).ready(function () {


            var src = document.getElementById("lrgImg").src;
            var items = [];

            @for($i=0;$i< count($srcs);$i++)

            items.push({src: '{{$srcs[$i]}}'});
                    @endfor
            var zoomCollection = '.large-image img';
            $(zoomCollection).elevateZoom({
                zoomType: "inner",
                lensSize: "200",
                easing: true,
                gallery: 'thumb-slider',
                cursor: 'pointer',
                loadingIcon: '{{ asset('assets/frontend') }}/image/theme/lazy-loader.gif',
                galleryActiveClass: "active"
            });

            $('.large-image').magnificPopup({
                items: items,
                gallery: {enabled: true, preload: [0, 2]},
                type: 'image',
                mainClass: 'mfp-fade',
                callbacks: {
                    open: function () {

                        var activeIndex = parseInt($('#thumb-slider .img.active').attr('data-index'));
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.goTo(activeIndex);
                    }
                }
            });
            $("#thumb-slider .owl2-item").each(function () {
                $(this).find("[data-index='0']").addClass('active');
            });

            $('.thumb-video').magnificPopup({
                type: 'iframe',
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                            id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                        },
                    }
                }
            });
            $('.product-options li.radio').on("click", function () {

                $(this).addClass(function () {
                    if ($(this).hasClass("active")) return "";
                    return "active";
                });

                $(this).siblings("li").removeClass("active");
                $(this).parent().find('.selected-option').html('<span class="label label-success">' + $(this).find('div').data('original-title') + '</span>');
            });
            // Product detial reviews button
            $(".reviews_button,.write_review_button").on("click", function () {
                var tabTop = $(".producttab").offset().top;
                $("html, body").animate({scrollTop: tabTop}, 1000);
            });


        });


        $(document).on('ready', function () {


            var zoomCollection = '.vertical.large-image img';
            $(zoomCollection).elevateZoom({
                zoomType: "inner",
                lensSize: "200",
                easing: true,
                gallery: 'thumb-slider-vertical',
                cursor: 'pointer',
                galleryActiveClass: "active"
            });
            var src = document.getElementById("lrgImg").src;
            var items = [];
            @for($i=0;$i< count($srcs);$i++)

            items.push({src: '{{$srcs[$i]}}'});
            @endfor
            $('.vertical.large-image').magnificPopup({

                items: items,
                gallery: {enabled: true, preload: [0, 2]},
                type: 'image',
                mainClass: 'mfp-fade',
                callbacks: {
                    open: function () {
                        var activeIndex = parseInt($('#thumb-slider-vertical .img.active').attr('data-index'));
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.goTo(activeIndex);
                    }
                }
            });
            $("#thumb-slider-vertical .owl2-item").each(function () {
                $(this).find("[data-index='0']").addClass('active');
            });

            $('.thumb-video').magnificPopup({
                type: 'iframe',
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                            id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                        },
                    }
                }
            });

            $('.product-options li.radio').on("click", function () {
                $(this).addClass(function () {
                    if ($(this).hasClass("active")) return "";
                    return "active";
                });

                $(this).siblings("li").removeClass("active");
                $(this).parent().find('.selected-option').html('<span class="label label-success">' + $(this).find('div').data('original-title') + '</span>');
            });

            var _isMobile = {
                iOS: function () {
                    return navigator.userAgent.match(/iPhone/i);
                },
                any: function () {
                    return (_isMobile.iOS());
                }
            };

            $(".thumb-vertical-outer .next-thumb").on("click", function () {
                $(".thumb-vertical-outer .lSNext").trigger("click");
            });

            $(".thumb-vertical-outer .prev-thumb").on("click", function () {
                $(".thumb-vertical-outer .lSPrev").trigger("click");
            });
            $(".reviews_button,.write_review_button").on("click", function () {
                var tabTop = $(".producttab").offset().top;
                $("html, body").animate({scrollTop: tabTop}, 1000);
            });
        });
    </script>
@endsection