<div class="ltabs-items-inner3 ">
    @foreach($products as $k=>$product)
        @if($k%2 == 0)
            <div class="col-product">
                @endif
                <div class="product">
                    <div class="left-block">
                        <a href="{{ url($localization)}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product->name) }}">
                            <img src="{{asset('assets/images/products/web/')}}/{{$product->image }}" alt=""></a>
                    </div>
                    <div class="right-block">
                        <h4 class="title-product"><a href="{{ url($localization)}}/{{ $product->category[$local . '_name'] }}/{{$product->id}}/{{ str_replace(' ', '_', $product->name) }}">{{ $product->name }}</a></h4>
                        <div class="rating">
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                        </div>
                        <div class="price">$ {{$product->price}}</div>
                    </div>
                </div>

                @if($k%2 != 0)
            </div>
        @endif
    @endforeach



</div>