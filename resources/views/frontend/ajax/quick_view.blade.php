<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic page needs
    ============================================ -->
    <title>BestShop - Multipurpose Responsive HTML5 Template</title>
    <meta charset="utf-8">
    <meta name="keywords" content="html5 template, best html5 template, best html template, html5 basic template, multipurpose html5 template, multipurpose html template, creative html templates, creative html5 templates" />
    <meta name="description" content="BestShop is a powerful Multi-purpose HTML5 Template with clean and user friendly design. It is definite a great starter for any eCommerce web project." />
    <meta name="author" content="Magentech">
    <meta name="robots" content="index, follow" />

    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/frontend') }}/ico/favicon-16x16.png"/>


    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/'.$local) }}/bootstrap/css/bootstrap.min.css">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend') }}/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend') }}/js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/lib.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend') }}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend') }}/js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS  mfp-content
    ============================================ -->
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so_searchpro.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so_megamenu.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-categories.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/'.$local) }}/footer/footer4.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/theme.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/'.$local) }}/responsive.css" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/') }}/main.css" rel="stylesheet">


    <!-- Google web fonts
   ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet' type='text/css'>
    <style type="text/css">
        body{font-family:'Roboto', sans-serif}
    </style>

</head>

<body class="res layout-subpage">
<div id="wrapper" class="wrapper-full ">
    <!-- Main Container  -->
    <div class="main-container container">

        <div class="row">
            <!--Middle Part Start-->
            <div id="content" class="col-md-12 col-sm-12">

                <div class="product-view row quickview-w">
                    <div class="left-content-product">

                        <div class="content-product-left class-honizol col-md-5 col-sm-12 col-xs-12">
                            <div  class="large-image  ">
                                <img id="lrgImg" itemprop="image" class="product-image-zoom" src="{{ asset('assets/images/products/detail')}}/{{$product->image }}" data-zoom-image="{{ asset('assets/images/products/detail')}}/{{$product->image }}" title=""{{$product[$local.'_name']}}" alt=""{{$product[$local.'_name']}}">
                            </div>

                            <div id="thumb-slider" class="yt-content-slider full_slider owl-drag" data-rtl="yes" data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="10" data-items_column0="4" data-items_column1="3" data-items_column2="4"  data-items_column3="4" data-items_column4="3" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
                                <a data-index="0" class="img thumbnail active" data-image="{{asset('assets/images/products/detail')}}/{{$product->image }}"
                                   title="{{$product[$local.'_name']}}">
                                    <img src="{{asset('assets/images/products/detail/')}}/{{$product->image }}" title="Chicken swinesha" alt="{{$product[$local.'_name']}}">
                                </a>
                                @php
                                    $srcs=[asset('assets/images/products/detail').'/'.$product->image];
                                @endphp
                                @foreach($product->images as $i=>$img)
                                    <a data-index="{{$i+1}}" class="img thumbnail " data-image="{{asset('assets/images/products')}}/{{$img->image }}" title="{{$product[$local.'_name']}}">
                                        <img src="{{asset('assets/images/products')}}/{{$img->image }}" title="{{$product[$local.'_name']}}" alt="{{$product[$local.'_name']}}">
                                    </a>
                                    @php
                                        $srcs[$i+1]=asset('assets/images/products').'/'.$img->image ;
                                    @endphp
                                @endforeach



                            </div>
                        </div>

                        <div class="content-product-right col-md-7 col-sm-12 col-xs-12">
                            <div class="title-product">
                                <h1>{{$product->name}}</h1>
                            </div>
                            <!-- Review ---->
                            <div class="box-review form-group">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    </div>
                                </div>

                                <a class="reviews_button" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">0 reviews</a>	|
                                <a class="write_review_button" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Write a review</a>
                            </div>

                            <div class="product-label form-group">
                                <div class="product_page_price price" itemprop="offerDetails" itemscope="" itemtype="http://data-vocabulary.org/Offer">
                                    <span class="price-old"></span>
                                    <span class="price-new" itemprop="price">$ {{$product->price}}</span>
                                </div>
                                <div class="stock"><span>Availability:</span> <span class="status-stock">In Stock</span></div>
                            </div>

                            <div class="product-box-desc">
                                <div class="inner-box-desc">
                                    <div class="price-tax"><span>Ex Tax:</span> $60.00</div>
                                    <div class="reward"><span>Price in reward points:</span> 400</div>
                                    <div class="brand"><span>Brand:</span><a href="#">Apple</a>		</div>
                                    <div class="model"><span>Product Code:</span> Product 15</div>
                                    <div class="reward"><span>Reward Points:</span> 100</div>
                                </div>
                            </div>

                            <div id="product">
                                @if(count($product->colors->all()) > 0)
                                    <div class="image_option_type form-group required">
                                        <label>Colors</label>
                                        <ul class="product-options clearfix"id="input-option231">
                                            @foreach($product->colors as $color)
                                                <li class="radio">
                                                    <label>
                                                        <input class="image_radio" type="radio" name="color[]" value="{{$color->id}}">
                                                        <div class="img-thumbnail icon icon-color"  data-original-title="" style="background-color:{{$color->color}}">

                                                        </div>
                                                        <i class="fa fa-check"></i>
                                                        <label> </label>
                                                    </label>
                                                </li>
                                            @endforeach
                                            <li class="selected-option">
                                            </li>

                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group box-info-product">
                                    <div class="option quantity">
                                        <label class="hidden-xs">Quantity</label>
                                        <div class="input-group quantity-control" unselectable="on" style="-webkit-user-select: none;">

                                            <input class="form-control" type="text" name="quantity"
                                                   value="1">
                                            <input type="hidden" name="product_id" value="50">
                                            <span class="input-group-addon product_quantity_down">−</span>
                                            <span class="input-group-addon product_quantity_up">+</span>
                                        </div>
                                    </div>

                                    <div class="add-to-links wish_comp">
                                        <ul class="blank list-inline">
                                            <li>
                                                <a type="button" class="addToCart btn-button btn icon" data-toggle="tooltip" title=""
                                                   data-original-title="Add To Cart" onclick="cart.add('60 ');">  <i class="fa fa-shopping-basket">

                                                    </i>
                                                    <span>Add to cart </span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>


                            </div>
                            <!-- end box info product -->

                        </div>

                    </div>
                </div>

                <script type="text/javascript">
                    // Cart add remove functions
                    var cart = {
                        'add': function(product_id, quantity) {
                            parent.addProductNotice('Product added to Cart', '<img src="{{ asset('assets/frontend') }}/image/demo/shop/product/e11.jpg" alt="">', '<h3><a href="#">Apple Cinema 30"</a> added to <a href="#">shopping cart</a>!</h3>', 'success');
                        }
                    }

                    var wishlist = {
                        'add': function(product_id) {
                            parent.addProductNotice('Product added to Wishlist', '<img src="{{ asset('assets/frontend') }}/image/demo/shop/product/e11.jpg" alt="">', '<h3>You must <a href="#">login</a>  to save <a href="#">Apple Cinema 30"</a> to your <a href="#">wish list</a>!</h3>', 'success');
                        }
                    }
                    var compare = {
                        'add': function(product_id) {
                            parent.addProductNotice('Product added to compare', '<img src="{{ asset('assets/frontend') }}/image/demo/shop/product/e11.jpg" alt="">', '<h3>Success: You have added <a href="#">Apple Cinema 30"</a> to your <a href="#">product comparison</a>!</h3>', 'success');
                        }
                    }


                </script>


            </div>


        </div>
        <!--Middle Part End-->
    </div>
    <!-- //Main Container -->

    <style type="text/css">
        #wrapper{box-shadow:none;}
        #wrapper > *:not(.main-container){display: none;}
        #content{margin:0}
        .container{width:100%;}

        .product-info .product-view,.left-content-product,.box-info-product{margin:0;}
        .left-content-product .content-product-right .box-info-product .cart input{padding:12px 16px;}

        .left-content-product .content-product-right .box-info-product .add-to-links{ width: auto;  float: none; margin-top: 0px; clear:none; }
        .add-to-links ul li{margin:0;}

    </style></div>

<!-- Include Libs & Plugins
   ============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/libs.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-ui/jquery-ui.min.js"></script>


<!-- Theme files
============================================ -->

    <script>

        $(document).ready(function() {

            $('.all_cat_').hide();
            var src=document.getElementById("lrgImg").src;
            var items=[ ];

            @for($i=0;$i< count($srcs);$i++)

            items.push({src:'{{$srcs[$i]}}'});
                    @endfor
            var zoomCollection = '.large-image img';
            $( zoomCollection ).elevateZoom({
                zoomType    : "inner",
                lensSize    :"200",
                easing:true,
                gallery:'thumb-slider',
                cursor: 'pointer',
                loadingIcon: '{{ asset('assets/frontend') }}/image/theme/lazy-loader.gif',
                galleryActiveClass: "active"
            });

            $('.large-image').magnificPopup({
                items:items ,
                gallery: { enabled: true, preload: [0,2] },
                type: 'image',
                mainClass: 'mfp-fade',
                callbacks: {
                    open: function() {

                        var activeIndex = parseInt($('#thumb-slider .img.active').attr('data-index'));
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.goTo(activeIndex);
                    }
                }
            });
            $("#thumb-slider .owl2-item").each(function() {
                $(this).find("[data-index='0']").addClass('active');
            });

            $('.thumb-video').magnificPopup({
                type: 'iframe',
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                            id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                        },
                    }
                }
            });
            $('.product-options li.radio').on("click", function(){

                $(this).addClass(function() {
                    if($(this).hasClass("active")) return "";
                    return "active";
                });

                $(this).siblings("li").removeClass("active");
                $(this).parent().find('.selected-option').html('<span class="label label-success">'+ $(this).find('div').data('original-title') +'</span>');
            });
            // Product detial reviews button
            $(".reviews_button,.write_review_button").on("click", function (){
                var tabTop = $(".producttab").offset().top;
                $("html, body").animate({ scrollTop:tabTop }, 1000);
            });


        });



        $(document).on('ready',function() {


            var zoomCollection = '.vertical.large-image img';
            $( zoomCollection ).elevateZoom({
                zoomType    : "inner",
                lensSize    :"200",
                easing:true,
                gallery:'thumb-slider-vertical',
                cursor: 'pointer',
                galleryActiveClass: "active"
            });
            var src=document.getElementById("lrgImg").src;
            var items=[];
            @for($i=0;$i< count($srcs);$i++)

            items.push({src:'{{$srcs[$i]}}'});
            @endfor
            $('.vertical.large-image').magnificPopup({

                items: items,
                gallery: { enabled: true, preload: [0,2] },
                type: 'image',
                mainClass: 'mfp-fade',
                callbacks: {
                    open: function() {
                        var activeIndex = parseInt($('#thumb-slider-vertical .img.active').attr('data-index'));
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.goTo(activeIndex);
                    }
                }
            });
            $("#thumb-slider-vertical .owl2-item").each(function() {
                $(this).find("[data-index='0']").addClass('active');
            });

            $('.thumb-video').magnificPopup({
                type: 'iframe',
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                            id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                        },
                    }
                }
            });

            $('.product-options li.radio').on("click", function(){
                $(this).addClass(function() {
                    if($(this).hasClass("active")) return "";
                    return "active";
                });

                $(this).siblings("li").removeClass("active");
                $(this).parent().find('.selected-option').html('<span class="label label-success">'+ $(this).find('div').data('original-title') +'</span>');
            });

            var _isMobile = {
                iOS: function() {
                    return navigator.userAgent.match(/iPhone/i);
                },
                any: function() {
                    return (_isMobile.iOS());
                }
            };

            $(".thumb-vertical-outer .next-thumb").on("click", function () {
                $( ".thumb-vertical-outer .lSNext" ).trigger( "click" );
            });

            $(".thumb-vertical-outer .prev-thumb").on("click", function () {
                $( ".thumb-vertical-outer .lSPrev" ).trigger( "click" );
            });
            $(".reviews_button,.write_review_button").on("click", function (){
                var tabTop = $(".producttab").offset().top;
                $("html, body").animate({ scrollTop:tabTop }, 1000);
            });
        });
    </script>

<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/homepage.js"></script>

<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/addtocart.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend') }}/js/themejs/application.js"></script>

</body>

</html>