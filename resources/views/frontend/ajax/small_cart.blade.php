@php
    $cart = $_SESSION['cart'];
@endphp
@if($cart !=null && count($cart)> 0)
    <li>

        <table class="table table-striped">

            <tbody>

            @if(Auth::check())
                <?php $cart= \App\Models\Cart::where('user_id',Auth::user()->id)->get()?>
                @foreach($cart as $p)
                    <?php
                    $pro = \App\Models\Product::find($p->product_id);
                    ?>
                    <tr>
                        <td class="text-center" style="width:70px">
                            <a href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }}">
                                <img src="{{ asset('assets/images/products/mobile/'.$pro->image) }}"
                                     style="width:70px" alt="{{ $pro[$local.'_name'] }}" title="{{ $pro[$local.'_name'] }}"
                                     class="preview">
                            </a>
                        </td>
                        <td class="text-left"><a class="cart_product_name"
                                                 href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }}">{{ $pro[$local.'_name'] }}</a>
                        </td>
                        <td class="text-center">{{  $p->quantity }}</td>
                        <td class="text-center">{{ $pro->price *  $p->quantity }}</td>
                        <td class="text-right">
                            <a href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }}" class="fa fa-edit"></a>
                        </td>
                        <td class="text-right">
                            <a onclick="cart.delete('{{ $pro->id }}','{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$pro->image) }}','{{ url($localization.'/products/cart/delete') }}','{{ url($localization.'/products/cart/small') }}');"
                               class="fa fa-times fa-delete"></a>
                        </td>
                    </tr>
                @endforeach
                @else
                @foreach($cart as $p)
                    <?php
                    $pro = \App\Models\Product::find($p['productid']);
                    ?>
                    <tr>
                        <td class="text-center" style="width:70px">
                            <a href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }}">
                                <img src="{{ asset('assets/images/products/mobile/'.$pro->image) }}"
                                     style="width:70px" alt="{{ $pro[$local.'_name'] }}" title="{{ $pro[$local.'_name'] }}"
                                     class="preview">
                            </a>
                        </td>
                        <td class="text-left"><a class="cart_product_name"
                                                 href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }}">{{ $pro[$local.'_name'] }}</a>
                        </td>
                        <td class="text-center">{{  $p['quantity'] }}</td>
                        <td class="text-center">{{ $pro->price *   $p['quantity'] }}</td>
                        <td class="text-right">
                            <a href="{{ url($localization)}}/{{ $pro->category[$local . '_name'] }}/{{$pro->id}}/{{ str_replace(' ', '_', $pro[$local.'_name']) }} class="fa fa-edit"></a>
                        </td>
                        <td class="text-right">
                            <a onclick="cart.delete('{{ $pro->id }}','{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$pro->image) }}','{{ url($localization.'/products/cart/delete') }}','{{ url($localization.'/products/cart/small') }}');"
                               class="fa fa-times fa-delete"></a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>

        </table>
    </li>
    <li>
        <div>
            <table class="table table-bordered">
                <tbody>

                <tr>
                    <td class="text-left"><strong>{{ trans('front.total') }}</strong>
                    </td>
                    <td class="text-right">{{ $_SESSION['total'] }}</td>
                </tr>
                </tbody>
            </table>
            <p class="text-right"><a class="btn view-cart" href="{{ url($local.'/eg/products/cart') }}"><i
                            class="fa fa-shopping-cart"></i>{{ trans('front.shopping_cart') }}</a>&nbsp;&nbsp;&nbsp;
                <a class="btn btn-mega checkout-cart" href="checkout.html"><i
                            class="fa fa-share"></i>{{ trans('front.checkout') }}</a>
            </p>
        </div>
    </li>
@else
    {{ trans('front.no_cart') }}
@endif