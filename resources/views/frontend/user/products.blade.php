@extends('layouts.frontend.user')
@section('profile_content')
    <div class="products-category">
        <div class="module">
            <h3 class="modtitle2">{{trans('dashboard.products')}}</h3>
        </div>
        @if(count($products) > 0)
        <!-- Filters -->
            <div class="product-filter product-filter-top filters-panel">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 view-mode">
                        <div class="list-view">
                            <button class="btn btn-default grid " data-view="grid" data-toggle="tooltip"
                                    data-original-title="Grid"><i class="fa fa-th"></i></button>
                            <button class="btn btn-default list active" data-view="list"
                                    data-toggle="tooltip" data-original-title="List"><i
                                        class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div class="short-by-show form-inline text-right col-lg-6 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group short-by">
                            <select id="input-sort" class="form-control"
                                    onchange="location = this.value;">
                                <option value="" selected="selected">Sort By Default</option>
                                <option value="">Sort By Name (A - Z)</option>
                                <option value="">Sort By Name (Z - A)</option>
                                <option value="">Sort By Price (Low &gt; High)</option>
                                <option value="">Sort By Price (High &gt; Low)</option>
                                <option value="">Sort By Rating (Highest)</option>
                                <option value="">Sort By Rating (Lowest)</option>
                                <option value="">Sort By Model (A - Z)</option>
                                <option value="">Sort By Model (Z - A)</option>
                            </select>
                            <button class="btn btn-group"><i class="fa fa-long-arrow-down"></i></button>
                        </div>
                        <div class="form-group">
                            <span class="control-label">Show:</span>
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                <option value="" selected="selected">15</option>
                                <option value="">25</option>
                                <option value="">50</option>
                                <option value="">75</option>
                                <option value="">100</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-pagination col-lg-3 col-md-12 col-sm-12 col-xs-12 text-right">
                        <div class="content-pagination-2">
                            <span>Page: </span>
                            <ul class="pagination1">
                                <li class="active"><span>1</span></li>
                                <li><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href="">4</a></li>
                                <li><a href=""><i class="fa fa-caret-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //end Filters -->
        @endif
    <!--changed listings-->
        <div class="products-list row nopadding-xs so-filter-gird list">
            @foreach($products as $product)
                <div class="product-layout  col-md-3 col-sm-6 col-xs-12">
                    @include('includes.frontend.product_box2')
                </div>
            @endforeach

        </div>
        <!--// End Changed listings-->
        @if(count($products) > 0)
        <!-- Filters -->
            <div class="product-filter product-filter-top filters-panel">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 view-mode">
                        <div class="list-view">
                            <button class="btn btn-default grid " data-view="grid" data-toggle="tooltip"
                                    data-original-title="Grid"><i class="fa fa-th"></i></button>
                            <button class="btn btn-default list active" data-view="list"
                                    data-toggle="tooltip" data-original-title="List"><i
                                        class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div class="short-by-show form-inline text-right col-lg-6 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group short-by">
                            <select id="input-sort" class="form-control"
                                    onchange="location = this.value;">
                                <option value=""
                                        selected="selected">{{trans('front.sort_default')}}</option>
                                <option value="">Sort By Name (A - Z)</option>
                                <option value="">Sort By Name (Z - A)</option>
                                <option value="">Sort By Price (Low &gt; High)</option>
                                <option value="">Sort By Price (High &gt; Low)</option>
                                <option value="">Sort By Rating (Highest)</option>
                                <option value="">Sort By Rating (Lowest)</option>
                                <option value="">Sort By Model (A - Z)</option>
                                <option value="">Sort By Model (Z - A)</option>
                            </select>
                            <button class="btn btn-group"><i class="fa fa-long-arrow-down"></i></button>
                        </div>
                        <div class="form-group">
                            <span class="control-label" for="input-limit">{{trans('front.show')}}:</span>
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                <option value="" selected="selected">15</option>
                                <option value="">25</option>
                                <option value="">50</option>
                                <option value="">75</option>
                                <option value="">100</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-pagination col-lg-3 col-md-12 col-sm-12 col-xs-12 text-right">
                        <div class="content-pagination-2">
                            <span>{{trans('front.page')}}: </span>
                            <ul class="pagination1">
                                <li class="active"><span>1</span></li>
                                <li><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href="">4</a></li>
                                <li><a href=""><i class="fa fa-caret-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //end Filters -->
        @endif
    </div>
@endsection