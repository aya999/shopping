@extends('layouts.frontend.user')
@section('profile_content')
    <div id="content" class="col-sm-12">
        <h2 class="title">Register Account</h2>
        <p>If you already have an account with us, please login at the <a href="#">login page</a>.</p>
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal account-register clearfix">

            <fieldset>
                <legend>Your Password</legend>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-password">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
                    <div class="col-sm-10">
                        <input type="password" name="confirm" value="" placeholder="Password Confirm" id="input-confirm" class="form-control">
                    </div>
                </div>
            </fieldset>


        </form>
    </div>
@endsection