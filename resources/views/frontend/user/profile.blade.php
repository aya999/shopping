@extends('layouts.frontend.user')
@section('profile_content')
    <div id="content" class="col-sm-12">
        {!! Form::model($user, ['route' => ['frontend.users.profileUpdate', $user->id], 'method' => 'POST' , 'files' => true,'class'=>'form-horizontal account-register clearfix']) !!}
            <fieldset id="account">
                <legend>Your Personal Details</legend>
                <div class="form-group required" style="display: none;">
                    <label class="col-sm-2 control-label">Customer Group</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="customer_group_id" value="1" checked="checked"> Default
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-firstname">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="{{$user->name}}" placeholder="Name" id="input-firstname" class="form-control">
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" value="{{$user->email}}" placeholder="E-Mail" id="input-email" class="form-control">
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-telephone">Telephone</label>
                    <div class="col-sm-10">
                        <input type="tel" name="mobile" value="{{$user->mobile}}" placeholder="Telephone" id="input-telephone" class="form-control">
                    </div>
                </div>

            </fieldset>
            <fieldset id="address">
                <legend>Your Address</legend>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-address-1">Address </label>
                    <div class="col-sm-10">
                        <input type="text" name="address" value="{{$user->address}}" placeholder="Address 1" id="input-address-1" class="form-control">
                    </div>
                </div>




                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-postcode">	Country code</label>
                    <div class="col-sm-10">
                        <input type="text" name="country_code" value="{{$user->country_code}}" placeholder="Country Code" id="input-postcode" class="form-control">
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-country">City</label>
                    <div class="col-sm-10">
                        <select name="country_id" id="input-country" class="form-control">
                            <option value=""> --- Please Select --- </option>
                            @foreach($cities as $city)
                                    <option @if($user->city_id == $city->id) selected @endif value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

            </fieldset>
            <fieldset>
                <legend>Your Password</legend>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-password">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
                    <div class="col-sm-10">
                        <input type="password" name="confirm" value="" placeholder="Password Confirm" id="input-confirm" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Newsletter</legend>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Subscribe</label>
                    <div class="col-sm-10">
                        <label class="radio-inline">
                            <input type="radio" name="newsletter" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="newsletter" value="0" checked="checked"> No
                        </label>
                    </div>
                </div>
            </fieldset>
            <div class="buttons">
                <div class="pull-right">

                    <input type="submit" value="Edit" class="btn btn-primary">
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection