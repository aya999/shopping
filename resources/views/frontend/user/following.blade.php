@extends('layouts.frontend.user')
@section('profile_content')
    <div class="row" >
        @foreach($followers as $follower)
            <div class="col col-md-4" id="following{{ $follower->id }}">
                <div class="userBlock">
                    <div class="backgrounImg">
                        <img src="{{ asset($follower->bg) }}">
                    </div>
                    <div class="userImg">
                        <img src="{{ asset($follower->image) }}">
                    </div>
                    <div class="userDescription">
                        <h5>
                            <a href="{{url($local.'/eg/profile/'. $follower->id .'/'. str_replace(' ', '_', $follower->name)) }}">{{ $follower->name }}</a>
                        </h5>
                        <p>{{ $follower->mobile }}</p>
                        <div class="followrs">
                            <span class="number number{{ $follower->id }}">{{  $follower->no_follows  }}</span>
                            <span> {{ trans('front.follower') }}</span>
                        </div>
                        @if(Auth::check())
                            @if(Auth::user()->isUseFollow($follower->id))
                                <button class="btn btn-danger"
                                        onclick="follow.add('{{  $follower->id }}','{{ url($local.'/users/follow') }}');@if($user->id == Auth::user()->id ) element.remove('following{{ $follower->id }}') @endif"
                                        id="message{{ $follower->id }}">{{ trans('front.unfollow') }}
                                </button>
                            @else
                                <button class="btn btn-primary"
                                        onclick="follow.add('{{  $follower->id }}','{{ url($local.'/users/follow') }}');"
                                        id="message{{ $follower->id }}">{{ trans('front.follow') }}
                                </button>
                            @endif
                        @else
                            <button class="btn btn-primary" data-toggle="modal"
                                    data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                        @endif

                    </div>

                </div>
            </div>
        @endforeach
            {!! $followers->render() !!}

    </div>
@endsection