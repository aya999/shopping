@section('styles')
<link href="{{ asset('assets/frontend/wizard/css') }}/paper-bootstrap-wizard.css" rel="stylesheet">
<link href="{{ asset('assets/frontend/wizard/css') }}/demo.css" rel="stylesheet">
<link href="{{ asset('assets/frontend/wizard/css') }}/themify-icons.css" rel="stylesheet">
<style>
    .packageType select{
        width:60% !important;
    }
    .package_li
    {
        border: 3px solid #e8e4e2;
        margin-right: 6px;
        margin-bottom: 6px;
    }
    .package_li.selected {
        border: 3px solid #eb5e28;
    }
    .checkbox input[type=checkbox].online_check
    {
        display: block !important;
    }

</style>
@endsection

<div class="image-container set-full-height">

    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">


                            <div class="wizard-navigation">
                                <div class="progress-with-circle">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;"></div>
                                </div>
                                <ul>
                                   <li>
                                        <a  href="#location" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-map"></i>
                                            </div>
                                            القسم
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="#type" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-direction-alt"></i>
                                            </div>
                                            تفاصيل
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="#facilities" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-panel"></i>
                                            </div>
                                            صورة

                                        </a>
                                    </li>
                                    @if (!isset($row))
                                    <li id="customLi">
                                        <a  href="#description" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-comments"></i>
                                            </div>
                                            ميز إعلانك
                                        </a>
                                    </li>
                                        @endif
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane" id="location">
                                    <div class="row">
                                        <input id="user_id" name="user_id" value="{{$user->id}}" type="hidden" />

                                          @if (isset($row))
                                            <div  class="form-group col-md-12 has-float-label {{ ($errors->has('sub_category_id')) ?  'tooltipx has-error ': '' ,(isset($row)) ? 'disabled' : ''}} ">

                                            {{ Form::select('category_id',App\Models\Category::all()->pluck($local.'_name','id'), null, ['class'=>' form-control' . ($errors->has('category_id') ? 'redborder' : '') , 'required' => 'required' , 'id' => 'Maincategory_id',(isset($row)) ? 'disabled' : '', 'placeholder'=>trans('dashboard.category') ]) }}
                                            </div>
                                          @else
                                                <div class="form-group col-md-12 has-float-label {{ ($errors->has('Maincategory_id')) ?  'tooltipx has-error ': '' }} ">
                                                    {{ Form::select('Maincategory_id',App\Models\Category::all()->pluck($local.'_name','id'), null, ['class'=>' form-control' . ($errors->has('category_id') ? 'redborder' : '') , 'required' => 'required' , 'id' => 'Maincategory_id',(isset($row)) ? 'disabled' : '', 'placeholder'=>trans('dashboard.category') ]) }}
                                                    <small class="text-danger tooltiptext">{{ $errors->first('category_id') }}</small>
                                                </div>
                                                <div  class="form-group col-md-12 has-float-label {{ ($errors->has('sub_category_id')) ?  'tooltipx has-error ': '' ,(isset($row)) ? 'disabled' : ''}} ">

                                                <select  id="sub_Cat" required name="category_id" class="form-control" >
                                                    <option value>{{ trans('front.sub_category') }}</option>

                                                </select>
                                                </div>

                                            @endif


                                        <div class="form-group col-md-12 has-float-label ">
                                            {{ Form::select('brand_id',App\Models\Brand::all()->pluck($local.'_name','id'), null, ['class'=>' form-control' . ($errors->has('brand_id') ? 'redborder' : '')  , 'id' => 'brand_id', 'placeholder'=>trans('dashboard.brand') ]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('brand_id') }}</small>
                                        </div>

                                        <div class="form-group col-md-12 has-float-label ">
                                            {{ Form::select('store_id',App\Models\Store::where('user_id',Auth::user()->id)->pluck($local.'_name','id'), null, ['class'=>' form-control' . ($errors->has('store_id') ? 'redborder' : '')  , 'id' => 'store_id', 'placeholder'=>trans('dashboard.store') ]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('brand_id') }}</small>
                                        </div>
                                        <div class="form-group col-md-12 has-float-label {{ ($errors->has('store_id')) ?  'tooltipx has-error ': '' }} " id="myDiv">
                                        </div>
                                        <div id="properties">
                                            @if(isset($row))
                                                @if($category->is_online == 1)
                                                    <div class="row">
                                                        <div class="form-group col-md-12 has-float-label {{ ($errors->has('is_online')) ?  'tooltipx has-error ': '' }}">
                                                            <div class="checkbox" style="margin-bottom: 40px">
                                                                <label>
                                                                    {{ Form::checkbox('is_online',1,null,['class'=>'ace online_check']) }}
                                                                    <span class="lbl"> اون لاين  ؟ </span>
                                                                </label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @endif
                                                @foreach($properties as $property)
                                                    @php
                                                        $value=\App\Models\ProductPropertyValue::where('property_id',$property->id)->where('product_id',$row->id)->first();
                                                       if(!empty($value)){
                                                       $x=$value->value;
                                                       }
                                                        else
                                                        {
                                                        $x=null ;
                                                        }
                                                    @endphp
                                                    <div class="form-group col-md-12 has-float-label ">
                                                        <label for="name">{{ $property->en_name }}</label>
                                                        @if($property->type->id == 1)
                                                            {{ Form::select($property->name.$property->id, $property->options->pluck('en_name','id'),null, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
                                                        @elseif($property->type->id == 2)
                                                            {{ Form::text($property->name.$property->id, $x, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
                                                        @elseif($property->type->id == 3)
                                                            {{ Form::number($property->name.$property->id, $x, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}

                                                        @elseif($property->type->id == 4)
                                                            {{ Form::date($property->name.$property->id, $x, ['class'=>' form-control att ' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'placeholder'=> $property->name]) }}
                                                        @endif
                                                        <label for="en_meta_tags">{{ $property->name }}</label>
                                                        <small class="text-danger">{{ $errors->first('en_meta_tags') }}</small>
                                                    </div>
                                                @endforeach
                                                @if($category->is_size == 1)
                                                    <div class="">
                                                        <div class="form-group col-md-3 has-float-label {{ ($errors->has('image')) ?  'tooltipx has-error ': '' }}">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"xs",(in_array('xs',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> XS </span>
                                                                </label>
                                                            </div>

                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"s",(in_array('xs',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> S </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"l",(in_array('m',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> M </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"l",(in_array('l',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> L </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label {{ ($errors->has('image')) ?  'tooltipx has-error ': '' }}">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"xl",(in_array('xl',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> XL </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"xxl",(in_array('xxl',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> XXL </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"xxxl",(in_array('xxxl',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> XXXL </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3 has-float-label ">
                                                            <div class="checkbox">
                                                                <label>
                                                                    {{ Form::checkbox('sizes[]',"xxxxl",(in_array('xxxxl',$sizes)) ? 'checked' : null,['class'=>'ace']) }}
                                                                    <span class="lbl"> XXXXL </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" id="other" style="margin-top:10px;">
                                                        <div class=" row" id="oneline">
                                                            <div class="col-xs-1" style="margin-bottom: 30px">
                                                                <a class="btn btn-primary btn-large add-other"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                            <div class="fields">

                                                                <div class="form-group  has-float-label  col-md-11 col-xs-11">
                                                                    {{ Form::color('colors[]', null, ['class'=>'form-control ' . ($errors->has('specs') ? 'redborder' : '')  , 'id'=>'specs' , 'placeholder'=>'الصفة']) }}
                                                                    <label for="specs">{{ trans('dashboard.color') }}<span
                                                                                class="astric">*</span></label>
                                                                    <small class="text-danger ">{{ $errors->first('specs') }}</small>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        @if(isset($row))
                                                            @foreach($colors as $color)
                                                                <div class="row">
                                                                    <div class="col-md-1" style="margin-bottom: 30px"><a
                                                                                class="btn btn-danger btn-large del-other"><i class="fa fa-trash"></i></a>
                                                                    </div>

                                                                    <div class="form-group  has-float-label  col-md-11 col-xs-11">
                                                                        {{ Form::color('colors[]', $color->color, ['class'=>'form-control ' . ($errors->has('specs') ? 'redborder' : '')  , 'id'=>'specs' , 'placeholder'=>'الصفة']) }}
                                                                        <label for="specs">{{ trans('dashboard.color') }}<span
                                                                                    class="astric">*</span></label>
                                                                        <small class="text-danger ">{{ $errors->first('specs') }}</small>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif

                                                    </div>
                                                @endif
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="type">
                                    <h5 class="info-text">تفاصيل الاعلان</h5>
                                    <div class="row">
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_name')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::text('ar_name', null, ['class'=>' form-control' . ($errors->has('ar_name') ? 'redborder' : '')  , 'id' => 'ar_name', 'required' => 'required', 'placeholder'=>'Arabic name ']) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_name') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label  {{ ($errors->has('en_name')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::text('en_name', null, ['class'=>' form-control' . ($errors->has('en_name') ? 'redborder' : '')  , 'id' => 'en_name', 'required' => 'required', 'placeholder'=>'English name']) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_name') }}</small>
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12 has-float-label {{ ($errors->has('price')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::number('price', null, ['class'=>' form-control' . ($errors->has('capacity') ? 'redborder' : '')  , 'id' => 'price', 'required' => 'required', 'placeholder'=>'Price ']) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('price') }}</small>
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12 has-float-label {{ ($errors->has('quantity')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::number('quantity', null, ['class'=>' form-control' . ($errors->has('quantity') ? 'redborder' : '')  , 'id' => 'quantity', 'required' => 'required', 'placeholder'=>'Quantity ']) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('quantity') }}</small>
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12 has-float-label {{ ($errors->has('code')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::number('code', null, ['class'=>' form-control' . ($errors->has('code') ? 'redborder' : '')  , 'id' => 'code', 'required' => 'required', 'placeholder'=>trans('dashboard.code')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('code') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('en_brief_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('en_brief_desc', null, ['class'=>' form-control' . ($errors->has('en_brief_desc') ? 'redborder' : '')  , 'id' => 'en_brief_desc', 'required' => 'required', 'rows' => '3', 'placeholder'=>trans('dashboard.en_brief_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_brief_desc') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_brief_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('ar_brief_desc', null, ['class'=>' form-control' . ($errors->has('ar_brief_desc') ? 'redborder' : '')  , 'id' => 'ar_brief_desc', 'required' => 'required', 'rows' => '3', 'placeholder'=>trans('dashboard.ar_brief_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_desc') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('en_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('en_desc', null, ['class'=>' form-control' . ($errors->has('en_desc') ? 'redborder' : '')  , 'id' => 'en_desc', 'rows' => '3', 'placeholder'=>trans('dashboard.en_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_desc') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('ar_desc', null, ['class'=>' form-control' . ($errors->has('ar_desc') ? 'redborder' : '')  , 'id' => 'ar_desc', 'rows' => '3', 'placeholder'=>trans('dashboard.ar_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_desc') }}</small>
                                        </div>
                                        <hr class="hr-12">
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('en_meta_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('en_meta_desc', null, ['class'=>' form-control' . ($errors->has('en_meta_desc') ? 'redborder' : '')  , 'id' => 'en_meta_desc', 'rows' => '3', 'placeholder'=>trans('dashboard.en_meta_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_meta_desc') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_meta_desc')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('ar_meta_desc', null, ['class'=>' form-control' . ($errors->has('ar_meta_desc') ? 'redborder' : '')  , 'id' => 'ar_meta_desc', 'rows' => '3', 'placeholder'=>trans('dashboard.ar_meta_desc')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_meta_desc') }}</small>
                                        </div>

                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('en_meta_tags')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('en_meta_tags', null, ['class'=>' form-control' . ($errors->has('en_meta_tags') ? 'redborder' : '')  , 'id' => 'en_meta_tags', 'rows' => '3', 'placeholder'=>trans('dashboard.en_meta_tags')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_meta_tags') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_meta_tags')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('ar_meta_tags', null, ['class'=>' form-control' . ($errors->has('ar_meta_tags') ? 'redborder' : '')  , 'id' => 'ar_meta_tags', 'rows' => '3', 'placeholder'=>trans('dashboard.ar_meta_tags')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_meta_tags') }}</small>
                                        </div>

                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('en_meta_title')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('en_meta_title', null, ['class'=>' form-control' . ($errors->has('en_meta_title') ? 'redborder' : '')  , 'id' => 'en_meta_title', 'rows' => '3', 'placeholder'=>trans('dashboard.en_meta_title')]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('en_meta_title') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label {{ ($errors->has('ar_meta_title')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::textarea('ar_meta_title', null, ['class'=>' form-control' . ($errors->has('ar_meta_title') ? 'redborder' : '')  , 'id' => 'ar_meta_title', 'rows' => '3', 'placeholder'=>trans('dashboard.ar_meta_title') ]) }}
                                            <small class="text-danger tooltiptext">{{ $errors->first('ar_meta_title') }}</small>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="facilities">
                                    <h5 class="info-text">أضف صورة </h5>
                                    <div class="row">


                                        <div class="form-group col-md-6 has-float-label  {{ ($errors->has('image')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::file('image', ['class'=>'form-control ' . ($errors->has('image') ? 'redborder' : '') , 'id'=>'image' , 'placeholder'=>'image']) }}
                                            <label for="image">{{ trans('dashboard.image') }} <span class="astric">*</span></label>
                                            <small class="text-danger tooltiptext">{{ $errors->first('image') }}</small>
                                        </div>
                                        <div class="form-group col-md-6 has-float-label  {{ ($errors->has('images[]')) ?  'tooltipx has-error ': '' }}">
                                            {{ Form::file('images[]', ['class'=>'form-control ' . ($errors->has('image') ? 'redborder' : '') , 'id'=>'image' , 'placeholder'=>'image','multiple'=>'multiple']) }}
                                            <label for="image">{{ trans('dashboard.images') }} <span class="astric">*</span></label>
                                            <small class="text-danger tooltiptext">{{ $errors->first('images') }}</small>
                                        </div>
                                    </div>
                                </div>
                                @if (!isset($row))
                                <div class="tab-pane" id="description">
                                    <div class="row">
                                        <h5 class="info-text"> ميز إعلانك  </h5>

                            <div class="col-sm-12">
                                <ul>
                                    <input type="hidden"  name="selected_package"  id="selected_package" />

                                @foreach($packages as $package)
                                    @php

                                    $packagType_=App\Models\PackageType::where('package_id',$package->id)->get();
                                    $declare=0;
                                    $declarevalue='';

                                          if(isset($packagType_))
                                            {
                                             $declareObj=$packagType_->first();
                                             if(isset($declareObj->id))
                                             {
                                                  $declarevalue=$declareObj->days;
                                                  $declare=$declareObj->id;

                                            }
                                           }

                                    @endphp
                                    @if ($declare != 0)
                                        <li class="inline col-md-5 package_li  " data-packageId="{{$package->id}}" id="pac_num_{{$package->id}}" >

                                            <label>
                                                <p>{{$package[$local.'_name']}}</p>
                                            </label>
                                            <div class="packageType">
                                                {{ Form::select('package_typ_id',App\Models\PackageType::where('package_id',$package->id)->pluck('days','id'), null, ['class'=>' form-control hidden myselect' . ($errors->has('package_id') ? 'redborder' : '') , 'required' => 'required' , 'id' => 'packagetype_id',(isset($row)) ? 'disabled' : '']) }}
                                                <div class="form-group box-info-product ">
                                                    <div class="option quantity">
                                                        <div  class="input-group  " unselectable="on"  style="-webkit-user-select: none;">
                                                            <p id="details"></p>
                                                            <span data-pac="{{$package->id}}" class="input-group-addon getprevious">−</span>
                                                            <input type="hidden"  value="{{$declare}}" id="test" />
                                                            <input class="form-control  col-md-6" id="package_type_val" disabled  type="text" name="package_type_val" value=" {{$declarevalue}} days">
                                                            <span data-pac="{{$package->id}}"  class="input-group-addon getNext">+</span>
                                                        </div>
                                                    </div>
                                                    <div class="packageTypeDetails">
                                                        <label id="price"></label>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>

                                     @endif
                                @endforeach
                                </ul>

</div>
                                    </div>
                                </div>
                                    @endif
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
                                    <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' />
                                </div>

                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div> <!-- wizard container -->
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->


</div>


@section('scripts3')

<script type="text/javascript" src="{{ asset('assets/frontend/wizard') }}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/wizard') }}/js/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/wizard') }}/js/paper-bootstrap-wizard.js"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/wizard') }}/js/jquery.validate.min.js"></script>

<script>
    $('.btn-next').click(function (e) {
        debugger;
        var stop = false;
        var activeId = $(".active").attr('id');
        var button = $(this);
        $(".tab-pane.active").find('input,select,textarea').each(function () {

            if (!$(this).attr('disabled')) {

                if (!$(this).valid()) {

                    stop = true;
                    return;
                }


            }
        });
        if (stop) {
            e.stopImmediatePropagation();
        }
    });
      $('.package_li').on('click',function(){
debugger;
          var packageid=$(this).data('packageid');

          var par=$('#pac_num_'+packageid);
          var packageType=$(par).find('#package_type_val'); // input
          var packageType_=$(par).find('#test'); // input
          var packageTypeval_=$(par).find('#test').val(); // input

         $('.package_li').each(function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
           }


        });
          $(this).addClass('selected');
          $('#selected_package').val(packageTypeval_);
    });


    $('.getprevious').on('click',function() {
        debugger;
               var   selectdnextd=0;
                 var packageid=$(this).data('pac');
                 var par=$('#pac_num_'+packageid);
                 var packageType=$(par).find('#package_type_val'); // input
                 var packageType_=$(par).find('#test'); // input
                 var packageTypeval_=$(par).find('#test').val(); // input

        //get select

        var sel=$(par).find('.packageType select option[value="'+packageTypeval_+'"]');
        var prev=$(sel).prev();
        if(prev.length == 0)
        {
            selectdnextd=$(par).find('.packageType select option').first(); //first option

        }else {
            selectdnextd=prev //next option

        }



                 //get next val & id
                 var nextVal=$(selectdnextd).val();
                 var nextText=$(selectdnextd).text();
                 $(packageType).val(nextText + " days");
                 $(packageType_).val(nextVal);


    });
    $('.getNext').on('click',function() {
        debugger;
        var   selectdnextd=0;

        var packageid=$(this).data('pac');
        var par=$('#pac_num_'+packageid);
        var packageType=$(par).find('#package_type_val'); // input
        var packageType_=$(par).find('#test'); // input
        var packageTypeval_=$(par).find('#test').val(); // input
        //get select

        var sel=$(par).find('.packageType select option[value="'+packageTypeval_+'"]');
        var next=selectdnextd=$(sel).next();
        if(next.length == 0)
        {
            selectdnextd=$(par).find('.packageType select option').first(); //first option

        }else {
             selectdnextd=next; //next option

        }

        //get next val & id
        var nextVal=$(selectdnextd).val();
        var nextText=$(selectdnextd).text();
        $(packageType).val(nextText + " days");
        $(packageType_).val(nextVal);
    });


</script>


    <script>
        CKEDITOR.replace('en_desc', {
            customConfig: '{{ asset("assets/admin/js/ckeditor/config.js") }}'
        });
        CKEDITOR.replace('ar_desc', {
            customConfig: '{{ asset("assets/admin/js/ckeditor/config.js") }}'
        });

    </script>
    <script>
        $(document).on('change', '#Maincategory_id', function (event) {
            var category_id = $(this).val(); //soeciality_id
            var URL = "{{ url('/') }}";
            if (category_id != "") {
                $.ajax({
                    url: '{{ url($local) }}/eg/products/get_subcategory/' + category_id,
                    type: 'get',
                }).done(function (data) {
                    $("#sub_Cat").html(data);

                })
            }
        });
    $(document).on('change', '#sub_Cat', function (event) {
            var category_id = $(this).val(); //soeciality_id
            var URL = "{{ url('/') }}";
            if (category_id != "") {
                $.ajax({
                    url: '{{ url($local) }}/eg/products/properties/' + category_id,
                    type: 'get',
                }).done(function (data) {
                    $("#properties").html(data);

                });

                $.ajax({
                    url: '{{ url($local) }}/eg/products/category_brands/' + category_id,
                    type: 'get',
                }).done(function (data) {
                    var $el = $('#brand_id');
                    $el.empty(); // remove old options
                    var newOptions=data.data;
                    $el.append($("<option></option>")
                        .attr("value", '').text({{ trans('dashboard.brand') }}));
                    $.each(newOptions, function(key,value) {
                        $el.append($("<option></option>")
                            .attr("value", value.id).text(value.name));
                    });


                });
            }
        });


        $(document).on('click', '.add-other', function (event) {
            var oth = $('.fields').html();

            var final = "<div class='row'>" + "<div class='col-md-1' style=\"margin-bottom: 30px\"><a class='btn btn-danger btn-large del-other' ><i class='fa fa-trash'></i></a></div>" + oth + "</div>";
            $('#other').append(final);

        });

        $(document).on('click', '.del-other', function () {
            $(this).parent().parent().html('');
        });

    </script>
@endsection
