@extends('layouts.frontend.user')
@section('profile_content')
    <div class="page-content">
        <div class="page-header">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">{{ trans('dashboard.add_product') }}</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        {!! Form::open(['url' =>$local.'/eg/profile/products/storeProduct', 'method' => 'POST' , 'files' => true]) !!}
                        @include('frontend.user.products.form', ['btn' =>trans('dashboard.save_changes'), 'classes' => 'btn-primary btn-xs btn-block'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
