@extends('layouts.frontend.user')
@section('profile_content')
        <h2 class="title">{{ trans('front.wishlist') }}</h2>
        <div class="table-responsive">
            <table class="table table-bordered table-hover wishlist">
                <thead>
                <tr>
                    <td class="text-center">{{ trans('front.image') }}</td>
                    <td class="text-center">{{ trans('front.product_name') }}</td>
                    <td class="text-center">{{ trans('front.availability') }}</td>
                    <td class="text-center">{{ trans('front.unit_price') }}</td>
                    <td class="text-center">{{ trans('front.action') }}</td>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                <tr id="product_row{{ $product->id }}">
                    <td class="text-center">
                        <a href="{{ url($local.'/eg')}}/product_view/{{$product->id . '/' . $product->name}}"><img width="70px"
                         src="{{ asset('assets/images/products/mobile/'.$product->image) }}" alt="{{ $product->name }}"
                                                    title="{{ $product->name }}">
                        </a>
                    </td>
                    <td class="text-center"><a href="{{ url($local.'/eg')}}/product_view/{{$product->id . '/' . $product->name}}">{{ $product->name }}</a>
                    </td>
                    <td class="text-center">@if($product->is_avail) {{ trans('front.avail') }} @else {{ trans('front.inavail') }}  @endif </td>
                    <td class="text-center">
                        <div class="price"> <span class="price-new">{{ $product->price }}</span> </div>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-primary" title="" data-toggle="tooltip" onclick="cart.add('{{ $product->id }}',1,'{{ trans('front.my_cart') }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($local.'/eg/products/cart/add') }}','{{ url($local.'/eg/products/cart/small') }}');"
                                type="button" data-original-title="Add to Cart">
                            <i class="fa fa-shopping-cart"></i>
                        </button>
                        <button class="btn btn-danger" title="" data-toggle="tooltip"
                                onclick="wishlist.add('{{ $product->id }}','{{ trans("front.product_added_wishlist_title")  }}','{{ asset('/assets/images/products/mobile/'.$product->image) }}','{{ url($local.'/products/like') }}');element.remove('product_row{{ $product->id }}')" data-original-title="Remove"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>


@endsection