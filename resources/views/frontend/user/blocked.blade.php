@extends('layouts.frontend.user')
@section('profile_content')
    @foreach($block_list as $follower)
        <div class="col col-md-4">
            <div class="userBlock">
                <div class="backgrounImg">
                    <img src="{{ asset($follower->bg) }}">
                </div>
                <div class="userImg">
                    <img src="{{ asset($follower->image) }}">
                </div>
                <div class="userDescription">
                    <h5>
                        <a href="{{url($local.'/eg/profile/'. $follower->id .'/'. str_replace(' ', '_', $follower->name)) }}">{{ $follower->name }}</a>
                    </h5>
                    <p>{{ $follower->mobile }}</p>
                    <div class="followrs">
                        <span class="number number{{ $follower->id }}">{{  $follower->no_follows  }}</span>
                        <span> {{ trans('front.follower') }}</span>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->isUseFollow($follower->id))
                            <button class="btn btn-danger"
                                    onclick="follow.add('{{  $follower->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $follower->id }}">{{ trans('front.unfollow') }}
                            </button>
                        @else
                            <button class="btn btn-primary"
                                    onclick="follow.add('{{  $follower->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $follower->id }}">{{ trans('front.follow') }}
                            </button>
                        @endif
                    @else
                        <button class="btn btn-primary" data-toggle="modal"
                                data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                    @endif

                </div>

            </div>
        </div>
    @endforeach
    {!! $block_list->render() !!}

@endsection