@extends('layouts.frontend.master')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection
@section('content')

@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($local.'/eg')}}/category_view/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}"
                               class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $category)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($local.'/eg')}}/category_view/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($category->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($local.'/eg')}}/category_view/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
<div class="row">
    <ul class="breadcrumb">
        <li><a href="{{url($localization.'/')}}"><i class="fa fa-home"></i> {{trans('front.home')}}</a></li>
        <li>{{ $page[$local.'_name'] }}</li>
    </ul>
    <!--Middle Part Start-->
    <div id="content" class="col-md-9 col-sm-8">
        {!! $page[$local . '_desc'] !!}
    </div>
@endsection

