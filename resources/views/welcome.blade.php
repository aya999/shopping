@extends('layouts.frontend.master')
@section('content')
<!-- Main Container  -->
<div class="main-container">
        <div id="content">
            <div class="container">
                <div class=" box-content1">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-left">
                            <!-- Secondary menu -->
                            <div class="responsive so-megamenu  megamenu-style-dev">
                                <div class="so-vertical-menu ">
                                    <nav class="navbar-default">
                                        <div class="container-megamenu vertical">
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                        <ul class="megamenu">
                                                            <li class="item-vertical  with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Gifts & Toys</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="60">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Apparel</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Accessories
                                                                                                                for
                                                                                                                Tablet
                                                                                                                PC</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Accessories
                                                                                                                for i
                                                                                                                Pad</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Accessories
                                                                                                                for
                                                                                                                iPhone</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Bags,
                                                                                                                Holiday
                                                                                                                Supplies</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Car
                                                                                                                Alarms
                                                                                                                and
                                                                                                                Security</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Car
                                                                                                                Audio
                                                                                                                &amp;
                                                                                                                Speakers</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Cables
                                                                                                        &amp;
                                                                                                        Connectors</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Cameras
                                                                                                                &amp;
                                                                                                                Photo</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Electronics</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Outdoor
                                                                                                                &amp;
                                                                                                                Traveling</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Camping
                                                                                                        &amp; Hiking</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Earings</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Shaving
                                                                                                                &amp;
                                                                                                                Hair
                                                                                                                Removal</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Salon
                                                                                                                &amp;
                                                                                                                Spa
                                                                                                                Equipment</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Smartphone
                                                                                                        &amp;
                                                                                                        Tablets</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Sports
                                                                                                                &amp;
                                                                                                                Outdoors</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Bath
                                                                                                                &amp;
                                                                                                                Body</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Bags,
                                                                                                        Holiday
                                                                                                        Supplies</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#"
                                                                                                               onclick="window.location = '18_46';">Battereries
                                                                                                                &amp;
                                                                                                                Chargers</a>
                                                                                                        </li>
                                                                                                        <li><a href="#"
                                                                                                               onclick="window.location = '24_64';">Bath
                                                                                                                &amp;
                                                                                                                Body</a>
                                                                                                        </li>
                                                                                                        <li><a href="#"
                                                                                                               onclick="window.location = '18_45';">Headphones,
                                                                                                                Headsets</a>
                                                                                                        </li>
                                                                                                        <li><a href="#"
                                                                                                               onclick="window.location = '18_30';">Home
                                                                                                                Audio</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Fashion & Accessories</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical  style1 with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="label"></span>
                                                                    <span class="dot"></span>
                                                                    <span>Electronic</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="40">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Smartphone</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Esdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Scanners</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Apple</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Dell</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Scanners</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Asdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Diam
                                                                                                                sit</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Labore
                                                                                                                et</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Monitors</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row banner">
                                                                                    <a href="#">
                                                                                        <img src="{{ asset('assets/frontend') }}/image/catalog/menu/megabanner/vbanner1.jpg"
                                                                                             alt="banner1">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Health &amp; Beauty</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="60">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Car
                                                                                                        Alarms and
                                                                                                        Security</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Car
                                                                                                                Audio
                                                                                                                &amp;
                                                                                                                Speakers</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Headphones,
                                                                                                                Headsets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="24"
                                                                                                       onclick="window.location = '24';"
                                                                                                       class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Home
                                                                                                                Audio</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Helicopters
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Outdoor
                                                                                                                &amp;
                                                                                                                Traveling</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Toys
                                                                                                                &amp;
                                                                                                                Hobbies</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Earings</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Salon
                                                                                                                &amp;
                                                                                                                Spa
                                                                                                                Equipment</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Shaving
                                                                                                                &amp;
                                                                                                                Hair
                                                                                                                Removal</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Smartphone
                                                                                                                &amp;
                                                                                                                Tablets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Sports
                                                                                                        &amp;
                                                                                                        Outdoors</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Flashlights
                                                                                                                &amp;
                                                                                                                Lamps</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Fragrances</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Fishing</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">FPV
                                                                                                                System
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">More
                                                                                                        Car
                                                                                                        Accessories</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Lighter
                                                                                                                &amp;
                                                                                                                Cigar
                                                                                                                Supplies</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mp3
                                                                                                                Players
                                                                                                                &amp;
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Men
                                                                                                                Watches</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mobile
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Gadgets
                                                                                                        &amp; Auto
                                                                                                        Parts</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                &amp;
                                                                                                                Lifestyle
                                                                                                                Gadgets</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Man</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical css-menu with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Smartphone &amp; Tablets</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="20">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12 hover-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Headphones,
                                                                                                        Headsets</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Home
                                                                                                        Audio</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                       class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Health & Beauty</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Bathroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Health &amp; Beauty</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Toys &amp; Hobbies </span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Jewelry &amp; Watches</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Home &amp; Lights</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="dot"></span>
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>
                                                            <li class="loadmore">
                                                                <i class="fa fa-plus-square-o"></i>
                                                                <span class="more-view">More Categories</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <!-- // end Secondary menu -->
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-right">
                            <div class="module sohomepage-slider ">
                                <div class="yt-content-slider" data-rtl="yes" data-autoplay="no" data-autoheight="no"
                                     data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1"
                                     data-items_column1="1" data-items_column2="1" data-items_column3="1"
                                     data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes"
                                     data-loop="yes" data-hoverpause="yes">
                                    <div class="yt-content-slide">
                                        <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/slideshow/home3/slide-1.jpg" alt="slider1"
                                                         class="img-responsive"></a>
                                    </div>
                                    <div class="yt-content-slide">
                                        <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/slideshow/home3/slide-2.jpg" alt="slider2"
                                                         class="img-responsive"></a>
                                    </div>
                                    <div class="yt-content-slide">
                                        <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/slideshow/home3/slide-3.jpg" alt="slider3"
                                                         class="img-responsive"></a>
                                    </div>
                                </div>
                                <div class="loadeding"></div>
                            </div>
                            <div class="block-policy3">
                                <ul>
                                    <li class="item-1">
                                        <div class="item-inner">
                                            <div class="icon icon1"></div>
                                            <div class="content">
                                                <a href="#">Fast & Free Shipping</a>
                                                <p>on all orders $99</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-2">
                                        <div class="item-inner">
                                            <div class="icon icon2"></div>
                                            <div class="content">
                                                <a href="#">100% Money G.uarantee</a>
                                                <p>30 days mone.y back</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-5">
                                        <div class="item-inner">
                                            <div class="icon icon5"></div>
                                            <div class="content">
                                                <a href="#">Safe Shopping</a>
                                                <p>Safe Shopping Guarantee</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banners banners1">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-11.jpg" alt="Banner"></a>
                        </div>
                        <div class="col-md-6">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-12.jpg" alt="Banner"></a>
                        </div>
                    </div>
                </div>
                <div class="banners banners2">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-2.jpg" alt="Banner"></a>
                        </div>
                    </div>
                </div>
                <div class="module categoties3">
                    <div class="box-title">
                        <h3 class="modtitle"><span>Selected Categories</span></h3>
                        <a class="view-all " href="#">View all <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="module listingtab-icons3">
                        <div class="modcontent">
                            <div id="so_listing_tabs_1" class="so-listing-tabs first-load">
                                <div class="loadeding"></div>
                                <div class="ltabs-wrap">
                                    <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300"
                                         data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0"
                                         data-lg="5" data-md="4" data-sm="2" data-xs="1" data-margin="30">
                                        <!--Begin Tabs-->
                                        <div class="ltabs-tabs-wrap">
                                            <span class='ltabs-tab-selected'></span>
                                            <span class="ltabs-tab-arrow">▼</span>
                                            <ul class="ltabs-tabs cf list-sub-cat font-title">
                                                <li class="ltabs-tab tab-sel tab-loaded" data-category-id="72"
                                                    data-active-content=".items-category-72">
                                                    <span class="ltabs-tab-label hidden-md hidden-lg"> Electronics</span>
                                                    <div class="category-title">
                                                        <img class="hidden-xs" src="{{ asset('assets/frontend') }}/image/catalog/demo/product/90/6.jpg"
                                                             alt="">
                                                        <span class="ltabs-tab-label"> Electronics</span>
                                                    </div>
                                                </li>
                                                <li class="ltabs-tab" data-category-id="51"
                                                    data-active-content=".items-category-51">
                                                    <span class="ltabs-tab-label hidden-md hidden-lg">Cellphone</span>
                                                    <div class="category-title">
                                                        <img class="hidden-xs" src="{{ asset('assets/frontend') }}/image/catalog/demo/product/90/7.jpg"
                                                             alt="">
                                                        <span class="ltabs-tab-label">Cellphone</span>
                                                    </div>
                                                </li>
                                                <li class="ltabs-tab" data-category-id="52"
                                                    data-active-content=".items-category-52">
                                                    <span class="ltabs-tab-label hidden-md hidden-lg">Fashion</span>
                                                    <div class="category-title">
                                                        <img class="hidden-xs" src="{{ asset('assets/frontend') }}/image/catalog/demo/product/90/8.jpg"
                                                             alt="">
                                                        <span class="ltabs-tab-label">Fashion</span>
                                                    </div>
                                                </li>
                                                <li class="ltabs-tab" data-category-id="53"
                                                    data-active-content=".items-category-53">
                                                    <span class="ltabs-tab-label hidden-md hidden-lg">Furnicoms</span>
                                                    <div class="category-title">
                                                        <img class="hidden-xs" src="{{ asset('assets/frontend') }}/image/catalog/demo/product/90/1.jpg"
                                                             alt="">
                                                        <span class="ltabs-tab-label">Furnicoms</span>
                                                    </div>
                                                </li>
                                                <li class="view-all">
                                                    <a href="#">
                                                        <span class="dot hidden-xs hidden-sm hidden-md">....</span>
                                                        <span class="text">View All Categories</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- End Tabs-->
                                    </div>
                                    <div class=" ltabs-items-container products-list ">
                                        <div class="ltabs-items ltabs-items-selected items-category-72" data-total="6">
                                            <div class="ltabs-items-inner3 ">
                                                <div class="col-product">
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$45.00</div>
                                                        </div>
                                                    </div>
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$454.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-product">
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$787.00</div>
                                                        </div>
                                                    </div>
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$78.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-product hidden-md">
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$124.00</div>
                                                        </div>
                                                    </div>
                                                    <div class="product">
                                                        <div class="left-block">
                                                            <a href="product.html"><img
                                                                        src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                        alt=""></a>
                                                        </div>
                                                        <div class="right-block">
                                                            <h4 class="title-product"><a href="product.html">ut labore
                                                                    et do</a></h4>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>
                                                            <div class="price">$175.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-items items-category-51 grid" data-total="5">
                                            <div class="ltabs-loading"></div>
                                        </div>
                                        <div class="ltabs-items  items-category-52 grid" data-total="5">
                                            <div class="ltabs-loading"></div>
                                        </div>
                                        <div class="ltabs-items items-category-53 grid" data-total="4">
                                            <div class="ltabs-loading"></div>
                                        </div>
                                        <div class="categories-text">
                                            <ul>
                                                <li><a href="category.html">Computers</a></li>
                                                <li><a href="category.html">Smartphones</a></li>
                                                <li><a href="category.html">Cameras</a></li>
                                                <li><a href="category.html">Accessories</a></li>
                                                <li><a href="category.html">Headphones</a></li>
                                                <li><a href="category.html">Electronics</a></li>
                                                <li><a href="category.html">Clothings</a></li>
                                                <li><a href="category.html">Bags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banners banners3">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="banner31">
                                <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-31.jpg" alt=""></a>
                            </div>
                            <div class="banner32">
                                <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-32.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="banner33">
                                <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-33.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="banner34">
                                <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-34.jpg" alt=""></a>
                            </div>
                            <div class="banner35">
                                <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-35.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-content2">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="deals-product">
                                <h3 class="title-deals">Deals Of The Day</h3>
                                <div class="so-extraslider">
                                    <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                         data-pagination="no" data-autoplay="no" data-delay="4" data-speed="0.6"
                                         data-margin="30" data-items_column0="1" data-items_column1="2"
                                         data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                         data-arrows="yes" data-lazyload="yes" data-loop="yes" data-buttonpage="top">
                                        <div class="item">
                                            <div class="product-thumb transition">
                                                <div class="row">
                                                    <div class="inner">
                                                        <div class="item-left ">
                                                            <div class="image">
                                                                <span class="label-product label-product-sale">-26%</span>
                                                                <a href="product.html" target="_self">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/390/2.jpg"
                                                                         alt="Ground round enim" class="img-responsive">
                                                                </a>
                                                                <div class="button-group so-quickview">
                                                                    <button class="btn-button addToCart"
                                                                            title="Add to Cart" type="button"
                                                                            onclick="cart.add('101');"><i
                                                                                class="fa fa-shopping-basket"></i>
                                                                        <span>Add to Cart</span>
                                                                    </button>
                                                                    <button class="btn-button wishlist" type="button"
                                                                            title="Add to Wish List"
                                                                            onclick="wishlist.add('101');"><i
                                                                                class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                    </button>
                                                                    <button class="btn-button compare" type="button"
                                                                            title="Compare this Product"
                                                                            onclick="compare.add('101');"><i
                                                                                class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                    </button>
                                                                    <!--quickview-->
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                       href="quickview.html" title="Quick view"
                                                                       data-fancybox-type="iframe"><i
                                                                                class="fa fa-eye"></i><span>Quick view</span></a>
                                                                    <!--end quickview-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-right ">
                                                            <div class="caption">
                                                                <h4><a href="product.html" target="_self"
                                                                       title="Ground round enim">Ground round enim</a>
                                                                </h4>

                                                                <p class="price"><span class="price-old">$76.00</span>
                                                                    <span class="price-new">$66.00</span>
                                                                </p>
                                                                <div class="item-available">
                                                                    <div class="row">
                                                                        <p class="col-xs-6 a1">Available: <b>58</b>
                                                                        </p>
                                                                        <p class="col-xs-6 a2">Sold: <b>36</b>
                                                                        </p>
                                                                    </div>
                                                                    <div class="available"><span class="color_width"
                                                                                                 data-title="40%"
                                                                                                 data-toggle='tooltip'
                                                                                                 style="width: 60%"></span>
                                                                    </div>
                                                                </div>
                                                                <!--countdown box-->
                                                                <div class="item-time-w">
                                                                    <div class="item-time">
                                                                        <div class="item-timer">
                                                                            <div class="defaultCountdown-30"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end countdown box-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb transition">
                                                <div class="row">
                                                    <div class="inner">
                                                        <div class="item-left ">
                                                            <div class="image">
                                                                <span class="label-product label-product-sale">-13%</span>
                                                                <a href="product.html" target="_self">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/390/1.jpg"
                                                                         alt="Ground round enim" class="img-responsive">
                                                                </a>
                                                                <div class="button-group so-quickview">
                                                                    <button class="btn-button addToCart"
                                                                            title="Add to Cart" type="button"
                                                                            onclick="cart.add('101');"><i
                                                                                class="fa fa-shopping-basket"></i>
                                                                        <span>Add to Cart</span>
                                                                    </button>
                                                                    <button class="btn-button wishlist" type="button"
                                                                            title="Add to Wish List"
                                                                            onclick="wishlist.add('101');"><i
                                                                                class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                    </button>
                                                                    <button class="btn-button compare" type="button"
                                                                            title="Compare this Product"
                                                                            onclick="compare.add('101');"><i
                                                                                class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                    </button>
                                                                    <!--quickview-->
                                                                    <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                       href="quickview.html" title="Quick view"
                                                                       data-fancybox-type="iframe"><i
                                                                                class="fa fa-eye"></i><span>Quick view</span></a>
                                                                    <!--end quickview-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-right ">
                                                            <div class="caption">
                                                                <h4><a href="product.html" target="_self"
                                                                       title="Ground round enim">Ground round enim</a>
                                                                </h4>

                                                                <p class="price"><span class="price-old">$95.00</span>
                                                                    <span class="price-new">$65.00</span></p>
                                                                <div class="item-available">
                                                                    <div class="row">
                                                                        <p class="col-xs-6 a1">Available: <b>60</b>
                                                                        </p>
                                                                        <p class="col-xs-6 a2">Sold: <b>40</b>
                                                                        </p>
                                                                    </div>
                                                                    <div class="available"><span class="color_width"
                                                                                                 data-title="40%"
                                                                                                 data-toggle='tooltip'
                                                                                                 style="width: 40%"></span>
                                                                    </div>
                                                                </div>
                                                                <!--countdown box-->
                                                                <div class="item-time-w">
                                                                    <div class="item-time">
                                                                        <div class="item-timer">
                                                                            <div class="defaultCountdown-30"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end countdown box-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="module so-listing-tabs-ltr listingtab-layout3">
                                <div class="modcontent">
                                    <div id="so_listing_tabs_2" class="so-listing-tabs first-load">
                                        <div class="loadeding"></div>
                                        <div class="ltabs-wrap">
                                            <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300"
                                                 data-duration="600" data-effect="starwars" data-ajaxurl=""
                                                 data-type_source="0" data-lg="4" data-md="4" data-sm="2" data-xs="1"
                                                 data-xxs="1" data-margin="0">
                                                <!--Begin Tabs-->
                                                <div class="ltabs-tabs-wrap">
                                                    <span class="ltabs-tab-selected">Bathroom</span> <span
                                                            class="ltabs-tab-arrow">▼</span>
                                                    <div class="item-sub-cat">
                                                        <ul class="ltabs-tabs cf">
                                                            <li class="ltabs-tab tab-sel" data-category-id="31"
                                                                data-active-content=".items-category-31"><span
                                                                        class="ltabs-tab-label">New Arrivals</span></li>
                                                            <li class="ltabs-tab " data-category-id="32"
                                                                data-active-content=".items-category-32"><span
                                                                        class="ltabs-tab-label">Featured Products</span>
                                                            </li>
                                                            <li class="ltabs-tab " data-category-id="33"
                                                                data-active-content=".items-category-33"><span
                                                                        class="ltabs-tab-label">Bestsellers</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- End Tabs-->
                                            </div>
                                            <div class="ltabs-items-container products-list grid">
                                                <!--Begin Items-->
                                                <div class="ltabs-items ltabs-items-selected items-category-31"
                                                     data-total="31">
                                                    <div class="ltabs-items-inner ltabs-slider">
                                                        <div class="col-item">
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <span class="label-product label-product-sale">-26%</span>
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <p class="price"><span
                                                                                            class="price-new">$654.00</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <span class="label-product label-product-new">new</span>
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$68.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-item">
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$65.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$134.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-item">
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <span class="label-product label-product-new">new</span>
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$98.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$74.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-item">
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$35.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="ltabs-item">
                                                                <div class="item-inner product-layout transition product-grid">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container second_img">
                                                                                <a href="product.html" target="_self"
                                                                                   title="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                                         class="img-1 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                                         class="img-2 img-responsive"
                                                                                         alt="Pastrami bacon">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview cartinfo--left">
                                                                                <button type="button"
                                                                                        class="addToCart btn-button"
                                                                                        title="Add to cart"
                                                                                        onclick="cart.add('60 ');"><i
                                                                                            class="fa fa-shopping-basket"></i>
                                                                                    <span>Add to cart </span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="wishlist btn-button"
                                                                                        title="Add to Wish List"
                                                                                        onclick="wishlist.add('60');"><i
                                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="compare btn-button"
                                                                                        title="Compare this Product "
                                                                                        onclick="compare.add('60');"><i
                                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                                </button>
                                                                                <!--quickview-->
                                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                                   href="quickview.html"
                                                                                   title="Quick view"
                                                                                   data-fancybox-type="iframe"><i
                                                                                            class="fa fa-eye"></i><span>Quick view</span></a>
                                                                                <!--end quickview-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <div class="rating"><span
                                                                                            class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                    <span class="fa fa-stack"><i
                                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                                </div>
                                                                                <h4><a href="product.html"
                                                                                       title="Pastrami bacon"
                                                                                       target="_self">Pastrami bacon</a>
                                                                                </h4>
                                                                                <div class="price">$82.00</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ltabs-items items-category-32 grid" data-total="32">
                                                    <div class="ltabs-loading"></div>
                                                </div>
                                                <div class="ltabs-items  items-category-33 grid" data-total="33">
                                                    <div class="ltabs-loading"></div>
                                                </div>
                                                <!--End Items-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banners banner4">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/banners/home3/banner-4.jpg" alt="Banner"></a>
                        </div>
                    </div>
                </div>
                <div class="best-selling module">
                    <div class="block-top-selling">
                        <h3 class="modtitle"><span>Best Selling Products</span></h3>
                    </div>
                    <div class="module layout3-listingtab3">
                        <div id="so_listing_tabs_3" class="so-listing-tabs first-load">
                            <div class="loadeding"></div>
                            <div class="ltabs-wrap">
                                <div class="ltabs-tabs-container" data-rtl="yes" data-delay="300" data-duration="600"
                                     data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="5" data-md="4"
                                     data-sm="2" data-xs="2" data-xxs="1" data-margin="30">
                                    <!--Begin Tabs-->
                                    <div class="ltabs-tabs-wrap">
                                        <span class='ltabs-tab-selected'></span>
                                        <span class="ltabs-tab-arrow">▼</span>
                                        <ul class="ltabs-tabs cf list-sub-cat font-title">
                                            <li class="ltabs-tab tab-sel" data-category-id="61"
                                                data-active-content=".items-category-61"><span class="ltabs-tab-label">Top 10 Selling </span>
                                            </li>
                                            <li class="ltabs-tab  " data-category-id="62"
                                                data-active-content=".items-category-62"><span class="ltabs-tab-label">Laptops</span>
                                            </li>
                                            <li class="ltabs-tab  " data-category-id="63"
                                                data-active-content=".items-category-63"><span class="ltabs-tab-label">Computers</span>
                                            </li>
                                            <li class="ltabs-tab  " data-category-id="64"
                                                data-active-content=".items-category-64"><span class="ltabs-tab-label">Headphones</span>
                                            </li>
                                            <li class="ltabs-tab  " data-category-id="65"
                                                data-active-content=".items-category-65"><span class="ltabs-tab-label">Tv & Audio</span>
                                            </li>
                                            <li class="ltabs-tab  " data-category-id="66"
                                                data-active-content=".items-category-66"><span class="ltabs-tab-label">Electronics</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- End Tabs-->
                                </div>
                                <div class="wap-listing-tabs ltabs-items-container products-list grid">
                                    <!--Begin Items-->
                                    <div class="ltabs-items ltabs-items-selected items-category-61" data-total="10">
                                        <div class="ltabs-items-inner ltabs-slider">
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Pastrami bacon">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                         class="img-1 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                         class="img-2 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                </a>
                                                            </div>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Pastrami bacon"
                                                                       target="_self">Pastrami bacon</a></h4>
                                                                <div class="price">$42.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Chicken swinesha">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                         class="img-1 img-responsive" alt="image">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                         class="img-2 img-responsive" alt="image">
                                                                </a>
                                                            </div>
                                                            <span class="label-product label-product-sale">-15%</span>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Chicken swinesha"
                                                                       target="_self">Chicken swinesha</a></h4>
                                                                <div class="price"><span class="price-new">$46.00</span>
                                                                    <span class="price-old">$55.00</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Kielbasa hamburg">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                         class="img-1 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/8.jpg"
                                                                         class="img-2 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                </a>
                                                            </div>
                                                            <span class="label-product label-product-new">new</span>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Kielbasa hamburg"
                                                                       target="_self">Kielbasa hamburg</a></h4>
                                                                <div class="price">$55.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Sausage cowbee">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                         class="img-1 img-responsive" alt="image">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                         class="img-2 img-responsive" alt="image">
                                                                </a>
                                                            </div>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Sausage cowbeea"
                                                                       target="_self">Sausage cowbee</a></h4>
                                                                <div class="price">$60.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Kielbasa hamburg">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                         class="img-1 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                         class="img-2 img-responsive"
                                                                         alt="Pastrami bacon">
                                                                </a>
                                                            </div>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Drumstick tempor"
                                                                       target="_self">Drumstick tempor</a></h4>
                                                                <div class="price">$75.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Balltip nullaelit">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                         class="img-1 img-responsive" alt="image">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                         class="img-2 img-responsive" alt="image">
                                                                </a>
                                                            </div>
                                                            <span class="label-product label-product-new">new</span>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Balltip nullaelit"
                                                                       target="_self">Balltip nullaelit</a></h4>
                                                                <div class="price">$80.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ltabs-item">
                                                <div class="item-inner product-layout transition product-grid">
                                                    <div class="product-item-container">
                                                        <div class="left-block">
                                                            <div class="product-image-container second_img">
                                                                <a href="product.html" target="_self"
                                                                   title="Lamboudin ribeye">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                         class="img-1 img-responsive" alt="image">
                                                                    <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                         class="img-2 img-responsive" alt="image">
                                                                </a>
                                                            </div>
                                                            <div class="button-group so-quickview cartinfo--left">
                                                                <button type="button" class="addToCart btn-button"
                                                                        title="Add to cart" onclick="cart.add('60 ');">
                                                                    <i class="fa fa-shopping-basket"></i>
                                                                    <span>Add to cart </span>
                                                                </button>
                                                                <button type="button" class="wishlist btn-button"
                                                                        title="Add to Wish List"
                                                                        onclick="wishlist.add('60');"><i
                                                                            class="fa fa-heart"></i><span>Add to Wish List</span>
                                                                </button>
                                                                <button type="button" class="compare btn-button"
                                                                        title="Compare this Product "
                                                                        onclick="compare.add('60');"><i
                                                                            class="fa fa-refresh"></i><span>Compare this Product</span>
                                                                </button>
                                                                <!--quickview-->
                                                                <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                                   href="quickview.html" title="Quick view"
                                                                   data-fancybox-type="iframe"><i class="fa fa-eye"></i><span>Quick view</span></a>
                                                                <!--end quickview-->
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <div class="caption">
                                                                <div class="rating"><span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o fa-stack-2x"></i></span>
                                                                </div>
                                                                <h4><a href="product.html" title="Lamboudin ribeye"
                                                                       target="_self">Lamboudin ribeye</a></h4>
                                                                <div class="price">$63.00</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ltabs-items items-category-62 grid" data-total="10">
                                        <div class="ltabs-loading"></div>
                                    </div>
                                    <div class="ltabs-items  items-category-63 grid" data-total="10">
                                        <div class="ltabs-loading"></div>
                                    </div>
                                    <div class="ltabs-items  items-category-64 grid" data-total="10">
                                        <div class="ltabs-loading"></div>
                                    </div>
                                    <div class="ltabs-items  items-category-65 grid" data-total="10">
                                        <div class="ltabs-loading"></div>
                                    </div>
                                    <div class="ltabs-items  items-category-66
                                                grid" data-total="10">
                                        <div class="ltabs-loading"></div>
                                    </div>
                                    <!--End Items-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-brands clearfix">
                    <div class="yt-content-slider contentslider" data-rtl="no" data-loop="yes" data-autoplay="no"
                         data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6" data-margin="0"
                         data-items_column0="7" data-items_column1="6" data-items_column2="3" data-items_column3="2"
                         data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes"
                         data-hoverpause="yes">
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/1.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/2.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/3.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/4.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/5.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/6.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/7.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/1.jpg" alt="brand"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="{{ asset('assets/frontend') }}/image/catalog/brands/home1/3.jpg" alt="brand"></a>
                        </div>
                    </div>
                </div>
                <div class="box-content3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="new-arrivals module block-product">
                                <h3 class="modtitle"><span>New Arrivals</span></h3>
                                <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                     data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                     data-margin="0" data-items_column0="2" data-items_column1="2"
                                     data-items_column2="2" data-items_column3="2" data-items_column4="1"
                                     data-arrows="yes" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$42.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$75.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$81.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$74.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$90.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$75.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$64.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$81.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="new-arrivals module block-product">
                                <h3 class="modtitle"><span>On Sale Products</span></h3>
                                <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                     data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                     data-margin="0" data-items_column0="2" data-items_column1="2"
                                     data-items_column2="2" data-items_column3="2" data-items_column4="1"
                                     data-arrows="yes" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$64.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$47.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$84.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$74.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$51.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/1.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$84.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$81.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$49.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="most-viewed module block-product">
                                <h3 class="modtitle"><span>Most Viewed</span></h3>
                                <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                                     data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                     data-margin="0" data-items_column0="2" data-items_column1="2"
                                     data-items_column2="2" data-items_column3="2" data-items_column4="1"
                                     data-arrows="yes" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$71.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/16.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$65.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/18.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/17.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$54.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/2.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/3.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$44.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/4.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/5.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$73.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/6.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/13.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$96.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$87.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ltabs-item">
                                            <div class="item-inner product-layout transition product-grid">
                                                <div class="product-item-container">
                                                    <div class="left-block">
                                                        <div class="product-image-container second_img">
                                                            <a href="product.html" target="_self"
                                                               title="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/14.jpg"
                                                                     class="img-1 img-responsive" alt="Pastrami bacon">
                                                                <img src="{{ asset('assets/frontend') }}/image/catalog/demo/product/250/15.jpg"
                                                                     class="img-2 img-responsive" alt="Pastrami bacon">
                                                            </a>
                                                        </div>
                                                        <div class="button-group so-quickview cartinfo--left">
                                                            <button type="button" class="addToCart btn-button"
                                                                    title="Add to cart" onclick="cart.add('60 ');"><i
                                                                        class="fa fa-shopping-basket"></i>
                                                                <span>Add to cart </span>
                                                            </button>
                                                            <button type="button" class="wishlist btn-button"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('60');"><i
                                                                        class="fa fa-heart"></i><span>Add to Wish List</span>
                                                            </button>
                                                            <button type="button" class="compare btn-button"
                                                                    title="Compare this Product "
                                                                    onclick="compare.add('60');"><i
                                                                        class="fa fa-refresh"></i><span>Compare this Product</span>
                                                            </button>
                                                            <!--quickview-->
                                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                               href="quickview.html" title="Quick view"
                                                               data-fancybox-type="iframe"><i
                                                                        class="fa fa-eye"></i><span>Quick view</span></a>
                                                            <!--end quickview-->
                                                        </div>
                                                    </div>
                                                    <div class="right-block">
                                                        <div class="caption">
                                                            <div class="rating"><span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                                            </div>
                                                            <h4><a href="product.html" title="Pastrami bacon"
                                                                   target="_self">Pastrami bacon</a></h4>
                                                            <div class="price">$68.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lastest-blog module">
                    <h3 class="modtitle"><span>Latest From Blogs</span></h3>
                    <div class="list-blog">
                        <div class="yt-content-slider extraslider-inner products-list" data-rtl="yes"
                             data-pagination="no" data-autoplay="no" data-delay="4" data-speed="0.6" data-margin="30"
                             data-items_column0="4" data-items_column1="4" data-items_column2="2" data-items_column3="2"
                             data-items_column4="1" data-arrows="yes" data-lazyload="yes" data-loop="no"
                             data-buttonpage="top">
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/1.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">14</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 4 comments</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/2.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">23</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 15 comments</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/3.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">25</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 1 comments</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/4.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">27</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 23 comments</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/2.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">28</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 3 comments</div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="top-block">
                                    <a href="blog-detail.html">
                                        <img class="content" src="{{ asset('assets/frontend') }}/image/catalog/blog/home3/3.jpg" alt="">
                                        <img class="icon" src="{{ asset('assets/frontend') }}/image/theme/icons/blog.png" alt="">
                                    </a>
                                    <div class="date-blog">
                                        <div class="day-time">29</div>
                                        <div class="mon-time">Oct</div>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <h4><a href="blog-detail.html">Winner Of Primetime Emmy Award And Outstanding Comedy
                                            Series, It's A Must Watch.</a></h4>
                                    <div class="entry-meta">
                                        <div class="entry-date"><i class="fa fa-calendar"></i> Feb 28, 2017</div>
                                        <div class="entry-comment"><i class="fa fa-comments"></i> 0 comments</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //Main Container -->
@endsection