@extends('layouts.frontend.master')
@section('content')
    <div class="main-container ">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-left">
                @include('includes.frontend.side_menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-right">
                <ul class="breadcrumb">
                    <li><a href="#">{{trans('front.home')}}</a></li>
                    <li><a href="#">{{trans('front.account')}}</a></li>
                    <li><a href="#">{{trans('front.login')}}</a></li>
                </ul>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-center"><i class="fa fa-user"></i>  </h4>
                </div>
                <div class="panel-body row">

                    <div class="col-sm-4 new-customer">
                        <div class="">

                            <a href="{{ url('login/facebook') }}" class="btn btn-primary facebook">
                                <i style="margin: 10px" class="fa fa-facebook "></i>
                                {{trans('front.face_login')}}
                            </a>
                            <a href="{{ url('login/google') }}" class="btn btn-primary google">
                                <i style="margin: 10px" class="fa fa-google"></i>
                                {{trans('front.google_login')}}
                            </a>
                        </div>

                    </div>
                    <form action="{{ url($localization.'/register') }}" method="post" enctype="multipart/form-data"
                    <div class="col-sm-8 customer-login">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label " for="input-email">{{trans('dashboard.name')}}</label>
                            {!! Form::text('name', null, ['class' => 'form-control mb-15']) !!}
                            <small class="error">{{ $errors->first('name') }}</small>

                        </div>
                        <div class="form-group">
                            <label class="control-label " for="input-email">{{trans('dashboard.email')}}</label>
                            {!! Form::text('email', null, ['class' => 'form-control mb-15']) !!}
                            <small class="error">{{ $errors->first('email') }}</small>
                        </div>

                        <div class="form-group">
                            <label class="control-label " for="input-email">{{trans('dashboard.mobile')}}</label>
                            {!! Form::text('mobile', null, ['class' => 'form-control mb-15']) !!}
                            <small class="error">{{ $errors->first('mobile') }}</small>
                        </div>
                        <div class="form-group">
                            <label class="control-label " for="input-password">{{trans('dashboard.password')}}</label>
                            {!! Form::password('password', ['class' => 'form-control mb-15']) !!}
                            <small class="error">{{ $errors->first('password') }}</small>
                        </div>
                        <div class="form-group">
                            <label class="control-label " for="input-password">{{trans('dashboard.confirm_password')}}</label>
                            {!! Form::password('password_confirmation', ['class' => 'form-control mb-15']) !!}
                            <small class="error">{{ $errors->first('en_name') }}</small>

                        </div>

                        <div class="bottom-form">
                            <input type="submit" value="{{trans('front.submit')}}" class="btn btn-default pull-right">
                        </div>

                    </div>


                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>


        <!-- Include Libs & Plugins
        ============================================ -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-2.2.4.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/frontend') }}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/frontend') }}/js/jquery-ui/jquery-ui.min.js"></script>


        <!-- Theme files
        ============================================ -->

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=333791084037551&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <script>
            function onSuccess(googleUser) {
                console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
            }

            function onFailure(error) {
                console.log(error);
            }

            function renderButton() {
                gapi.signin2.render('my-signin2', {
                    'scope': 'profile email',
                    'width': 240,
                    'height': 40,
                    'longtitle': true,
                    'theme': 'dark',
                    'onsuccess': onSuccess,
                    'onfailure': onFailure
                });
            }
        </script>
        <script>
            $(document).ready(function () {
                $('body').height($(window).height());
                $('.error-box').delay(3000).fadeOut(500);
            });
        </script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

    </div>
    </body>
    </html>
@endsection
