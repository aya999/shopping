@extends('layouts.frontend.master')
@section('content')
      <div class="main-container ">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-left">
                @include('includes.frontend.side_menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-right">
                <ul class="breadcrumb">
                    <li><a href="#">{{trans('front.home')}}</a></li>
                    <li><a href="#">{{trans('front.account')}}</a></li>
                    <li><a href="#">{{trans('front.login')}}</a></li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center"><i class="fa fa-unlock"></i>{{trans('front.log_desc')}}
                        </h4>
                    </div>
                    <div class="panel-body row">

                        <div class="col-sm-4 new-customer">
                            <div class="">

                                <a href="{{ url('login/facebook') }}" class="btn btn-primary facebook">
                                    <i style="margin: 10px" class="fa fa-facebook "></i>
                                    {{trans('front.face_login')}}
                                </a>
                                <a href="{{ url('login/google') }}" class="btn btn-primary google">
                                    <i style="margin: 10px" class="fa fa-google"></i>
                                    {{trans('front.google_login')}}
                                </a>

                            </div>

                        </div>

                        {!! Form::open(['url'=>$local.'/login']) !!}
                        <div class="col-sm-8 customer-login">
                            <div class="">
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger error-box">
                                        <p>{{trans('front.error_login')}}</p>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label " for="input-email">{{trans('dashboard.email')}}</label>
                                    {!! Form::text('email', null, ['class' => 'form-control mb-15']) !!}
                                </div>
                                <div class="form-group">
                                    <label class="control-label " for="input-password">{{trans('dashboard.password')}}</label>
                                    {!! Form::password('password', ['class' => 'form-control mb-15']) !!}
                                </div>
                            </div>
                            <div class="bottom-form">
                                <a href="#" class="forgot">{{trans('front.forget_pass')}}</a>
                                <input type="submit" value="{{trans('front.login')}}" class="btn btn-default pull-right">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>




    </div>
    </body>

    </html>
@endsection
