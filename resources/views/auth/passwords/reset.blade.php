@extends('layouts.frontend.login')
@section('content')
    <div class="col-md-4"></div>
    <div class="col-md-4  text-center login-form">
        <h1 class="white">تغير كلمة المرور</h1>
        @if(Session::has('status'))
            <div class="text-center alert alert-success flash">
                {{  Session::get('status') }}
            </div>
        @endif
        {!! Form::open(['route' => 'resetPassword']) !!}
        {!! Form::hidden('token', $token) !!}
        <div class="form-group">
            {{ Form::email('email', null, ['class'=>'form-control ' . ($errors->has('email') ? 'redborder' : '')  , 'id'=>'email', 'required' => 'required', 'placeholder'=>'البريد الالكترونى']) }}
            <small class="text-danger">{{ $errors->first('email') }}</small>
            {!! Form::password('password', ['class' => 'form-control mb-15','placeholder'=>'كلمة المرور']) !!}
            {{ Form::password('password_confirmation', ['class'=>'form-control ' . ($errors->has('password_confirmation') ? 'redborder' : '')  , 'id'=>'password_confirmation', 'required' => 'required', 'placeholder'=>'اعد كتابة كلمة المرور']) }}

        </div>

        <input class="btn btn-primary btn-block" type="submit" name="submit" value="تعديل  ">
        {!! Form::close() !!}
        @if(count($errors) > 0)
            <div class="error-box">
                <p>يوجد خطا  </p>
            </div>
        @endif
    </div>
@stop