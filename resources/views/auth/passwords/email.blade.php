@extends('layouts.frontend.login')
@section('content')
    <div class="col-md-4">

    </div>
    <div class="col-md-4 text-center login-form">
        <h1 class="white">استعادة الحساب</h1>
        @if(Session::has('status'))
            <div class="text-center alert alert-success flash">
                {{  Session::get('status') }}
            </div>
        @endif
        {!! Form::open(['route' => 'sendResetEmail']) !!}
        <div class="form-group">
            {!! Form::text('email', null, ['class' => 'form-control mb-15']) !!}
        </div>
        <input class="btn btn-primary btn-block" type="submit" name="submit" value="ارسل الايميل التاكيدى">
        {!! Form::close() !!}
        @if(count($errors) > 0)
            <div class="error-box">
                <p>يوجد خطا في البريد</p>
            </div>
        @endif
    </div>
@stop