@extends('layouts.email')

@section('content')

    @component('emails.plugin.greeting') مرحبا, {{ $data['user']->name }} @endcomponent

    @component('emails.plugin.paragraph')
        {{ $data['message'] }}
     @endcomponent


    @component('emails.plugin.button', ['bg_color' => '#065191', 'color' => '#FFF', 'link' =>$data['link'] ])
        {{ $data['btn'] }}
     @endcomponent

    @component('emails.plugin.signature') واي ماب | دليلك لسياحة افضل  @endcomponent
@stop

@section('copy', 'جميع الحقوقو محفوظة')

