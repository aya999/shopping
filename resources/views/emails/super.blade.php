@extends('layouts.email')

@section('content')

    @component('emails.plugin.greeting') {{ $data['welcome_text'] }}@endcomponent

    @component('emails.plugin.paragraph')
        {{ $data['message'] }}
    @endcomponent


    @component('emails.plugin.button', ['bg_color' => '#065191', 'color' => '#FFF', 'link' =>$data['link'] ])
        {{ $data['btn'] }}
    @endcomponent

    @component('emails.plugin.signature') {{ $data['signature'] }}  @endcomponent
@stop

@section('copy', $data['signature'])
