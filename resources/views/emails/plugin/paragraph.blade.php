<p style="font-size: 12px;font-style: italic; text-align: {{ isset($align) ? $align : 'right' }}">
    {{ $slot }}
</p>