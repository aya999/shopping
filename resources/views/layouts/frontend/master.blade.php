@include('includes.frontend.header')
<div class="main-container">
    <div id="content" class="allContainer">
        <div class="container inside">
            @yield('content')
        </div>
    </div>
</div>
@include('includes.frontend.footer')



