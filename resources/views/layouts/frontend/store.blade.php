@extends('layouts.frontend.master')
@section('content')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection
@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($local.'/eg')}}/category_view/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}"
                               class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $category)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($local.'/eg')}}/category_view/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($category->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($local.'/eg')}}/category_view/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
<div class="row">
    <ul class="breadcrumb">
        <li><a href="{{url($localization)}}">{{trans('front.home')}}</a></li>
        <li>{{ $store[$local.'_name'] }}</li>
    </ul>
    <!--Middle Part Start-->
    <div id="content" class="col-md-12 col-sm-12">
        <div class="profile">
            <div class="backgrounImg">
                <img src="{{ asset('/assets/images/stores/web/'.$store->bg) }}">
            </div>
            <div class="userImg">
                <img src="{{  asset('/assets/images/stores/web/'.$store->image) }}">
            </div>
            <div class="userDescription">
                <h5>{{ $store[$local.'_name'] }}</h5>
                <div class="rating">
                    @for($i=0;$i <intval($store->rate());$i++)
                        <span class="fa fa-stack"><i
                                    class="fa fa-star fa-stack-2x"></i></span>
                        @if($i==5)
                            @break
                        @endif
                    @endfor
                    @for($j=$i ;$j<5 ;$j++)
                        <span class="fa fa-stack"><i
                                    class="fa fa-star-o fa-stack-2x"></i></span>
                    @endfor
                </div>
                <p>{{ $store->mobile }}</p>
                <a href="{{ url($localization.'/store/'.$store->id . '/'.str_replace(' ','-',$store[$local.'_name']).'/followers') }}"
                   class="followrs">
                    <span class="number number{{ $store->id }}">{{  $store->no_follows  }}</span>
                    <span> {{ trans('front.follower') }}</span>
                </a>
                {{--  <div class="followrs">
                      <span class="followingsnumber number">{{  $store->no_followings  }}</span>
                      <span> {{ trans('front.following') }}</span>
                  </div>--}}
                @if(Auth::check())
                    @if(Auth::user()->isUseFollow($store->id))
                        <button class="btn btn-danger"
                                onclick="follow.add('{{  $store->id }}','{{ url($local.'/users/follow') }}');"
                                id="message{{ $store->id }}">{{ trans('front.unfollow') }}
                        </button>
                    @else
                        <button class="btn btn-primary"
                                onclick="follow.add('{{  $store->id }}','{{ url($local.'/users/follow') }}');"
                                id="message{{ $store->id }}">{{ trans('front.follow') }}
                        </button>
                    @endif
                @else
                    <button class="btn btn-primary" data-toggle="modal"
                            data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                @endif

            </div>

        </div>
    </div>
    <aside class="col-sm-4" id="column-right">
        <div class="list-group">
            <div id="review">
                <table class="table table-striped table-bordered">
                    <tbody>

                    <tr>
                        <td colspan="2">
                            <ul>
                                @if(!empty($store->address))
                                    <li class="adres"><i
                                                class="fa fa-home"></i> {{ $store->address }}
                                        .
                                    </li>
                                @endif
                                @if(!empty($store->city_id))
                                    <li class="adres"><i
                                                class="fa fa-map-marker"></i> {{ $store->city[$local .'_name'] }}
                                        .
                                    </li>
                                @endif
                                @if(!empty($store->email))

                                    <li class="mail">
                                        <a href="mailto:{{ $store->email }}"><i
                                                    class="fa fa-envelope-open"></i> {{ $store->email }}
                                        </a>
                                    </li>
                                @endif
                                @if(!empty($store->mobile))

                                    <li class="phone"><i
                                                class="fa fa-phone"></i> {{ $store->mobile }}
                                    </li>
                                @endif
                                <li class="phone"><i
                                            class="fa fa-user"></i> {{ $store->user->name }}
                                </li>

                            </ul>

                            <div class="icon-share">
                                <ul class="socials">
                                    @if(!empty($store->facebook))
                                        <li class="facebook"><a class="_blank" href="{{ $store->facebook }}"
                                                                target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($store->twitter))
                                        <li class="twitter"><a class="_blank" href="{{ $store->twitter }}"
                                                               target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($store->google))
                                        <li class="google_plus"><a class="_blank"
                                                                   href="{{ $store->google }}"
                                                                   target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($store->instagram))
                                        <li class="pinterest"><a class="_blank"
                                                                 href="{{ $store->instagram }}"
                                                                 target="_blank"><i class="fa fa-skype"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($store->youtube))
                                        <li class="google"><a class="_blank"
                                                                 href="{{ $store->youtube }}"
                                                                 target="_blank"><i class="fa fa-youtube"></i></a>
                                        </li>
                                    @endif

                                </ul>
                            </div>
                        </td>

                    </tr>
                    </tbody>
                </table>
                <div class="text-right"></div>
            </div>
            <div class="userBlock">
                <div class="backgrounImg">
                    <img src="{{ asset($store->user->bg) }}">
                </div>
                <div class="userImg">
                    <img src="{{asset($store->user->image) }}">
                </div>
                <div class="userDescription">
                    <h5>
                        <a href="{{url($local.'/eg/profile/'. $store->user->id .'/'. str_replace(' ', '_', $store->user->name)) }}">{{ $store->user->name }}</a>
                    </h5>
                    <p>{{ $store->user->mobile }}</p>
                    <div class="followrs">
                        <span class="number number{{ $store->user->id }}">{{  $store->user->no_follows  }}</span>
                        <span> {{ trans('front.follower') }}</span>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->isUseFollow($store->user->id))
                            <button class="btn btn-danger"
                                    onclick="follow.add('{{  $store->user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $store->user->id }}">{{ trans('front.unfollow') }}
                            </button>
                        @else
                            <button class="btn btn-primary"
                                    onclick="follow.add('{{  $store->user->id }}','{{ url($local.'/users/follow') }}');"
                                    id="message{{ $store->user->id }}">{{ trans('front.follow') }}
                            </button>
                        @endif
                    @else
                        <button class="btn btn-primary" data-toggle="modal"
                                data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                    @endif

                </div>

            </div>
        </div>
    </aside>
    <div class="col-sm-8 col-sm-12">
        @yield('profile_content')
    </div>
</div>
@endsection
