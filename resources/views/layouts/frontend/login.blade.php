<!DOCTYPE html>
<html lang="en-gb" class="uk-height-1-1">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ريسيبشن</title>
{!! Html::style('assets/css/common/bootstrap.min.css') !!}
{!! Html::style('assets/css/common/font-awesome.min.css') !!}



{!! Html::style('assets/css/admin/jquery-ui.custom.min.css') !!}
{!! Html::style('assets/css/admin/chosen.min.css') !!}
{!! Html::style('assets/css/admin/bootstrap-datepicker3.min.css') !!}
{!! Html::style('assets/css/admin/bootstrap-timepicker.min.css') !!}
{!! Html::style('assets/css/admin/daterangepicker.min.css') !!}
{!! Html::style('assets/css/admin/bootstrap-datetimepicker.min.css') !!}
{!! Html::style('assets/css/admin/bootstrap-colorpicker.min.css') !!}


{!! Html::style('assets/css/common/sweetalert.css') !!}

{!! Html::style('assets/css/admin/fonts.googleapis.com.css') !!}
<!-- ace styles -->
    {!! Html::style('assets/css/admin/ace.min.css') !!}

{!! Html::style('assets/css/admin/ace-skins.min.css') !!}


<!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css"/>
    <![endif]-->


    <link rel="stylesheet" href="https://unpkg.com/flatpickr/dist/flatpickr.min.css">

    {!! Html::style('assets/css/admin/ace-rtl.min.css') !!}


    {!! Html::style('assets/css/admin/main.css') !!}
    {!! Html::style('assets/css/admin/media.css') !!}
    @yield('styles')
    @yield('extra-scripts')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    {!! Html::style('assets/css/frontend/main.css') !!}
    <style>
        input {
            text-align: center;
        }

        body {
            padding: 1px;
            background-repeat: no-repeat;
            background-image: url(../../../../assets/images/frontend/bg/login.jpg);
            background-size: cover;
            background-position: center;
        }

        .error-box {
            background-color: #FF0000;
            color: #FFF;
            margin-top: 30px;
            opacity: 0.5;
        }

        .error-box p {
            padding: 10px;
        }
        .overlay{
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(16, 16, 16, 0.53);
            z-index: -2;
            cursor: pointer;
        }
    </style>
</head>

<body class="uk-text-center">
<div class="overlay"></div>
@yield('content')
@include('includes.frontend.scripts')
<script>
    $(document).ready(function () {
        $('body').height($(window).height());
        $('.error-box').delay(3000).fadeOut(500);
    });
</script>

</body>
</html>

