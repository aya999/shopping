@extends('layouts.frontend.master')
@section('content')
@section('styles')
    <link id="color_scheme" href="{{ asset('assets/frontend/css/'.$local) }}/header/header4.css" rel="stylesheet">
@endsection
@section('all_categories')
    <div class="vertical-wrapper">
        <span id="remove-verticalmenu" class="fa fa-times"></span>
        <div class="megamenu-pattern">
            <div class="container-mega">
                <ul class="megamenu">
                    @foreach($cats as $i=>$cat)
                        <li class="item-vertical @if(count($cat->categories) > 0) with-sub-menu  @endif hover"
                            @if($i > 9) style="display: none" @endif>
                            <p class="close-menu"></p>
                            <a href="{{url($local.'/eg')}}/category_view/{{$cat->id}}/{{ str_replace(' ', '_', $cat->name) }}"
                               class="clearfix">
                                <img src="{{ asset('assets/images/categories/icons/'.$cat->icon) }}"
                                     alt="icon">
                                <span>{{ $cat->name }}</span>
                                <b class="caret"></b>
                            </a>
                            @if(count($cat->categories) > 0)
                                <div class="sub-menu" data-subwidth="60">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @foreach($cat->categories as $category)
                                                        <div class="col-md-4 static-menu">
                                                            <div class="menu">
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{url($local.'/eg')}}/category_view/{{$category->id}}/{{ str_replace(' ', '_', $category[$local.'_name'] ) }}"
                                                                           class="main-menu">{{ $category[$local.'_name'] }}</a>
                                                                        <ul>
                                                                            @foreach($category->categories as $child)
                                                                                <li>
                                                                                    <a href="{{url($local.'/eg')}}/category_view/{{ $child->id }}/{{ str_replace(' ', '_',$child[$local.'_name']) }}">{{ $child[$local.'_name'] }}</a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                    @if(count($cats) > 10)
                        <li class="loadmore">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="more-view">More Categories</span>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
<div class="row">
    <ul class="breadcrumb">
        <li><a href="{{url($local.'/eg/')}}">{{trans('front.home')}}</a></li>
        <li><a href="{{url($local.'/eg/')}}/category_view/{{$category->id}}">Profile</a></li>
    </ul>
    <!--Middle Part Start-->
    <div id="content" class="col-md-12 col-sm-12">
        <div class="profile">
            <div class="backgrounImg">
                <img src="{{ asset($user->bg) }}">
            </div>
            <div class="userImg">
                <img src="{{  $user->profileImage() }}">
            </div>
            <div class="userDescription">
                <h5>{{ $user->name }}</h5>
                <p>{{ $user->mobile }}</p>
                <div class="followrs">
                    <span class="number number{{ $user->id }}">{{  $user->no_follows  }}</span>
                    <span> {{ trans('front.follower') }}</span>
                </div>
                <div class="followrs">
                    <span class="followingsnumber number">{{  $user->no_followings  }}</span>
                    <span> {{ trans('front.following') }}</span>
                </div>
                @if(Auth::check())
                    @if(Auth::user()->isUseFollow($user->id))
                        <button class="btn btn-danger"
                                onclick="follow.add('{{  $user->id }}','{{ url($local.'/users/follow') }}');"
                                id="message{{ $user->id }}">{{ trans('front.unfollow') }}
                        </button>
                    @else
                        <button class="btn btn-primary"
                                onclick="follow.add('{{  $user->id }}','{{ url($local.'/users/follow') }}');"
                                id="message{{ $user->id }}">{{ trans('front.follow') }}
                        </button>
                    @endif
                @else
                    <button class="btn btn-primary" data-toggle="modal"
                            data-target="#LoginFirst">{{ trans('front.follow') }}</button>
                @endif

            </div>

        </div>
    </div>
    <aside class="col-sm-3 hidden-xs" id="column-right">
        <h2 class="subtitle">{{ trans('front.account') }}</h2>
        <div class="list-group">
            @include('includes.frontend.profile_sidebar')
        </div>
    </aside>
    <div class="col-md-9 col-sm-12">
        @yield('profile_content')
    </div>
</div>
@endsection