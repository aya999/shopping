@extends('layouts.admin.login')
@section('content')
    <div class="col-md-4">

    </div>
    <div class="col-md-4 text-center login-form">
        <h1 class="white ">{{ trans('front.login') }}</h1>
        {!! Form::open() !!}
        <div class="form-group">
            {!! Form::text('email', null, ['class' => 'form-control mb-15']) !!}
            {!! Form::password('password', ['class' => 'form-control mb-15']) !!}
        </div>
        <input class="btn btn-primary btn-block" type="submit" name="submit" value="{{ trans('front.login') }}">
        {!! Form::close() !!}
        @if(count($errors) > 0)
            <div class="error-box">
                <p>{{ trans('auth.failed') }}</p>
            </div>
        @endif
    </div>
@stop