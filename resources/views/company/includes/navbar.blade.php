<div id="navbar" class="navbar navbar-default  ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-right" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">Toggle sidebar</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>

		<div class="navbar-header  " >
			<a href="company/" class="navbar-brand">
				{{-- <img src="{{ asset('images/frontend/logo/company-logo.png') }}"> --}}
				 DaliliSouQ
			</a>
		</div>

		<div class="navbar-buttons navbar-header @if($local == "ar") pull-left @else  pull-right @endif" role="navigation">
			<ul class="nav ace-nav">
				<li class="light-blue dropdown-modal">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<span class="user-info">
						</span>
						{{ Auth::guard('company')->user()->name }}
						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu @if($local == "ar") dropdown-menu-left @else dropdown-menu-right  @endif dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
 							<a @if($local == "en") href="{{ url('/trans/ar/en') }}"  @else href="{{ url('/trans/en/ar') }}" @endif>
								<i class="ace-icon fa fa-globe"></i>
								{{ trans('dashboard.lang') }}
							</a>
						</li>

						<li>
							<a href="{{ url('/company/password/change') }}">
								<i class="ace-icon fa fa-lock"></i>
								{{ trans('dashboard.change_password') }}

							</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="{{ url('/company/logout') }}">
								<i class="ace-icon fa fa-sign-out"></i>
								{{ trans('dashboard.logout') }}

							</a>
						</li>

						{{--<li>
							<a href="{{ url('/company/send-message') }}">
								<i class="ace-icon fa fa-send"></i>
								ارسل للملاك
							</a>
						</li>--}}


						<li>

						</li>

					</ul>

				</li>

			</ul>

		</div>

	</div><!-- /.navbar-container -->

</div>