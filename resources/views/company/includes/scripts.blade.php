{!! Html::script('assets/admin/js/jquery.latest.min.js') !!}
{!! Html::script('assets/admin/js/moment.latest.min.js') !!}
@include('flashy::message')


{!! Html::script('assets/common/js/bootstrap.min.js') !!}

{!! Html::script('assets/admin/js/ace-extra.min.js') !!}

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN2d1fqFelylJlBQAD0a0r80FUPSXw7Aw&libraries=places&sensor=false"></script>

<script type="text/javascript">
    var map;
    var myMap =$('#map');


    var marker;
    var myLatlng = new google.maps.LatLng(myMap.data('lat'), myMap.data('long'));
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();

    function initialize() {
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        { "color": "#2196f3" },/*#b1c1ce*/
                        { "visibility": "on" }
                    ]
                },{
                    "featureType": "landscape",
                    "stylers": [
                        { "color": "#FFF" }
                    ]
                },{
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        { "color": "#D2D2D2" }/*#dae1e7*/
                    ]
                },{
                    "elementType": "labels",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "poi",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                },{
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        { "visibility": "on" },
                        { "hue": "#e9e5dc" }/*#0091ff*/
                    ]
                },{
                    "featureType": "road",
                    "elementType": "labels.text",
                    "stylers": [
                        { "visibility": "on" }
                    ]
                }
            ],
            title:"RK ANJEL"
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            title: "RK ANJEL"

//            draggable: true,
        });
        geocoder.geocode({'latLng': myLatlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#latitude,#longitude').show();
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });

        google.maps.event.addListener(marker, 'dragend', function () {

            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);

    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<span class="error-text" style="color: red;"></span>',
        classHandler: function (el) {
            return el.$element.closest('input');
        },
        successClass: 'valid',
        errorClass: 'invalid'
    };
</script>



<!-- page specific plugin scripts -->


{!! Html::script('assets/admin/js/jquery.hotkeys.index.min.js') !!}
<!-- ace scripts  -->

{!! Html::script('assets/admin/js/ace-elements.min.js') !!}
{!! Html::script('assets/admin/js/ace.min.js') !!}

{!! Html::script('assets/common/js/sweetalert.min.js') !!}
{!! Html::script('assets/common/js/ajax.js') !!}

{{--{!! Html::script('assets/js/admin/moment.latest.min.js') !!}--}}




{!! Html::script('assets/admin/js/bootstrap-colorpicker.min.js') !!}
{!! Html::script('assets/admin/js/bootstrap-tag.min.js') !!}

<script src="https://unpkg.com/flatpickr"></script>


<script>
    /*$('.timepicker-cstm').timepicki();*/
    $(".calendar").flatpickr();
    $(".flat-timepicker").flatpickr({
        enableTime: true,
        noCalendar: true,

        enableSeconds: false, // disabled by default

        time_24hr: false, // AM/PM time picker is used by default

        // default format
        dateFormat: "H:i",

        // initial values for time. don't use these to preload a date
        defaultHour: 12,
        defaultMinute: 0

        // Preload time with defaultDate instead:
        // defaultDate: "3:30"
    });



</script>

<script>
	URL = "{{ url('/') }}";
	token = "{{ csrf_token() }}"
</script>


{{--
{!! Html::script('assets/js/common/sweetalert.min.js') !!}
--}}

@yield('scripts')

<script>
    $(document).ready(function () {

        $(document).on('click', '.ajax-btn', function (event) {
            event.preventDefault();
            id = $(this).data('id');
            link = $(this).data('link');
            type = $(this).data('type');
            swal({
                    title: "{{ trans('dashboard.sure') }}",
                    text: "{{ trans('dashboard.sure_desc') }}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{ trans('dashboard.no') }}",

                    confirmButtonClass: "btn-danger",

                    confirmButtonText: "{{ trans('dashboard.yes') }}",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        url: link ,
                        type: 'POST',
                        data: {_token: token,_method: type ,id: id}
                    }).done(function (data){
                        swal({
                                title: "{{ trans('dashboard.done') }}",
                                text: "",
                                type: "success",
                            },
                            function () {
                                window.location.reload();

                            });
                    });
                });
        });


    });
    $("#city_id").change(function () {
        var city = $(this).val(); //selected_city
        var URL = "{{ url('/') }}";
        if (city != "") {
            $.ajax({
                url: URL + '/regions/' + city ,
                type: 'get',
            }).done(function (data) {
                var cities = data.data;
                var dataString = "";
                var i;
                for (i = 0; i < cities.length; i++) {
                    dataString += '<option value="' + cities[i].id + '">' + cities[i].name + '</option>';
                }
                $('#region_id').html(dataString);
            });
        }
    });
</script>

{{--{!! Html::script('assets/js/admin/main.js') !!}--}}





