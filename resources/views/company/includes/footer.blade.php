<div class="footer">
		<div class="footer-inner">
			<div class="footer-content rtl">
				<span class="bigger-120">
					<span class="blue bolder"> Dalili</span>
					Developed by <a href="http://www.smartstep-eg.com">Smart Step</a> &copy; {{ date('Y') }}
				</span>
			</div>
		</div>
	</div>

	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>

@include('includes.admin.scripts')

