<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('dashboard.dashboard') }} |  @yield('title')</title>


{!! Html::style('assets/common/css/bootstrap.min.css') !!}
{!! Html::style('assets/common/css/font-awesome.min.css') !!}



{!! Html::style('assets/admin/css/jquery-ui.custom.min.css') !!}
{!! Html::style('assets/admin/css/chosen.min.css') !!}
{!! Html::style('assets/admin/css/bootstrap-datepicker3.min.css') !!}
{!! Html::style('assets/admin/css/bootstrap-timepicker.min.css') !!}
{!! Html::style('assets/admin/css/daterangepicker.min.css') !!}
{!! Html::style('assets/admin/css/bootstrap-datetimepicker.min.css') !!}
{!! Html::style('assets/admin/css/bootstrap-colorpicker.min.css') !!}


{!! Html::style('assets/common/css/sweetalert.css') !!}

{!! Html::style('assets/admin/css/fonts.googleapis.com.css') !!}
<!-- ace styles -->
{!! Html::style('assets/admin/css/ace.min.css') !!}

{!! Html::style('assets/admin/css/ace-skins.min.css') !!}


<!--[if lte IE 9]>
<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
<![endif]-->


<link rel="stylesheet" href="https://unpkg.com/flatpickr/dist/flatpickr.min.css">



{!! Html::script('assets/admin/js/ckeditor/ckeditor.js') !!}
{!! Html::style('assets/admin/css/main.css') !!}
{!! Html::style('assets/admin/css/media.css') !!}


@yield('styles')
@if($local == "ar")
    {!! Html::style('assets/admin/css/ace-rtl.min.css') !!}
    <style>
        .has-float-label label{
            right: 17px !important;
            left: inherit !important;
        }
    </style>
@endif
@yield('extrascripts') 



    