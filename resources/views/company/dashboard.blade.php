@extends('company.layout.master')

@section('title', trans('dashboard.dashboard') )

@section('styles')
    {!! Html::style('assets/admin/css/dashboard-widgets.css') !!}
@stop

@section('content')
    {{-- {!! Breadcrumbs::render('dashboard') !!} --}}
    <div class="page-content">
        <div class="page-header">
            <h1 class="text-center">{{ trans('dashboard.dashboard') }}  </h1>
        </div>

        <div class="row">

            <div class="space-6"></div>
            <div class="">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget widget-call-to-action text-canter well-primary">
                            <div class="row">
                                <div class="widget-body">
                                    <div class="col-xs-4">
                                        <i class="icon fa fa-users"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <span class="number">{{ \App\User::count()  }}</span>
                                        <h3 class="widget-title">{{ trans('dashboard.client_number') }}   </h3>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 ">
                                    <a href="{{ url('admin/users') }}" class="btn btn-white btn-block">
                                        <span class="pull-left">  {{ trans('dashboard.show_all') }}  </span>
                                        <i class="fa fa-arrow-right pull-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>






                </div>

            </div>
            <div class="col-xs-8 col-md-offset-2">

            </div>
        </div>
    </div>
@stop

