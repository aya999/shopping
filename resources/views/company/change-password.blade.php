@extends('layouts.admin.admin-master')
@section('title','Change Password')
@section('styles')
    {!! Html::style('assets/css/admin/form.css') !!}

@stop
@section('content')
    <div class="page-content">
        <div class="page-header">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">تعديل كلمة المرور</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        {!! Form::open(['route' => 'password.change.post', 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="form-group col-md-12 has-float-label input-group">
                                {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => 'كلمة المرور الحالية']) !!}
                                <label for="old_password">كلمة المرور الحالية <span class="astric">*</span></label>
                                <small class="text-danger">{{ $errors->first('old_password') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 has-float-label input-group">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'كلمة المرور الجديدة']) !!}
                                <label for="password">كلمة المرور الجديدة <span class="astric">*</span></label>
                                <small class="text-danger">{{ $errors->first('password') }}</small>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-12 has-float-label input-group">
                                {!! Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => ' اعد كتابة كلمة المرور']) !!}
                                <label for="confirm_password"> اعد كتابة كلمة المرور<span class="astric">*</span></label>
                                <small class="text-danger">{{ $errors->first('confirm_password') }}</small>    </div>
                        </div>

                        {!! Form::submit("تعديل", ['class' => 'btn btn-primary btn-xs btn-block' ]) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
