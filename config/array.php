<?php
return [
    'icons' => [
        '1' => 'assets/images/icons/Bathroom.png',
        '2' => 'assets/images/icons/Bed.png',
        '3' => 'assets/images/icons/call.png',
        '4' => 'assets/images/icons/Babeque.png',
        '6' => 'assets/images/icons/Bride.png',
        '7' => 'assets/images/icons/Celeberation.png',
        '8' => 'assets/images/icons/Children.png',
        '9' => 'assets/images/icons/Coffeeshop.png',
        '10' => 'assets/images/icons/Conditioner.png',
        '11' => 'assets/images/icons/Dinning.png',
        '12' => 'assets/images/icons/WC.png',
        '13' => 'assets/images/icons/Work.png',
        '14' => 'assets/images/icons/Waterfall.png',
        '15' => 'assets/images/icons/Sportswear.png',
        '16' => 'assets/images/icons/Store.png',
        '17' => 'assets/images/icons/Shower.png',
        '18' => 'assets/images/icons/Shopping.png',
        '19' => 'assets/images/icons/Security.png',
        '20' => 'assets/images/icons/Meeting.png',
        '21' => 'assets/images/icons/Kitchen.png',
        '23' => 'assets/images/icons/Grass.png',
        '24' => 'assets/images/icons/Games.png',
        '25' => 'assets/images/icons/Football.png',
        '26' => 'assets/images/icons/Family.png',
    ],
    'cancel_policy' => [
        '0' => 'مرن',
        '1' => 'غير متاح'
    ],
    'fa_icons' => [
        'users' => 'Users',
        'code' => 'Code',
        'calendar' => 'calendar',
        'heart' => 'Heart',
        'money' => 'Money',
        'mobile' => 'Mobile',
        'car' => 'Car',
        'truck' => 'Truck',
        'star' => 'Star',
        'leaf' => 'leaf',
        'desktop' => 'desktop',
        'thumbs-up' => 'Like',
        'thumbs-down' => 'Dislike',
        'hand-o-left' => 'Hand',
        'rocket' => 'Roket',
        'times' => 'Delete',
        'tag' => 'Tag',
        'cogs' => 'Cogs',
        'comments' => 'Comments',
        'globe' => 'Globe',
        'bell' => 'bell',
        'camera' => 'camera',
        'check-circle' => 'check',
        'bookmark-o' => 'bookmark',
        'university' => 'Bank',
        'book' => 'Book',
        'briefcase' => 'Bag',
        'key' => 'Key',
        'magic' => 'Magic stick',
        'location-arrow' => 'arrow',
        'road' => 'Road',
    ]
    ,
    'price_per' => [
        3 => 'شهريا',
        2 => 'اسبوعيا',
        1 => 'يوميا',
    ],
    'request_status' => [
        '0' => 'قيد الانتظار',
        '1' => 'تمت الموافقه',
        '2' => 'تم الرفض',
    ],
    'ar_is_active' =>
        [
            '1' => 'مفعل',
            '0' => 'غير مفعل'
        ],
    'en_is_active' =>
        [
            '1' => 'مفعل',
            '0' => 'غير مفعل'
        ]
    ,
    'ar_gender' =>
        [
            '1' => 'ذكر',
            '0' => 'انثى'
        ],
    'en_gender' =>
        [
            '1' => 'Male',
            '0' => 'Female'
        ]


];
