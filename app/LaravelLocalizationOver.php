<?php
namespace App;
use function Aws\describe_type;
use  Mcamara\LaravelLocalization\LaravelLocalization;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Cookie ;
Class LaravelLocalizationOver extends LaravelLocalization {
    public function setLocale($locale = null)
    {
        if($this->request->segment(2) == 'packages') {
            #dd($this->request->segment(2));
        }
        if (empty($locale) || !is_string($locale)) {
            // If the locale has not been passed through the function
            // it tries to get it from the first segment of the url
            $locale = $this->request->segment(1);
        }
        $theLocale = false;
        if( strlen($locale) == 5 && strpos($locale, '-') !== false ) {
            $theLocale = $locale;
            $country = strtolower(substr($locale, 3));
            $country = isset($_COOKIE['nationality']) ? $_COOKIE['nationality'] : $country;
            #$country = \Cookie::get('nationality') ? \Cookie::get('nationality') : $country;
            $locale = substr($locale, 0, 2);
            $this->currentLocale = $locale;
            @setcookie('nationality', $country, 0, "/");
/*            Cookie::queue("nationality",$country, '43200'); // one month*/

           /* if(empty(Cookie::get('currency'))){
                $currency = $common->getCurrencyByNationality($country);
                setcookie('currency', $currency, 0, "/");
                Cookie::queue("currency",$currency, '43200'); // one month
            }*/
            config(['app.localisation' => $country]);
        }
        elseif (strlen($locale) == 2){
            $theLocale = $locale;
            $locale = substr($locale, 0, 2);
            \App::setLocale($theLocale);
        }
        if (!empty($this->supportedLocales[$locale])) {
            $this->currentLocale = $locale;
        } else {
            // if the first segment/locale passed is not valid
            // the system would ask which locale have to take
            // it could be taken by the browser
            // depending on your configuration

            $locale = null;

            // if we reached this point and hideDefaultLocaleInURL is true
            // we have to assume we are routing to a defaultLocale route.
            if ($this->hideDefaultLocaleInURL()) {
                $this->currentLocale = $this->defaultLocale;
            }
            // but if hideDefaultLocaleInURL is false, we have
            // to retrieve it from the browser...
            else {
                $this->currentLocale = $this->getCurrentLocale();
            }
        }

        if ($this->currentLocale && !isset($this->supportedLocales[$this->currentLocale])) {
            abort(404);
        }

        $this->app->setLocale($this->currentLocale);
        // Regional locale such as de_DE, so formatLocalized works in Carbon
        $regional = $this->getCurrentLocaleRegional();
        if ($regional) {
            setlocale(LC_TIME, $regional.'.UTF-8');
            setlocale(LC_MONETARY, $regional.'.UTF-8');
        }
        if(!$theLocale) {
            \App::setLocale($this->defaultLocale);
        }
        return $theLocale ? $theLocale : '';#$this->defaultLocale.'-'.config('app.localisation');

    }

}