<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Settings;
use App\Models\StoreCategory;
use App\Models\WebsitePage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Super;
use View;
use Request;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $local = Request::segment(1);
        config([
            'services.facebook.redirect' => url($local.'/login/facebook/callback'),
            'services.google.redirect' => url($local.'/login/google/callback'),
        ]);
         Schema::defaultStringLength(191);
        View::composer(['frontend/*', 'auth/*'], function ($view) {
            $localization = Request::segment(1);
            $local = substr($localization, 0, 2);
            $country_code= substr($localization, 3, 4);
        /*    $url_array = explode('.', parse_url(Request::url(), PHP_URL_HOST));
            $country_code = $url_array[0];*/
            #$country_code = Request::segment(2);
            $country = Country::where('code', $country_code)->first();

            if (!$country) {
                abort(402);
                die();
            }

            $main = Settings::first();
            $online_cats = Category::select($local . '_name as name', 'id', 'image', 'icon')->where('parent_id', null)->where('is_online', 1)->get();
            $stores_cats_ids = StoreCategory::all()->pluck('category_id');
            $stores_cats = Category::select($local . '_name as name', 'id', 'image', 'icon')->where('parent_id', null)->whereIn('id', $stores_cats_ids)->get();
            if (!$main) {
                abort(404);
            }
            $cats = Category::where('parent_id', null)->select($local . '_name As name', 'id', 'icon', 'image')->get();
            $cities = City::where('country_id', $country->id)->select($local . '_name As name', 'id')->get();
            $footer_pages = WebsitePage::where('is_footer',1)->select('id',$local.'_name as name')->get();

            if(Auth::user()){
                $cart_cont = count(Cart::where('user_id',Auth::user()->id)->get());
            }else{
                $cart_cont=count($_SESSION['cart']);
            }
            $view->with('main', $main);
            $view->with('online_cats', $online_cats);
            $view->with('stores_cats', $stores_cats);
            $view->with('local', $local);
            $view->with('localization', $localization);
            $view->with('country_code', $country_code);
            $view->with('footer_pages', $footer_pages);
            $view->with('cats', $cats);
            $view->with('cities', $cities);
            $view->with('cart_cont', $cart_cont);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Hesto\MultiAuth\MultiAuthServiceProvider');
        }
    }
}
