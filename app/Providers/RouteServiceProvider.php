<?php

namespace App\Providers;

use App\LaravelLocalizationOver;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use LaravelLocalization;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapCompanyRoutes();
        $this->mapAdminRoutes();
        $this->mapWebRoutes();


        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $loclaization = new LaravelLocalizationOver();
        Route::middleware('web',/*'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'*/
            'localizationPackagesRedirect', 'localeViewPath')
            ->namespace($this->namespace)->prefix($loclaization->setLocale())
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::group(['middleware' => ['web', 'admin', 'auth:admin', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
            'prefix' => LaravelLocalization::setLocale() . '/admin',
            'as' => 'admin.',
            'namespace' => $this->namespace . '\Admin'], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    protected function mapCompanyRoutes()
    {
        Route::group(['middleware' => ['web', 'company', 'auth:company', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
            'prefix' => LaravelLocalization::setLocale() . '/company',
            'as' => 'company.',
            'namespace' => $this->namespace . '\Company'], function ($router) {
            require base_path('routes/company.php');
        });
    }
}
