<?php

namespace App\Repositories;
use App\Traits\MalathTrait;
use App\Traits\QueryTrait;
use App\Traits\SuperTrait;
use App\Traits\UserTrait;
use App\Traits\NotificationTrait;
use App\Traits\FileTrait;
use App\Traits\NexmoTrait;
use App\Traits\DateTrait;

class SuperRepository
{
    use SuperTrait, FileTrait, NotificationTrait, NexmoTrait, DateTrait, MalathTrait;
}