<?php

namespace App\Traits;

trait NexmoTrait
{
    public function sendNexmoSMS($number, $message){
        $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
                'api_key' => 'd2d102d3',
                'api_secret' => '58a6cc9bbee97b56',
                'to' => $number,
                'from' => '201003939110',
                'type'     => 'unicode',
                'text' => $message
            ]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        return $response;
    }

    public function sendNexmoVerification($number, $brand){
        $url = 'https://api.nexmo.com/verify/json?' . http_build_query([
                'api_key' => 'd2d102d3',
                'api_secret' => '58a6cc9bbee97b56',
                'type'     => 'unicode',
                'number' => $number,
                'brand' => $brand,
            ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        return $response;
    }

    public function nexmoVerifyCode($request_id, $code){
        $url = 'https://api.nexmo.com/verify/check/json?' . http_build_query([
                'api_key' => 'd2d102d3',
                'type'     => 'unicode',
                'api_secret' => '58a6cc9bbee97b56',
                'request_id' => $request_id,
                'code' => $code
            ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        return $response;
    }

    public function controlVerificationProccess($request_id, $cmd, $country_code, $number){
        $url = 'https://api.nexmo.com/verify/control/json?' . http_build_query([
                'api_key' => 'd2d102d3',
                'type'     => 'unicode',
                'api_secret' => '58a6cc9bbee97b56',
                'request_id' => $request_id,
                'cmd' => $cmd,
                'number' => $country_code . $number
            ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        return $response;
    }
}