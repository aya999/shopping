<?php

namespace App\Traits;

use App\Mail\SuperMailer;
use App\Models\Notification;
use Image;
use File;
use Mail;
use App\Models\User;
use App\Models\Log;
use Nexmo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait SuperTrait
{

    public function generatePassword($size = 8)
    {
        $p = openssl_random_pseudo_bytes(ceil($size * 0.67), $crypto_strong);
        $p = str_replace('=', '', base64_encode($p));
        $p = strtr($p, '+/', '^*');
        return substr($p, 0, $size);
    }
    public function generateOrderId($size)
    {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
            .'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, $size) as $k) $rand .= $seed[$k];
        return time().$rand;
    }
    public function createLog($action,$object,$oject_type,$user_type)
    {

        $log=new Log();
        $log->action=$action;
        $log->object_id=$object;
        $log->object_type=$oject_type;
        $log->user_id=Auth::user()->id;
        $log->user_type=$user_type;
        $log->save();

    }
    public function randomPin()
    {
        return rand(1111, 9999);
    }

    public function shortenText($string, $wordsreturned)
    {
        $string = strip_tags($string);
        $retval = $string;
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
        $string = str_replace("\n", " ", $string);
        $array = explode(" ", $string);
        if (count($array) <= $wordsreturned) {
            $retval = $string;
        } else {
            array_splice($array, $wordsreturned);
            $retval = implode(" ", $array) . " ...";
        }
        return $retval;
    }

    function trim_text($input, $length, $ellipses = true, $strip_html = true)
    {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }


    public function validate($rules, $request)
    {
        if (is_array($rules)) {
            foreach ($rules as $rule) {
                if (!$request[$rule]) {
                    return $this->jsonResponse(true, 0, [], $rule . ' is required', []);
                }
            }
        }

    }

    public function checkPermission($permission){
        if(Auth::user()->hasPermission($permission)){
            return true;
        }else{
            return redirect()->to('/admin/ddd');
        }

    }

    public function jsonResponse($status,$error_code,$validation,$message,$response,$token=NULL){
        if(is_array($response)){
            if(count($response) == 0){
                $response = new \stdClass();
            }
        }
        return response()->json([
            'Error' => [
                'status' => $status,
                'token'=>$token,
                'code' => $error_code,
                'validation'=>$validation,
                'desc' =>$message,

            ],
            'Response' => $response,
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    static  public function Location($ip){

        $apiKey  ='e45458c799b946e090289f879d6093af';
        $lang = "en";
        $fields = "*";
        $excludes = "";
        //$ip = "197.50.105.247";

        $url = "https://api.ipgeolocation.io/ipgeo?apiKey=".$apiKey."&ip=".$ip."&lang=".$lang."&fields=".$fields."&excludes=".$excludes;
        $cURL = curl_init();

        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));
        return json_decode(curl_exec($cURL));
    }

}