<?php

namespace App;


use App\Models\Notification;

class Help
{
    static public function SendNotification($user_id, $receivers,$en_action,$ar_action,$object_type,$object_id)
    {
              foreach ($receivers as $receiver){
                 $o = new Notification();
                $o->sender_id = $user_id;
                $o->receiver_id = $receiver;
                $o->sender_type = "users";
                $o->receiver_type = "users";
                $o->ar_action = $ar_action;
                $o->en_action = $en_action;
                $o->object_type = $object_type;
                $o->object_id = $object_id;
                $o->save();
            }

         return true;

    }

}