<?php

namespace App\Http\Middleware;

use App\Models\Api;
use Closure;
use Auth;
use Request;
use Super;

class ActiveApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->os)) {
            $uri = $request->route()->uri;
            $sub = substr($uri, 3);
            $is_exist = Api::where('path', $sub)->get()->first();
            if($is_exist){
                if($is_exist->and_is_active == 1) {
                    return $next($request);
                }else{
                    return Super::jsonResponse(false, 501 , [], 'Dis active Service', $is_exist);
                }
            }
            else{
                return $next($request);
            }
        }
        return Super::jsonResponse(false, 503 , [], 'Bad Request', new \stdClass());

    }
}
