<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Request;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (Auth::user()->hasPermission($permission))
            return $next($request);

        else {
            $local = Request::segment(1);
            return redirect($local . '/admin/401');

        }
    }
}