<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationMiddlewareBase;
use Cookie;

class LaravelLocalizationRedirectFilter extends LaravelLocalizationMiddlewareBase
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $url_delemeters = explode('/', $request->url());
        $old = isset($_COOKIE['nationality']) ? $_COOKIE['nationality'] : strtolower(config('app.localisation'));
        if (!isset($url_delemeters[3])) { 
            $url_delemeters[3] = app()->getLocale() . '-' . $old;
            $url = implode('/', $url_delemeters);
            return redirect()->to($url);
        }
        if (isset($url_delemeters[3]) && strlen($url_delemeters[3]) == 2) {
            $url_delemeters[3] = $url_delemeters[3] . '-' . $old;
            $url = implode('/', $url_delemeters);
            return redirect()->to($url);
        } else {
            return $next($request);
        }
        return $next($request);
    }
}