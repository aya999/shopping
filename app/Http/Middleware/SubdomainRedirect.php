<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
class SubdomainRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        $subdomain = $url_array[0];

        $num_of_minutes_until_expire = 60 * 24 * 7; // one week
        Cookie::queue('kk', $subdomain, $num_of_minutes_until_expire, null, '.'.$request->domain);

        #Cookie::make("sub",$subdomain, '43200','d'); // one month
        #setcookie('sub', $subdomain, 0,'/',$request->domain);
        return $next($request);
    }
}