<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PackagetypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'days' => 'required',
                'package_id' => 'package_id',


            ];
        } else if ($this->method() == 'PATCH') {
            return [
                'days' => 'days',
                'package_id' => 'package_id',

            ];
        }

    }
}
