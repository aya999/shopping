<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'name' => 'required',
                 'email' => 'required|unique:admins',
                 'mobile' => 'required|unique:admins',
                'role_id'=>'required',
                'password' => 'required|confirmed',


            ];
        } else if ($this->method() == 'PATCH') {
            return [
                'name' => 'required',
                'role_id'=>'required',
                'email' => 'required|unique:admins,id,' . $this->route('admins'),
                'mobile' => 'required|unique:admins,id,' . $this->route('admins'),
                'password' => 'confirmed',
            ];
        }
        else{
            return [];
        }

    }
}
