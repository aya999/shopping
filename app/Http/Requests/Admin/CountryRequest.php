<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'ar_name' => 'required',
                'en_name' => 'required',
                'currency_symbol' => 'required',
                'code' => 'required|max:3|unique:countries',
                'currency_code' => 'required|max:3|unique:countries',
             ];
        } else if ($this->method() == 'PATCH') {
            return [
                'ar_name' => 'required',
                'en_name' => 'required',
                'currency_symbol' => 'required',
                'currency_code' => 'required|max:3|unique:countries,id,' . $this->route('category'),
                'code' => 'required|max:3|unique:countries,id,' . $this->route('category'),

            ];
        }

    }
}
