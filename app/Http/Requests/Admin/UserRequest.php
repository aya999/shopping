<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'name' => 'required',
                'mobile' => 'required',
                'country_id' => 'required|exists:countries,id',
                'email' => 'required|unique:users',
                'password' => 'required|confirmed',
                'image' => 'mimes:png,jpg,jpeg'

            ];
        } else if ($this->method() == 'PATCH') {
            return [
                'name' => 'required',
                'password' => 'confirmed',
                'mobile' => 'required',
                'country_id' => 'required|exists:countries,id',
                'email' => 'required|unique:users,id,' . $this->route('users'),
                'image' => 'mimes:png,jpg,jpeg'
            ];
        }
        else{
            return [];
        }

    }
}
