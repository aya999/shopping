<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'ar_name' => 'required',
                'en_name' => 'required',
                'points' => 'required',

             ];
        } else if ($this->method() == 'PATCH') {
            return [
                'ar_name' => 'required',
                'en_name' => 'required',
                'points' => 'required',
             ];
        }

    }
}
