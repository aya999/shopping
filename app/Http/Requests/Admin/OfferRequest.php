<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'image' => 'required|image',
                'ar_name' => 'required',
                'en_name' => 'required',
                'category_id' => 'required',
                'user_id' => 'required',
                'city_id' => 'required',

            ];
        } else if ($this->method() == 'PATCH') {
            return [
                'image' => 'image',
                'ar_name' => 'required',
                'en_name' => 'required',
                'category_id' => 'required',
                'user_id' => 'required',
                'city_id' => 'required',

            ];
        }

    }
}
