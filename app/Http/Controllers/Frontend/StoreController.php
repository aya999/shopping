<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreCategory;
use App\Models\StoreFollow;
use App\Models\WebsitePage;
use App\User;


class StoreController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function categoryStore ($id,$name){
        $category = Category::find($id);
        if (!$category) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $category[$this->local . '_meta_title'];
        $meta->meta_desc = $category[$this->local . '_meta_desc'];
        $meta->meta_keywords = $category[$this->local . '_meta_keywords'];
        if ($name != str_replace(' ', '_', $category[$this->local . '_name'])) {
            return redirect()->to($this->local . '/' . '/stores/' . $category->id . '/' . str_replace(' ', '_', $category[$this->local . '_name']));
        }

        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon')->get();
        $sub_categories = Category::where('parent_id', $category->id)->pluck('id');
        $sub_sub_categories = Category::whereIn('parent_id', $sub_categories)->pluck('id');
        $stores_ids = StoreCategory::whereIn('category_id', $category)->orWhereIn('category_id', $sub_categories)
            ->orWhereIn('category_id', $sub_sub_categories)->get()->pluck('store_id');
        $stores=Store::whereIn('id',$stores_ids)->select('id',$this->local.'_name As name','email','no_follows','image','mobile','address','user_id','city_id')->where('is_active', '1')->paginate(10);

        return view("frontend.stores.stores", compact('category', 'cities', 'cats', 'pages','stores'))->render();


    }

    public function store($id,$name){
        $store = Store::find($id);
        if (!$store) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $store[$this->local . '_meta_title'];
        $meta->meta_desc = $store[$this->local . '_meta_desc'];
        $meta->meta_keywords = $store[$this->local . '_meta_keywords'];
        if ($name != str_replace(' ', '-', $store[$this->local . '_name'])) {
            return redirect()->to($this->local . '/store/' . $store->id . '/' . str_replace(' ', '-', $store[$this->local . '_name']));
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon')->get();
       $products = Product::where('store_id',$id)->where('is_active','1')->paginate(10);
        return view("frontend.stores.store", compact('store','pages','cats','products'))->render();
    }


    public function followers($id,$name){
        $store = Store::find($id);
        if (!$store) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $store[$this->local . '_meta_title'];
        $meta->meta_desc = $store[$this->local . '_meta_desc'];
        $meta->meta_keywords = $store[$this->local . '_meta_keywords'];
        if ($name != str_replace(' ', '-', $store[$this->local . '_name'])) {
            return redirect()->to($this->local . '/store/' . $store->id . '/' . str_replace(' ', '-', $store[$this->local . '_name']).'/followers');
        }
        $ids = StoreFollow::where('store_id',$id)->get()->pluck('follower_id');
        $users =User::whereIn('id',$ids)->select('id', 'name', 'email', 'mobile', 'image','bg')->paginate(10);

        return view("frontend.stores.followers", compact('store','pages','cats','users'))->render();



    }
}