<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductLike;
use App\Models\Store;
use App\Models\UserFollow;
use App\User;
use Illuminate\Http\Request;
use App\Models\WebsitePage;

use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function profile( $id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
     /*   if ($name != str_replace(' ', '_', $user[$this->local . '_name'])) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }*/
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $stores = Store::where('user_id', $user->id)->get();
        $products = Product::select('id', $this->local . '_name', 'image','price','user_id')->where('user_id', $user->id)->where('is_active', 1)->paginate(10);
        $num_follower = UserFollow::where('following_id', $user->id)->get()->count();
        $num_following = UserFollow::where('follower_id', $user->id)->get()->count();
        $countries=Country::select('id', $this->local . '_name')->get();
        return view('frontend.user.profile', compact('user', 'products', 'stores', 'num_follower', 'num_following','pages','countries','cities'));
    }

    public function profileUpdate(Request $data,$id)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }

        $user->update($data->all());
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $stores = Store::where('user_id', $user->id)->get();
        $products = Product::select('id', $this->local . '_name', 'image','price','user_id')->where('user_id', $user->id)->where('is_active', 1)->paginate(10);
        $num_follower = UserFollow::where('following_id', $user->id)->get()->count();
        $num_following = UserFollow::where('follower_id', $user->id)->get()->count();
        $countries=Country::select('id', $this->local . '_name')->get();
        $cities=City::select('id', $this->local . '_name')->get();
        return view('frontend.user.profile', compact('user', 'products', 'stores', 'num_follower', 'num_following','pages','countries','cities'));
    }

    public function products($id, $name){
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        /*   if ($name != str_replace(' ', '_', $user[$this->local . '_name'])) {
               return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
           }*/
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $stores = Store::where('user_id', $user->id)->get();
        $products = Product::select('id', $this->local . '_name', 'image','price','user_id')->where('user_id', $user->id)->where('is_active', 1)->paginate(10);
        $num_follower = UserFollow::where('following_id', $user->id)->get()->count();
        $num_following = UserFollow::where('follower_id', $user->id)->get()->count();
        return view('frontend.user.products', compact('user', 'products', 'stores', 'num_follower', 'num_following','pages'));

    }

    public function likes( $id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if ($name != str_replace(' ', '_', $user->name)) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
        $product_likes = ProductLike::where('user_id', $user->id)->get()->pluck('product_id');
         $products = Product::select('id', $this->local . '_name As name', 'image','price','is_avail','category_id')->whereIn('id', $product_likes)->paginate(10);
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        return view('frontend.user.likes', compact('user', 'products','pages'));

    }

    public function followers($id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if ($name != str_replace(' ', '_', $user['name'])) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();

        $followers_ids = UserFollow::where('following_id', $user->id)->where('is_follow', 1)->pluck('follower_id');
        $followers=User::whereIn('id',$followers_ids)->paginate(10);
        return view('frontend.user.followers', compact('user', 'followers','pages'));
    }

    public function following( $id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if ($name != str_replace(' ', '_', $user['name'])) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();

        $followers_ids = UserFollow::where('follower_id', $user->id)->where('is_follow', 1)->pluck('following_id');
        $followers=User::whereIn('id',$followers_ids)->paginate(10);
        return view('frontend.user.following', compact('user', 'followers','pages'));
    }

    public function blocked($country, $id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if ($name != str_replace(' ', '_', $user['name'])) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
        $block_list = UserFollow::where('follower_id', $user->id)->where('is_blocked', 1)->paginate(10);
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();

        return view('frontend.user.blocked', compact('user', 'block_list','pages'));
    }


    public function follow(Request $request)
    {
        if (!Auth::check()) {
            return response()->json(['msg' => trans('remove_from_wishlist'), 'span' => "fa fa-heart"], 200);
        }
        $user=User::find($request['user_id']);
        if(!$user){
            return response()->json(['msg' => trans('remove_from_wishlist'), 'span' => "fa fa-heart"], 200);

        }
        $follow = UserFollow::where('follower_id', Auth::user()->id)
            ->where('following_id', $request['user_id'])->where('is_follow', 1)->first();
        if ($follow) {
            if ($follow->rate == 0 && $follow->is_blocked == 0) {
                $follow->delete();
            } else {
                $follow->is_follow = 0;
                $follow->save();
            }
            $user->no_follows -=1;
            $user->save();
            return response()->json(['msg' => trans('front.follow'),'num'=>'-1', 'class' => "btn btn-primary"], 200);
        } else {
            $row = UserFollow::where('follower_id', Auth::user()->id)
                ->where('following_id', $request['user_id'])->first();
            if (!$row) {
                $n = new UserFollow();
                $n->follower_id = Auth::user()->id;
                $n->following_id = $request['user_id'];
                $n->is_follow = 1;
                $n->save();
            } else {
                $follow->is_follow = 1;
                $follow->save();
            }
            $user->no_follows +=1;
            $user->save();
            return response()->json(['msg' => trans('front.unfollow'),'num'=>'1', 'class' => "btn btn-danger"], 200);
        }
    }


}