<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductLike;
use App\Models\ProductRate;
use App\Models\Settings;
use App\Models\Slider;
use Auth;
use Illuminate\Http\Request;
use Validator;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function newArrivals()
    {
        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id','category_id')->orderBy('created_at', 'desc')->take(6)->get();
        return view("frontend.ajax.product_box3", compact('products'))->render();
    }

    public function featuredProducts()
    {
        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id','category_id')->orderBy('created_at', 'desc')->take(6)->get();
        return view("frontend.ajax.product_box3", compact('products'))->render();
    }

    public function popularProducts()
    {
        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id','category_id')->orderBy('viewers', 'desc')->take(6)->get();
        return view("frontend.ajax.product_box3", compact('products'))->render();

    }

    public function bestSellers()
    {
        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id')->orderBy('created_at', 'desc')->take(6)->get();
        return view("frontend.ajax.product_box3", compact('products'))->render();
    }

    public function relatedProducts($id)
    {
        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id')->orderBy('created_at', 'desc')->take(5)->get();
        return view("frontend.ajax.product_box3", compact('products'))->render();
    }


    public function CategoryTab($cat_id)
    {

        $products = Product::select($this->local . '_name As name', 'image', 'price', 'id')->where('category_id', $cat_id)->orderBy('viewers', 'desc')->take(5)->get();
        return view("frontend.ajax.category_tab", compact('products'))->render();
    }


    public function productDetails( $id)
    {
        $product = Product::select($this->local . '_name as name', 'price', 'id', 'image', 'category_id', 'user_id')->find($id);
        return view("frontend.ajax.quick_view", compact('product'))->render();
    }


    public function productLike(Request $request)
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $check = ProductLike::where('user_id', '=', $user_id)->where('product_id', '=', $request['id'])->first();
            if (count($check) >= 1) {
                $check->delete();
                return response()->json(['msg' => trans('front.add_to_wishlist'), 'desc' => trans('front.product_remove_wishlist_desc'), 'span' => "fa fa-heart "], 200);
            } else {
                $item = new ProductLike();
                $item->user_id = $user_id;
                $item->product_id = $request['id'];
                $item->save();
                return response()->json(['msg' => trans('front.remove_from_wishlist'), 'desc' => trans('front.product_added_wishlist_desc'), 'span' => "fa fa-heart like"], 200);

            }
        }
    }

    public function productRate(Request $data)
    {
        $validator = Validator::make($data->all(), [
            'rate' => 'required',
            'comment' => 'required',
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors(), 'status' => false], 400);
        }

        if (Auth::check()) {
            $old = ProductRate::where('product_id', $data['product_id'])->where('user_id', Auth::user()->id)->get()->first();
            try {
                if (empty($old)) {
                    $review = new ProductRate();
                    $review->rate = $data['rate'];
                    $review->comment = $data['comment'];
                    $review->product_id = $data['product_id'];
                    $review->user_id = Auth::user()->id;
                    $review->save();
                } else {
                    $old->rate = $data['rate'];
                    $old->save();
                }
                return response()->json(['message' => "Thanks", 'status' => true], 200);

            } catch (Exception $e) {
                return error;
            }
        } else {

        }
    }


    public function userFollowers($user_id)
    {
        $followers_ids = UserFollow::where('following_id', $user_id)->where('is_follow', 1)->pluck('follower_id');
        $followers = User::whereIn('id', $followers_ids)->paginate(10);
        return view("frontend.user.ajax.followers", compact('followers'))->render();

    }


    //Add To Cart--------------------------------------------------------------------------------------------------------

    public function addToCart(Request $request)
    {
        $poid = $request['id'];
        $q = $request['quantity'];
        $product = Product::find($poid);
        $q = 1; //initial quntity
        if(Auth::check()){
           $is_exist  = Cart::where('user_id',Auth::user()->id)->where('product_id',$request['id'])->first();
           if($is_exist){
               if ($product->quantity < $is_exist->quantity + $q) {
                   return $product->quantity;
               }
               $is_exist->quantity += $q ;
               $is_exist->save();
           }else{
               if ($product->quantity <  $q) {
                   return $product->quantity;
               }
               $add =new Cart();
               $add->user_id = Auth::user()->id;
               $add->product_id=$product->id;
               $add->quantity=$q;
               $add->price=$product->price;
               $add->country_id=1;
               $add->save();
           }
            $_SESSION['total'] += $q * $product->price;
            return response()->json(['msg' => trans('front.add_to_cart'), 'desc' => $product[$this->local .'_name'].' '.trans('front.add_to_cart_desc')], 200);

        }
        if (isset($_SESSION['cart'])) {
            if (is_array($_SESSION['cart'])) {
                $i = $this->product_exists($poid);
                if ($i >= 0) {//if producr already in shop cart
                    if ($product->quantity < ($_SESSION['cart'][$i]['quantity'] + $q)) {
                        return $product->quantity;
                    }
                    $_SESSION['cart'][$i]['quantity'] += $q;
                    if ($product->offer > 0) {
                        $_SESSION['total'] += $q * ($product->price - (($product->price * $product->offer / 100)));
                    } else {
                        $_SESSION['total'] += $q * $product->price;
                    }
                } else {
                    if ($product->quantity < $q) {
                        return $product->quantity;
                    }
                    $max = count($_SESSION['cart']);
                    $_SESSION['cart'][$max]['productid'] = $poid;
                    $_SESSION['cart'][$max]['quantity'] = $q;
                    if ($product->offer > 0) {
                        $_SESSION['total'] += $q * ($product->price - (($product->price * $product->offer / 100)));
                    } else {
                        $_SESSION['total'] += $q * ($product->price);
                    }
                }
            }
        } else {
// for first element in shop cart
            $_SESSION['cart'] = array();
            $_SESSION['cart'][0]['productid'] = $poid;
            $_SESSION['cart'][0]['quantity'] = $q;
            if ($product->offer > 0) {
                $_SESSION['total'] += $q * ($product->price - (($product->price * $product->offer / 100)));
            } else {
                $_SESSION['total'] += $q * $product->price;
            }
        }
        return response()->json(['msg' => trans('front.add_to_cart'), 'desc' => $product[$this->local .'_name'].' '.trans('front.add_to_cart_desc')], 200);

    }

    public function product_exists($pid)
    {
        //check if product exist in cart or not
        $pid = intval($pid);
        $max = count($_SESSION['cart']);
        $flag = -1;
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['cart'][$i]['productid']) {
                $flag = $i;
                return $i;
            }
        }
        return $flag;
    }

    public function removeFromCart(Request $request)
    {
        $pid = $request['id'];
        $product = Product::find($pid);
        if(Auth::check()) {
            $is_exist = Cart::where('user_id', Auth::user()->id)->where('product_id', $request['id'])->first();
            if ($is_exist) {
                $is_exist->delete();
            }
            $_SESSION['total'] -=  $_SESSION['total'] * $product->price;

            return response()->json(['msg' => trans('front.removed_from_cart'), 'desc' => trans('front.removed_from_cart_desc')], 200);

        }


        $max = count($_SESSION['cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['cart'][$i]['productid']) {
                if ($product->offer > 0) {
                    $_SESSION['total'] -= $_SESSION['cart'][$i]['quantity'] * ($product->price - (($product->price * $product->offer / 100)));
                } else {
                    $_SESSION['total'] -=  $_SESSION['total'] * $product->price;
                }
                unset($_SESSION['cart'][$i]);
                break;
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);
        return response()->json(['msg' => trans('front.removed_from_cart'), 'desc' => trans('front.removed_from_cart_desc')], 200);
    }

    public function editCart(Request $request)
    {
        $pid = intval($request->id);
        $new_q=$request->quantity;
        $product = Product::find($pid);
        $max = count($_SESSION['cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['cart'][$i]['productid']) {
                if ($product->quantity < $new_q) {
                    return response()->json(['msg' => trans('front.invalid_quantity'), 'desc' => trans('front.invalid_quantity_desc')], 200);

                }
                if ($product->offer > 0) {
                    $_SESSION['total'] -=$_SESSION['cart'][$i]['quantity'] * ($product->price - (($product->price * $product->offer / 100)));
                } else {
                    $_SESSION['total'] -=$_SESSION['cart'][$i]['quantity'] * $product->price;
                }
                $_SESSION['cart'][$i]['quantity'] = $new_q;
                if ($product->offer > 0) {
                    $_SESSION['total'] +=$new_q * ($product->price - (($product->price * $product->offer / 100)));
                } else {
                    $_SESSION['total'] +=$new_q* $product->price;
                }
                break;
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);
        return response()->json(['msg' => trans('front.edit_cart'), 'desc' => trans('front.edit_cart_desc')], 200);

    }

    public function Cart()
    {
        return view('frontend.ajax.small_cart');
    }
    public function total(){
        return $_SESSION['total'];
    }
    public function count(){
        if(Auth::check()){
            $cars  = Cart::where('user_id',Auth::user()->id)->get();
            return count($cars);

        }
        return count($_SESSION['cart']);
    }


}