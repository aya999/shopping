<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Block;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\WebsitePages;
use App\Models\Product;
use App\Models\Settings;
use App\Models\Slider;
use App\Models\WebsitePage;
use App\Models\Brand;
use Illuminate\Http\Request;
use Super;
class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
       // dd(Super::Location($request->ip()));

        $home_cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon', 'image')->where('is_home', 1)->get();
        if (isset($home_cats[0])) {
            $home_cats[0]->products = Product::select($this->local . '_name As name', 'image', 'price', 'id','category_id')->where('category_id', $home_cats[0]->id)->orderBy('viewers', 'desc')->take(6)->get();

        }
        $brands = Brand::select($this->local . '_name As name', 'id', 'image')->get();
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $slider = Slider::where('is_active', 1)->get();
        $newest = Product::select($this->local . '_name As name', 'image', 'price', 'id','category_id')->orderBy('created_at', 'desc')->take(6)->get();
        $blocks = Block::select('id',$this->local .'_name as name')->where('is_active',1)->get()->take(3);
        return view('frontend.index', compact('slider','blocks', 'home_cats', 'newest', 'pages', 'brands'));
    }


    public function page($id, $name)
    {
        $page = WebsitePage::find($id);
        if (!$page) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $page[$this->local.'meta_title'];
        $meta->meta_desc = $page[$this->local. 'meta_desc'];
        $meta->meta_tags = $page[$this->local. 'meta_tags'];
        if ($name != str_replace(' ', '-', $page[$this->local. '_name'])) {
            return redirect()->to($page->id . '/' . str_replace(' ', '-',$page[$this->local. '_name']));
        }
        /*   $services = Service::where('local', $this->lang)->get();
           $testimonials = Testmonial::all();
           $blogs = Blog::where('local', $this->lang)->orderBy('created_at', 'desc')->get()->take(2);*/

        return view('frontend.page', compact('page', 'meta'));
    }


    public function terms(){

    }
    public function policy(){

    }


}