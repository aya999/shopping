<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryProperty;
use App\Models\PackageTypeCountry;
use App\Models\CategoryPropertyOption;
use App\Models\Country;
use App\Models\Product;
use App\Models\City;
use App\Models\Brand;
use App\Models\ProductColor;
use App\Models\Package;
use App\Models\ProductPropertyValue;
use App\Models\Store;
use App\Models\WebsitePage;
use App\User;
use Illuminate\Http\Request;
use Super;
use Flashy;
use File;
use Image;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Storage;
use App\Models\ProductImage;
use App\Models\ProductSize;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function productDetails($cat, $id, $name)
    {
        $product = Product::find($id);
        if (!$product) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $product[$this->local . '_meta_title'];
        $meta->meta_desc = $product[$this->local . '_meta_desc'];
        $meta->meta_keywords = $product[$this->local . '_meta_keywords'];
        if ($name != str_replace(' ', '-', $product[$this->local . '_name'])) {
            return redirect()->to($this->local . '/'.  str_replace(' ', '-', $product->category[$this->local . '_name']) .'/'. $product->id . '/' . str_replace(' ', '-', $product[$this->local . '_name']));
        }
        $cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon')->get();
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $related = Product::where('category_id', $product->category_id)->where('id', '<>', $product->id)->select($this->local . '_name As name', 'image', 'price', 'id','category_id')->orderBy('created_at', 'desc')->take(10)->get();
        $latest = Product::select($this->local . '_name As name', 'image', 'price', 'id')->orderBy('created_at', 'desc')->take(4)->get();

        return view("frontend.products.product_details", compact('product', 'cats', 'related', 'latest', 'pages'))->render();
    }


    public function categoryDetails($id, $name, Request $request)
    {
         $category = Category::find($id);
        if (!$category) {
            abort(404);
        }
        $meta = new \stdClass();
        $meta->meta_title = $category[$this->local . '_meta_title'];
        $meta->meta_desc = $category[$this->local . '_meta_desc'];
        $meta->meta_keywords = $category[$this->local . '_meta_keywords'];
        if ($name != str_replace(' ', '_', $category[$this->local . '_name'])) {
            return redirect()->to($this->local . '/'. 'category/' . $category->id . '/' . str_replace(' ', '_', $category[$this->local . '_name']));
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon')->get();
        $sub_categories = Category::where('parent_id', $category->id)->pluck('id');
        $sub_sub_categories = Category::whereIn('parent_id', $sub_categories)->pluck('id');

        $properties = CategoryProperty::where('category_id', $category->id)->orWhereIn('category_id', $sub_categories)
            ->orWhereIn('category_id', $sub_sub_categories)->get();
        $products = Product::where(function ($query) use ($request, $sub_categories, $sub_sub_categories, $category, $properties) {
            if (isset($request->min_price)) {
                $query->where('price', '>', $request->min_price);
            }
            if (isset($request->max_price)) {
                $query->where('price', '<', $request->max_price);
            }
            foreach ($properties as $property) {
                if (isset($request[$this->local . '_'.$property[$this->local.'_name']])) {
                    if ($property->type_id == 1 && is_array($request[$this->local . '_'.$property[$this->local.'_name']])) //list
                    {
                        $ids = ProductPropertyValue::whereIn('option_id',$request[$this->local . '_'.$property[$this->local.'_name']])->pluck('product_id');
                        $query->whereIn('id', $ids);

                     } else if ($property->type_id == 2 && is_array($request[$this->local . '_'.$property[$this->local.'_name']])) //text
                    {
                        $ids = ProductPropertyValue::whereIn('value',$request[$this->local . '_'.$property[$this->local.'_name']])->pluck('product_id');
                        $query->whereIn('id', $ids);
                    }
                }
                if(isset($request['min_'.$property[$this->local.'_name']])){
                    $ids = ProductPropertyValue::whereIn('value','>',$request[$this->local . '_name'])->pluck('product_id');
                    $query->whereIn('id', $ids);
                } if(isset($request['max_'.$property[$this->local.'_name']])){
                    $ids = ProductPropertyValue::whereIn('value','<=',$request[$this->local . '_name'])->pluck('product_id');
                    $query->whereIn('id', $ids);
                }
            }
            if (isset($request->categories)) {
                 $sub_cats=Category::whereIn('parent_id',$request->categories)->pluck('id');
                 $query->whereIn('category_id',$request->categories)->orWhereIn('category_id',$sub_cats);
            } else {
                 $query->where('category_id', $category->id)
                    ->orWhereIn('category_id', $sub_categories)
                    ->orWhereIn('category_id', $sub_sub_categories);
            }

        })->where('is_active', '1')->paginate(10);


        /* $products = Product::whereIn('category_id', $category)->orWhereIn('category_id', $sub_categories)
             ->orWhereIn('category_id', $sub_sub_categories)->where('is_active', '1')->paginate(10);*/

        $colors = ProductColor::distinct()->pluck('color');
        $min_price = $products->min('price');
        $max_price = $products->max('price');


        foreach ($properties as $property) {
            if ($property->type_id == 1) //list
            {
                $list_values = CategoryPropertyOption::where('property_id', $property->id)->select($this->local . '_name As name', 'id')->get();
                $property->values = $list_values;
            } else if ($property->type_id == 2) //text
            {
                $text_values = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->distinct('value')->get();
                $property->values = $text_values;
            } else if ($property->type_id == 3) //number
            {
                $min_value = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->min('value');
                $property->min_value = $min_value;
                $max_value = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->max('value');
                $property->max_value = $max_value;
            }
        }
        return view("frontend.products.category_details", compact('category','sub_categories', 'products', 'cities', 'cats', 'max_price', 'min_price', 'colors', 'properties', 'pages'))->render();
    }

    public function brandDetails($id)
    {

        $cities = City::where('country_id', $country->id)->select($this->local . '_name As name', 'id')->get();
        $cats = Category::where('parent_id', null)->select($this->local . '_name As name', 'id', 'icon')->get();
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $brand = Brand::all()->find($id);

        $colors = ProductColor::distinct()->pluck('color');
        $min_price = Product::select($this->local . '_name as name', 'price', 'image', 'category_id', 'user_id', 'store_id')->where('brand_id', $id)->min('price');
        $max_price = Product::select($this->local . '_name as name', 'price', 'image', 'category_id', 'user_id', 'store_id')->where('brand_id', $id)->max('price');

        return view("frontend.products.brand_details", compact('brand', 'cities', 'cats', 'pages', 'max_price', 'min_price', 'colors'))->render();

    }


    public function Cart()
    {
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        return view('frontend.products.cart', compact('pages'));
    }

    public function addProduct( $id, $name)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if ($name != str_replace(' ', '_', $user->name)) {
            return redirect()->to($this->local . '/' . $country . '/profile/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $packages=Package::all();
        $store = Store::where('user_id', $id)->get();
        return view('frontend.user.products.create', compact('user', 'pages', 'store','packages'));

    }

    public function update( $id)
    {
        $user = Auth::user();
        if (!$user) {
            abort(404);
        }
        $packages=Package::all();

        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $row = Product::find($id);
        $category = Category::find($row->category_id);
        $properties = CategoryProperty::where('category_id', $category->id)->get();
        $sizes = ProductSize::where('product_id', $row->id)->get()->pluck('size')->toArray();
        $colors = ProductColor::where('product_id', $row->id)->get();

        return view('frontend.user.products.edit', compact('user', 'pages', 'row', 'category', 'properties', 'sizes', 'colors','packages'));

    }

    public function storeProduct(Request $request)
    {
        $package_type=$request->selected_package;
        $package_country=PackageTypeCountry::where('package_type_id',$package_type)->where('country_id','1')->get();
        if(isset($package_country))
        {
            if($package_country->first() != null)
            {
                $package_type_country_id=$package_country->first()->id;
                $request->package_type_country_id=$package_type_country_id;
            }


        }
        $row = Product::create($request->all());

        if ($row) {

/*            $row->update($row);*/
            $properties = CategoryProperty::where('category_id', $request->category_id)->get();
            foreach ($properties as $property) {
                $property_value = new ProductPropertyValue();
                $property_value->product_id = $row->id;
                $property_value->property_id = $property->id;
                if ($property->type->id == 1) {
                    $property_value->option_id = $request[str_replace(' ', '_', $property->name) . $property->id];
                } else {
                    $property_value->value = $request[str_replace(' ', '_', $property->name) . $property->id];
                }
                $property_value->save();
            }
            if ($request->hasFile('images')) {
                //Product Gallary
                $images = $request->file('images');
                foreach ($images as $photo) {
                    $name = str_random(6) . '_' . $photo->getClientOriginalName();
                    $extension = strtolower($photo->getClientOriginalExtension());
                    if ($extension == "jpg" || $extension == "jpeg" || $extension == "png" || $extension == "gif") {
                        $galary = new ProductImage();
                        $galary->image = $name;
                        $galary->product_id = $row->id;
                        $galary->save();
                        // $dest = 'assets/images/products/';

                        $imageLocation = getcwd() . '/assets/images/products/' . $name;
                        Super::uploadImage($photo, $imageLocation, 600, 600, 600);
                        //$photo->move($dest, $name);
                    }
                }
            }
            if ($request->has('sizes')) {
                $sizes = $request['sizes'];
                for ($i = 0; $i < count($sizes); $i++) {
                    if (!empty($sizes[$i])) {
                        $new = new ProductSize();
                        $new->product_id = $row->id;
                        $new->size = $sizes[$i];
                        $new->save();
                    }
                }
            }

            if ($request->has('colors')) {
                $colors = $request['colors'];
                for ($i = 0; $i < count($colors); $i++) {
                    if (!empty($colors[$i])) {
                        $new = new ProductColor();
                        $new->product_id = $row->id;
                        $new->color = $colors[$i];
                        $new->save();
                    }
                }
            }
            $user = User::find(Auth::user()->id);
            Flashy::message(trans('dashboard.dashboard.added_product'));
            return redirect()->to($this->local . '/eg/profile/products/' . $user->id . '/' . str_replace(' ', '_', $user->name));
        }
    }

    public function updateProduct(Request $request, $country, $id)
    {
        $row = Product::find($id);
        $package_type=$request->selected_package;
        $package_country=PackageTypeCountry::where('package_type_id',$package_type)->where('country_id','1')->get();
        if(isset($package_country))
        {
            if($package_country->first() != null)
            {
                $package_type_country_id=$package_country->first()->id;
                $request->package_type_country_id=$package_type_country_id;
            }


        }
        $update = $row->update($request->all());
        if ($request->hasFile('images')) {
            //Product Gallary
            $images = $request->file('images');
            foreach ($images as $photo) {
                $name = str_random(6) . '_' . $photo->getClientOriginalName();
                $extension = strtolower($photo->getClientOriginalExtension());
                if ($extension == "jpg" || $extension == "jpeg" || $extension == "png" || $extension == "gif") {
                    $galary = new ProductImage();
                    $galary->image = $name;
                    $galary->product_id = $row->id;
                    $galary->save();
                    // $dest = 'assets/images/products/';

                    $imageLocation = getcwd() . '/assets/images/products/' . $name;
                    Super::uploadImage($photo, $imageLocation, 600, 600, 600);
                    //$photo->move($dest, $name);
                }
            }
        }
        if ($request->has('sizes')) {
            $row->sizes()->delete();
            foreach ($request->sizes as $i => $size) {
                $size_obj = new ProductSize();
                $size_obj->size = $size;
                $size_obj->product_id = $row->id;
                $size_obj->save();
            }
        }
        if ($request->has('colors')) {
            $row->colors()->delete();
            foreach ($request->colors as $i => $color) {
                $color_obj = new ProductColor();
                $color_obj->color = $color;
                $color_obj->product_id = $row->id;
                $color_obj->save();
            }
        }
        return redirect()->to($this->local . '/eg/profile/products/' . $row->user->id . '/' . str_replace(' ', '_', $row->user->name));

    }

    public function subcategory($country, $id)
    {
        $category = Category::where('parent_id', $id)->get();;
        return view("frontend.products.sub_category", compact('category'))->render();

    }

    public function categoryProperties($country, $id)
    {

        $category = Category::find($id);
        $properties = CategoryProperty::where('category_id', $id)->get();
        return view("frontend.products.properties", compact('properties', 'category'))->render();

    }

    public function brands($category)
    {

        $brands = Brand::where('category_id', $category)->select($this->local . '_name As name', 'id')->get();
        return response()->json(['data' => $brands], 200);
    }

    public function deleteProduct($country, $id)
    {

        $row = Product::find($id);
        if (!$row) {
            return response()->json(['msg' => true], 404);
        }
        $row->delete();
        return response()->json(['msg' => true], 200);

    }
}