<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryProperty;
use App\Models\CategoryPropertyOption;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\WebsitePage;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {

        $products = Product::where(function ($query) use ($request) {

            if (isset($request->keyword) && !empty($request->keyword)) {
                $query->Where($this->local . '_name', 'LIKE', '%' . $request->keyword . '%');
            }
            if (isset($request->categories)  && !empty($request->categories) && is_array($request->categories)) {
                 $query->whereIn('category_id', $request->categories);
            }
            if (isset($request->cities)  && !empty($request->cities) && is_array($request->cities)) {
                 $users = User::whereIn('city_id', $request['cities'])->get()->pluck('id');
                $query->whereIn('user_id', $users);
            }
        });
        $cat_ids = $products->pluck('category_id');
        $product_ids = $products->pluck('id');
        $min_price = $products->min('price');
        $max_price = $products->max('price');
        $properties = CategoryProperty::whereIn('category_id', $cat_ids)->get();
        foreach ($properties as $property) {
            if ($property->type_id == 1) //list
            {
                $list_values = CategoryPropertyOption::where('property_id', $property->id)->select($this->local . '_name As name', 'id')->get();
                $property->values = $list_values;
            } else if ($property->type_id == 2) //text
            {
                $text_values = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->distinct('value')->get();

                $property->values = $text_values;
            } else if ($property->type_id == 3) //number
            {
                $min_value = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->min('value');
                $property->min_value = $min_value;
                $max_value = ProductPropertyValue::where('property_id', $property->id)->select('value as name', 'id')->max('value');
                $property->max_value = $max_value;
            }
        }
        $pages = WebsitePage::select($this->local . '_name As name', 'id')->get();
        $categories = Category::whereIn('id', $cat_ids)->select($this->local . '_name', 'icon')->get();
        $products = $products->paginate(1);
        $colors = ProductColor::distinct()->pluck('color');
        $products->appends($_GET)->links();
        return view("frontend.products.search", compact('category', 'products', 'cities', 'categories', 'max_price', 'min_price', 'colors', 'properties', 'pages'))->render();

    }

}