<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use LaravelLocalization;


class Controller extends BaseController
{
    public function __construct()
    {
        $local = LaravelLocalization::getCurrentLocale();
        $this->local=$local;
        view()->share('local', $local);

    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
