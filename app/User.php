<?php

namespace App;

use App\Models\AccountUser;
use App\Models\Account;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductLike;
use App\Models\UserFollow;
use App\Models\Store;
use App\Models\StoreFollow;
use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Super;
use Auth;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','s_name', 'password', 'country_code', 'mobile', 'image', 'bg', 'address', 'lat', 'long', 'facebook', 'twitter', 'country_id',
        'instagram', 'google', 'verification_code', 'wallet', 'no_views', 'is_public', 'no_follows', 'no_followings', 'no_adds', 'gender',
        'is_active', 'is_confirm', 'is_notification','is_driver', 'account_id', 'provider', 'provider_id', 'birth_date', 'provider_image','address_id'
    ];

    public function account()
    {
        if (!empty($this->account_id)) {
            $account =  Account::find($this->account_id);
            $is_avail=AccountUser::where('user_id',$this->id)->where('account_id',$account->id)->get()->first();
            if($is_avail){
                if($is_avail->due_date >= Carbon::now()){
                    $is_avail->account = $account ;
                    return $is_avail;
                }else{
                    $free_account = new \stdClass();
                    $free_account->name = trans('front.free_account');
                    return $free_account;
                }
            }
            $free_account = new \stdClass();
            $free_account->name = trans('front.free_account');
            return $free_account;
        }
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function country()
    {

        return $this->belongsTo(Country::class);
    }

    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/users/web/' . $imageName;
            Super::uploadImage($image, $imageLocation, 255, 255, 170);

            $imageLocation = getcwd() . '/assets/images/users/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation);


            $imageLocation = getcwd() . '/assets/images/users/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation);

            $this->attributes['image'] = $imageName;

        }
    }

    public
    function favoriteProducts()
    {
        if (Auth::check()) {
            $list = ProductLike::where('user_id', '=', Auth::user()->id)->get();
        } else {
            $list = Null;
        }
        return $list;
    }

    public
    function isProductFavorite($product_id)
    {
        $list = $this->favoriteProducts();
        if (count($list) > 0) {
            foreach ($list as $w) {
                if ($w->product_id == $product_id) {
                    return true; // So This item already in my favorite list
                }
            }
        } else {
            return false;
        }
    }

    public
    function isUseFollow($user_id)
    {
        $is_follow = UserFollow::where('follower_id', Auth::user()->id)->where('following_id', $user_id)->where('is_follow', 1)->get()->first();
        if ($is_follow) {
            return true;
        } else {
            return false;
        }
    }

    public
    function isStoreFollow($store_id)
    {
        $is_follow = StoreFollow::where('follower_id', Auth::user()->id)->where('store_id', $store_id)->where('is_follow', 1)->get()->first();
        if ($is_follow) {
            return true;
        } else {
            return false;
        }
    }

    public
    function products()
    {
        return $this->hasMany(Product::class);
    }

    public
    function stores()
    {
        return $this->hasMany(Store::class);
    }

    public
    function profileImage($width = 200)
    {
        if ($this->provider == "facebook") {
            return "https://graph.facebook.com/v3.0/" . $this->provider_id . '/picture?width=' . $width;
        } elseif ($this->provider == "google") {
            return $this->provider_image;
        } else {
            asset($this->image);
        }
    }
}
