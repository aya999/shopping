<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProperty extends Model
{
    protected $fillable=['en_name','ar_name','category_id','type_id'];
    public function type(){
        return $this->belongsTo(PropertyType::class);
    }
    public function options(){
        return $this->hasMany(CategoryPropertyOption::class,'property_id');
    }
}
