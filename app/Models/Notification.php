<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['sender_id','receiver_id','sender_type','receiver_type','ar_action','en_action','object_id','object_type','is_seen'];
}
