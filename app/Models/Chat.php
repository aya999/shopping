<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;
class Chat extends Model
{
    protected $fillable = ['sender_id','receiver_id','message','image','is_seen','seen_date','product_id','store_id','is_sender_delete','is_receiver_delete'];
    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/chats/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 300);

            $this->attributes['image'] = $imageName;

        }
    }
}
