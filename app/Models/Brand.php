<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;

class Brand extends Model
{
    protected $fillable = ['id','image', 'en_name', 'ar_name', 'en_meta_desc', 'ar_meta_desc', 'en_meta_title', 'ar_meta_title',
        'en_meta_tags', 'ar_meta_tags', 'is_active', 'category_id'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/brands/web/' . $imageName;
            Super::uploadImage($image, $imageLocation, 168, 168, 84);
            $this->attributes['image'] = $imageName;
        }

    }

}