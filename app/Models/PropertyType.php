<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
     protected $fillable=['en_name','ar_name'];
}
