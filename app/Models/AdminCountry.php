<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminCountry extends Model
{
    protected $fillable=['admin_id','country_id'];
}
