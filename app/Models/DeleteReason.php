<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeleteReason extends Model
{
    protected $fillable = ['ar_desc','en_desc'];
}
