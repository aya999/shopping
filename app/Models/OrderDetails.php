<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $fillable = ['order_id', 'product_id', 'quantity', 'price', 'color_id', 'size_id', 'shipment_num', 'status', 'is_paid', 'qr', 'shipped_date', 'delivered_date', 'delivery_price', 'is_complete'];
    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function size(){
        return $this->belongsTo(ProductSize::class);
    }
    public function color(){
        return $this->belongsTo(ProductColor::class);
    }
}
