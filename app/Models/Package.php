<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['id','ar_name','en_name','views','balance','is_active','is_special'];

}
