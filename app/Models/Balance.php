<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = ['ar_name','en_name','points' ];


    public function countries(){
        return $this->belongsToMany(Country::class ,'balance_countries');
    }


    public function getPrice($country_id,$balance){
        $isexist= BalanceCountry::where('balance_id',$balance)->where('country_id',$country_id)->first();

        if(isset($isexist))
        {
            return $isexist->price;
        }
        return '';
    }
}
