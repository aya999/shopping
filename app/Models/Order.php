<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id','country_id','f_name','s_name',
        'address','mobile','total','date','time','mobile','payment_type',
        'is_fast','address_id','code','is_paid','qr','shipment_num','status'];

    public function isComplete(){
        $dets = OrderDetails::where('order_id',$this->id)->get();
        foreach ($dets  as $det){
            if(!$det->status != 2){
                return false;
            }
        }
        return true;
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function details(){
        return $this->hasMany(OrderDetails::class);
    }
    public function status(){
        $dets = OrderDetails::where('order_id',$this->id)->min('status');
        return $dets;

    }

}
