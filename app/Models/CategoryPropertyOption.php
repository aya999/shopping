<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPropertyOption extends Model
{
    protected $fillable=['en_name','ar_name','property_id'];
}
