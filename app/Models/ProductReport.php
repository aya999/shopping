<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReport extends Model
{
    protected $fillable =['user_id','product_id','desc','name','email','mobile'];
}
