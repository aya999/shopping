<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['en_title', 'ar_title', 'mobile', 'email', 'facebook', 'twitter', 'instagram',
        'address', 'google', 'en_meta_desc', 'ar_meta_desc', 'en_meta_title', 'ar_meta_title',
        'en_meta_tags', 'ar_meta_tags','en_store_message','ar_store_message',
        'youtube', 'pin',];
}
