<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;
class Receipt extends Model
{
    protected $fillable = ['order_id','store_id','user_is','driver_id','image','status','is_read'];
    public function setImageAttribute($image)
    {
        if ($image) {
            $image_name = Super::uploadFile($image, 'assets/images/events/', 240, 240, 265);
            $this->attributes['image'] = 'assets/images/events/' . $image_name;
        }
    }
}
