<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['ar_name','en_name','country_id'];
    public function country(){
        return $this->belongsTo(Country::class);
    }
}
