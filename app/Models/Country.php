<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;
class Country extends Model
{
    protected $fillable = ['ar_name','en_name','image','code','phone_code','en_currency_name','ar_currency_name','currency_code','currency_symbol'
        ,'points','lang','shipping_price','max_free_adds','max_free_points','en_title','ar_title','en_meta_desc','ar_meta_desc','en_meta_title'
        ,'ar_meta_title','en_meta_tags','ar_meta_tags','is_online_users','fast_days','fast_price','is_cash_users','is_online_store','is_cash_store',
        'postal_code','max_free_days'
    ];
    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/countries/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation, 70, 70, 70);


            $imageLocation = getcwd() . '/assets/images/countries/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation, 163, 163, 163);

            $this->attributes['image'] = $imageName;
        }
    }

    public function users(){
        return $this->hasMany(User::class);
    }
    public function cities(){
        return $this->hasMany(City::class);
    }
}

