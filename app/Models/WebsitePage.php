<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebsitePage extends Model
{
    protected $fillable = ['en_name','ar_name','en_desc','ar_desc','en_meta_desc','ar_meta_desc','en_meta_title','ar_meta_title',
        'en_meta_tags','ar_meta_tags','is_active','is_header','is_footer'];
}
