<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreeAdds extends Model
{
    protected $fillable=['country_id','category_id','count'];
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
