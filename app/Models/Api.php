<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $fillable=['ar_name','en_name','method','en_desc','ar_desc','required_parameters','parameters','and_is_active','ios_is_active','path'];
}
