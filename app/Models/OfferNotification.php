<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferNotification extends Model
{
    protected $fillable=['user_id','category_id','is_notify'];
}
