<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceOffer extends Model
{
    protected $fillable=['product_id','user_id','country_id','product_price','price','is_approve'];
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
