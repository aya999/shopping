<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['address','lat','long','user_id','postal_code','city_id','region_id','name','building','apartment','mobile','phone','landmark','floor','street'];
}
