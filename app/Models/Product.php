<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Super;

class Product extends Model
{
    protected $fillable = ['image', 'en_name', 'ar_name', 'en_desc', 'ar_desc', 'en_brief_desc', 'ar_brief_desc','video',
        'en_meta_desc', 'brand_id', 'is_avail','lat','long','mobile',
        'viewers', 'ar_meta_desc', 'en_meta_title', 'ar_meta_title', 'en_meta_tags', 'ar_meta_tags', 'price',
        'quantity', 'offer', 'is_online', 'is_active', 'category_id', 'user_id', 'store_id', 'code','package_type_country_id','city_id','region_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function sizes()
    {
        return $this->hasMany(ProductSize::class);
    }

    public function colors()
    {
        return $this->hasMany(ProductColor::class);
    }

    public function properties()
    {
        return $this->hasMany(ProductPropertyValue::class);
    }

    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/products/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation);

            $imageLocation = getcwd() . '/assets/images/products/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation, 238, 238, 238);

            $imageLocation = getcwd() . '/assets/images/products/detail/' . $imageName;
            Super::uploadImage($image, $imageLocation, 600, 600, 600);

            $imageLocation = getcwd() . '/assets/images/products/web/' . $imageName;
            Super::uploadImage($image, $imageLocation, 207, 207, 207);

            $this->attributes['image'] = $imageName;
        }

    }

    public function rate()
    {
        $x = $this->hasMany(ProductRate::class)->avg('rate');
        if ($x == null) {
            return 0;
        }
        return $x;
    }
    public function likes(){
        return $this->hasMany(ProductLike::class)->count();
    }
}
