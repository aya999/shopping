<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;
class Slider extends Model
{
    protected $fillable=['image','is_active','url'];
    public function setImageAttribute($image) {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/sliders/web/' . $imageName;
            Super::uploadImage($image,$imageLocation , 960, 960, 475);

            $imageLocation = getcwd() . '/assets/images/sliders/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 200);

            $this->attributes['image']= $imageName;
        }
    }
}
