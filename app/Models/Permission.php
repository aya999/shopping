<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $fillable = ['name','key','page_id','permission_type_id'];
    public function rolePermission(){
        return $this->hasMany(RolePermission::class);
    }


    public function hasRole($id){
        $isexist=$this->hasMany(RolePermission::class)->where('role_id',$id)->first();
        if(isset($isexist))
        {
            return true;
        }
        return false;
    }

}
