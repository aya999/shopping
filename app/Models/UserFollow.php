<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFollow extends Model
{
    protected $fillable=['follower_id','following_id','rate','is_follow','is_blocked'];
}
