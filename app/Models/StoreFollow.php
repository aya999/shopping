<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreFollow extends Model
{
    protected $fillable=['follower_id','store_id','is_follow'];
}
