<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable=['en_title','ar_title','en_desc','ar_desc','type'];
}
