<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferLike extends Model
{
    protected $fillable = ['offer_id','user_id'];
}
