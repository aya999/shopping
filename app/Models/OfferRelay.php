<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferRelay extends Model
{
    protected $fillable =['user_id','price_offer_id','reply'];

    public function offer(){
        return $this->belongsTo(PriceOffer::class,'price_offer_id');
    }
}
