<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Super;
class Mobile extends Model
{
  protected $fillable=['video','categories_background','buyonline_background','stores_background','about_background','en_about','ar_about'];

    public function setCategoriesBackgroundAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/backgrounds/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 170);
            $this->attributes['categories_background'] = $imageName;
        }
    }
    public function setBuyonlineBackgroundAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/backgrounds/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 170);
            $this->attributes['buyonline_background'] = $imageName;
        }
    }
    public function setStoresBackgroundAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/backgrounds/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 170);
            $this->attributes['stores_background'] = $imageName;
        }
    }
    public function setAboutBackgroundAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/backgrounds/' . $imageName;
            Super::uploadImage($image, $imageLocation, 300, 300, 170);
            $this->attributes['about_background'] = $imageName;
        }
    }

}
