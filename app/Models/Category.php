<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FileTrait;
use Super;

class Category extends Model
{
    protected $fillable = ['ar_name', 'en_name', 'parent_id', 'is_online', 'en_desc', 'ar_desc', 'image','banner',
        'is_color', 'is_size',
        'en_meta_desc', 'ar_meta_desc', 'en_meta_title', 'ar_meta_title', 'en_meta_tags',
        'ar_meta_tags', 'icon', 'is_home'];

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function brand()
    {
        return $this->belongsToMany(Brand::class, 'products')->distinct('id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function properties()
    {
        return $this->hasMany(CategoryProperty::class);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/categories/web/' . $imageName;

            Super::uploadImage($image, $imageLocation, 255, 255, 170);

            $imageLocation = getcwd() . '/assets/images/categories/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation);


            $imageLocation = getcwd() . '/assets/images/categories/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation);

            $this->attributes['image'] = $imageName;
        }
    }
    public function setBannerAttribute($image)
    {
        if ($image) {
            $imageName = time() . '_banner.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/categories/web/' . $imageName;

            Super::uploadImage($image, $imageLocation, 255, 255, 170);

            $imageLocation = getcwd() . '/assets/images/categories/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation, 70, 70, 70);


            $imageLocation = getcwd() . '/assets/images/categories/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation, 163, 163, 163);

            $this->attributes['image'] = $imageName;
        }
    }

    public function setIconAttribute($image)
    {
        if ($image) {
            $imageName = time() . '_icon.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/categories/icons/' . $imageName;

            Super::uploadImage($image, $imageLocation, 26, 26, 26);
            $this->attributes['icon'] = $imageName;
        }
    }

    public function isColor(){
        if($this->is_color == 1){
            return true;
        }
        $childs=$this->categories;
        foreach ($childs as $child){
            if($child->is_color == 1){
                return true;
            }
            $grand_sons=$child->categories;
            foreach ($grand_sons as $grand_son) {
                if ($grand_son->is_color == 1) {
                    return true;
                }
            }
        }
        return false;
    }
    public function isSize(){
        if($this->is_size == 1){
            return true;
        }
        $childs=$this->categories;
        foreach ($childs as $child){
            if($child->is_size == 1){
                return true;
            }
            $grand_sons=$child->categories;
            foreach ($grand_sons as $grand_son) {
                if ($grand_son->is_size == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
