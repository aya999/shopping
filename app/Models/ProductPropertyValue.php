<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyValue extends Model
{
    protected $fillable=['property_id','product_id','option_id','value'];
    public function property(){
        return $this->belongsTo(CategoryProperty::class);
    }
    public function option(){
        return $this->belongsTo(CategoryPropertyOption::class);
    }

}
