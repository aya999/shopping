<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FileTrait;
use Super;
class StoreCategory extends Model
{
    protected $fillable = ['category_id','store_id'];

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function stores(){
        return $this->hasMany(Store::class);
    }

}
