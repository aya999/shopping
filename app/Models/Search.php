<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $fillable=['keyword','user_id','country_id','is_ok'];
}
