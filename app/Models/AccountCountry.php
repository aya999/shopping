<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Count;

class AccountCountry extends Model
{
    protected $fillable = ['account_id','country_id','price'];
    public function accounts(){
        return $this->belongsTo(Account::class);
    }
    public function countries(){
        return $this->belongsTo(Country::class);
    }



}
