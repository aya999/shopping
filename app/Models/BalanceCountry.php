<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceCountry extends Model
{
    protected $fillable = ['balance_id','country_id','price' ];
    public function balances(){
        return $this->belongsTo(Balance::class);
    }
    public function countries(){
        return $this->belongsTo(Country::class);
    }
}
