<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\StoreCategory;
use App\Models\Category;
use Super;
class Store extends Model
{
    protected $fillable = ['ar_name','en_name','user_id','address','mobile','email','en_desc','ar_desc','is_active','en_meta_desc','no_follows','no_views','no_adds','email','fa'
        ,'ar_meta_desc','en_meta_title','ar_meta_title','en_meta_tags','ar_meta_tags','image','bg','lat','long','facebook','twitter','google','instagram','youtube','video','city_id','region_id','is_verified'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function city(){
        return $this->belongsTo(City::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class ,'store_categories');
    }
    public function setImageAttribute($image) {
        if ($image) {

            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/stores/web/' . $imageName;
            Super::uploadImage($image, $imageLocation, 255, 255, 170);

            $imageLocation = getcwd() . '/assets/images/stores/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation);


            $imageLocation = getcwd() . '/assets/images/stores/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation);

            $this->attributes['image']= $imageName;
        }

    }
    public function setBgAttribute($image) {

        if ($image) {

            $imageName = time() . '_back.' . $image->getClientOriginalExtension();

            $imageLocation = getcwd() . '/assets/images/stores/web/' . $imageName;
            Super::uploadImage($image, $imageLocation,960, 960, 475);

            $imageLocation = getcwd() . '/assets/images/stores/thumbnail/' . $imageName;
            Super::uploadImage($image, $imageLocation, 500, 500, 300);


            $imageLocation = getcwd() . '/assets/images/stores/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation, 500, 500, 300);


            $this->attributes['bg']= $imageName;
        }
    }

    public function rate()
    {
        $x = $this->hasMany(StoreRate::class)->avg('rate');
        if ($x == null) {
            return 0;
        }
        return $x;
    }
    public function addsCount()
    {
        return count($this->hasMany(Product::class));
    }
}
