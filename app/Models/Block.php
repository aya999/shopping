<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable = ['en_name', 'ar_name', 'is_active'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'block_products');

    }

    public function showProducts($local)
    {
        return $this->belongsToMany(Product::class, 'block_products')->select($local . '_name As name', 'image', 'price', 'products.id','category_id')->get()->take(8);
    }
}
