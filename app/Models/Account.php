<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['ar_name','en_name','balance','ads','is_active','days'];
    public function users(){
        return $this->hasMany(User::class);
    }
    public function countries(){
        return $this->belongsToMany(Country::class ,'account_countries');
    }


    public function getPrice($country_id,$account){
        $isexist= AccountCountry::where('account_id',$account)->where('country_id',$country_id)->first();

        if(isset($isexist))
        {
            return $isexist->price;
        }
        return '';
    }
}
