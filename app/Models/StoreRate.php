<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreRate extends Model
{
    protected $fillable=['user_id','store_id','rate','comment'];
}
