<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['user_id', 'product_id','country_id', 'quantity', 'color_id', 'size_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);

    }
}
