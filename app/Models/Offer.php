<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Super;
class Offer extends Model
{
    protected $fillable=['en_name','ar_name','en_desc','ar_desc','mobile','email','user_id','category_id','is_active','viewers','store_id','image','city_id','region_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function setImageAttribute($image)
    {
        if ($image) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $imageLocation = getcwd() . '/assets/images/offers/mobile/' . $imageName;
            Super::uploadImage($image, $imageLocation);



            $imageLocation = getcwd() . '/assets/images/offers/web/' . $imageName;
            Super::uploadImage($image, $imageLocation);

            $this->attributes['image'] = $imageName;
        }

    }
}
