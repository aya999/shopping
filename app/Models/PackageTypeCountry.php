<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageTypeCountry extends Model
{
    protected $fillable = ['id','price','country_id','package_type_id'];
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function PackageType(){
        return $this->belongsTo(PackageType::class);
    }
}
