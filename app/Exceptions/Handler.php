<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Request;
use Super;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);

        if ($exception instanceof NotFoundHttpException) {

            if (Request::segment(1) == 'api') {
                return Super::jsonResponse(false, 404, [], "Page Not Found", []);
            }
            else if (Request::segment(2) == 'admin') {
                $local=Request::segment(1);
                return response()->view('admin.errors.404', compact('local'), 404);

            } else {
                return parent::render($request, $exception);
            }
        }
        if (Request::segment(1) == 'api') {
            return Super::jsonResponse(false, 500, [], $exception, []);
        }
        return parent::render($request, $exception);
    }
}
