<?php

namespace App;

use App\Models\AdminCountry;
use App\Models\Country;
use App\Notifications\AdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;
use App\Models\Permission;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'is_active', 'role_id', 'mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class, 'admin_countries');
    }

    public function allCountries()
    {
        if ($this->id == 1) {
            return Country::all();
        } else {
            $countries_id = AdminCountry::where('admin_id', $this->id)->get()->pluck('country_id');
            return Country::whereIn('id', $countries_id)->get();
            /*            return $this->belongsToMany(Country::class,'admin_countries');*/
        }
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function hasRole($role)
    {
        if ($this->role->name == $role) {
            return true;
        }
        return false;
    }

    public function hasPermission($permission_name)
    {

        if ($this->id == 1) {
            return true;
        }
        $permission = Permission::where('key', $permission_name)->first();
        if (isset($permission)) {
            $exist = $this->role->rolePermission->where('permission_id', $permission->id)->first();
            if (isset($exist)) {
                return true;
            }
        }


        return false;
    }

    //check if user has one  given Rules
    public function hasAnyRoles($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPassword($token));
    }
}
