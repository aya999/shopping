<?php
Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');
    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AdminAuth\RegisterController@register');
    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
Route::group(['prefix' => 'company'], function () {
    Route::get('/login', 'CompanyAuth\LoginController@showLoginForm')->name('company.login');
    Route::post('/login', 'CompanyAuth\LoginController@login');
    Route::post('/logout', 'CompanyAuth\LoginController@logout')->name('company.logout');
    /*
     Route::post('/password/email', 'CompanyAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
     Route::post('/password/reset', 'CompanyAuth\ResetPasswordController@reset')->name('password.email');
     Route::get('/password/reset', 'CompanyAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
     Route::get('/password/reset/{token}', 'CompanyAuth\ResetPasswordController@showResetForm');
    */
});

Route::get('/clear', function () {
    $exitCode = Artisan::call('cache:clear');
    return $exitCode;
});
session_start();
if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = [];
    $_SESSION['total'] = 0;
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Facebook Login
Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');
//Facebook Login
Route::get('login/google', 'Auth\LoginController@redirectToGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');

// Authentication Routes...
$this->post('login', 'Auth\LoginController@login');
$this->get('logout', 'Auth\LoginController@logout')->name('user.logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('trans/{lang}/{current_lang}', function ($lang, $current) {
    $url = url()->previous();
    $redir = str_replace('/' . $current, '/' . $lang, $url);
    return redirect()->to($redir);
}); //Done


Route::group(['namespace' => 'Auth'], function () {
    // Registration Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('user.login');
    $this->get('register', 'RegisterController@showRegistrationForm')->name('user.register');
    $this->post('register', 'RegisterController@register');
});

Route::group(['middleware' => ['auth'], 'namespace' => 'Frontend'], function () {
    Route::get('products/like', 'AjaxController@productLike');
    Route::post('products/rate', 'AjaxController@productRate');
    Route::get('users/follow', 'UserController@follow');
    Route::get('user_followers/{user_id}', 'AjaxController@userFollowers');
});
Route::group(['namespace' => 'Frontend'], function () {
    Route::get('terms', 'HomeController@terms');
    Route::get('policy', 'HomeController@policy');


    Route::get('/', 'HomeController@index');
    Route::get('product_quick_view/{id}', 'AjaxController@productDetails');
    Route::get('search', 'SearchController@index');
//------------------------------Api for Home Page -------------------------------------
    Route::get('new_arrivals', 'AjaxController@newArrivals');
    Route::get('featured_products', 'AjaxController@featuredProducts');
    Route::get('popular_products', 'AjaxController@popularProducts');
    Route::get('best_sellers', 'AjaxController@bestSellers');
    Route::get('category_tab/{cat_id}', 'AjaxController@CategoryTab');
//-------------------------------------Stores--------------------------------------------------
    Route::get('stores/{cat_id}/{cat_name}', 'StoreController@categoryStore');
    Route::get('store/{store_id}/{store_name}', 'StoreController@store');
    Route::get('store/{store_id}/{store_name}/followers', 'StoreController@followers');

//-----------------------------------USER----------------------------------------------------
    Route::get('profile/products/add/{id}/{name}', 'ProductController@addProduct');
    Route::get('profile/products/update/{id}', 'ProductController@update');
    Route::post('profile/products/storeProduct', 'ProductController@storeProduct');

    Route::get('profile/products/edit', 'ProductController@addProduct');
    Route::put('profile/products/updateProduct/{id}', 'ProductController@updateProduct');
    Route::delete('profile/products/delete/{id}', 'ProductController@deleteProduct');

    Route::get('profile/{id}/{name}', 'UserController@profile');
    Route::post('frontend/users/profileUpdate', 'UserController@profileUpdate')->name('frontend.users.profileUpdate');
    Route::get('profile/products/{id}/{name}', 'UserController@products')->name('frontend.users.products');
    Route::get('profile/likes/{id}/{name}', 'UserController@likes');
    Route::get('profile/followers/{id}/{name}', 'UserController@followers');
    Route::get('profile/following/{id}/{name}', 'UserController@following');
    Route::get('profile/block_list/{id}/{name}', 'UserController@blocked');
    Route::get('profile/blocked/{id}/{name}', 'UserController@blocked');
//----------------------------------Cart-------------------------------------------------------------
    Route::get('products/cart', 'ProductController@Cart');
    Route::get('products/cart/small', 'AjaxController@Cart');
    Route::get('products/cart/add', 'AjaxController@addToCart');
    Route::get('products/cart/edit', 'AjaxController@editCart');
    Route::get('products/cart/delete', 'AjaxController@removeFromCart');
    Route::get('products/cart/total', 'AjaxController@total');
    Route::get('products/cart/count', 'AjaxController@count');

    Route::get('products/properties/{id}', 'ProductController@categoryProperties');
    Route::get('products/get_subcategory/{id}', 'ProductController@subcategory');
    Route::get('products/category_brands/{category_id}', 'ProductController@brands');
//------------------------------- products -----------------------------------------------
    Route::get('category/{id}/{name}', 'ProductController@categoryDetails');
    Route::get('{category}/{id}/{name}', 'ProductController@productDetails');
    Route::get('brand/{id}', 'ProductController@brandDetails');

    Route::get('{page_id}/{name}', 'HomeController@page');


});





