<?php

Route::get('/Roles/GetPermistion/','RoleController@GetPermistion' );
Route::get('/', 'DashboardController@dashboard');
Route::resource('/countries', 'CountryController');
Route::post('data/countries', 'CountryController@getData');
Route::resource('/free_adds', 'FreeAddController');

Route::resource('/cities', 'CityController');
Route::post('data/cities', 'CityController@getData');

Route::resource('/users', 'UserController');
Route::post('data/users', 'UserController@getData');

Route::PATCH('activation/users/{id}', 'UserController@activation');
Route::resource('/admins', 'AdminController');

Route::post('data/admins', 'AdminController@getData');
Route::PATCH('activation/admins/{id}', 'AdminController@activation');

Route::resource('/roles', 'RoleController');
Route::post('data/roles', 'RoleController@getData');

Route::resource('/accounts', 'AccountController');
Route::post('data/accounts', 'AccountController@getData');

Route::resource('/categories', 'CategoryController');
Route::post('data/categories', 'CategoryController@getData');
Route::put('property/categories/{id}', 'CategoryController@editProperty');
Route::delete('property/categories/{id}', 'CategoryController@deleteProperty');
Route::resource('options', 'OptionsController');

Route::resource('/stores', 'StoreController');
Route::post('data/stores', 'StoreController@getData');

Route::resource('/website_pages', 'WebsitePageController');
Route::post('data/website_pages', 'WebsitePageController@getData');

Route::resource('/brands', 'BrandController');
Route::post('data/brands', 'BrandController@getData');

Route::resource('/blocks', 'BlockController');
Route::post('data/blocks', 'BlockController@getData');


Route::resource('/sliders', 'SliderController');
Route::post('data/sliders', 'SliderController@getData');

Route::resource('/balances', 'BalanceController');
Route::post('data/balances', 'BalanceController@getData');


Route::get('/settings', 'DashboardController@getSettings')->name('getSettings');
Route::post('/post-settings', 'DashboardController@postSettings')->name('postSettings');

Route::get('mobile/settings', 'DashboardController@mobileSettings');
Route::post('mobile/post-settings', 'DashboardController@postMobileSettings');


Route::get('/logs', 'DashboardController@getLogs')->name('getLogs');
Route::post('data/postLogs', 'DashboardController@postLogs')->name('postLogs');


Route::resource('/packages', 'PackageController');
Route::post('data/packages', 'PackageController@getData');
Route::resource('/package_types', 'PackagetypeController');

Route::resource('/offers', 'OfferController');
Route::post('data/offers', 'OfferController@getData');


Route::resource('/terms', 'TermsController');
Route::post('data/terms', 'TermsController@getData');


Route::resource('/products', 'ProductController');
Route::get('properties/{cat_id}', 'ProductController@categoryProperties');
Route::post('data/products', 'ProductController@getData');
Route::delete('images/products/{image_id}', 'ProductController@deleteImage');

Route::get('cities_country/{user_id}', 'UserController@cities');
Route::get('stores_seller/{user_id}', 'ProductController@stores');
Route::get('category_brands/{category_id}', 'ProductController@brands');

#--------------------------------------------mobile Apis --------------------
Route::resource('/apis', 'ApiController');
Route::post('/update-apis', 'ApiController@updateApis');

Route::resource('/mobile/slider', 'TextSliderController');
Route::post('data/mobile/slider', 'TextSliderController@getData');


Route::resource('/orders', 'OrderController');
Route::post('data/orders', 'OrderController@getData');



#---------------------------------------------------------------------------------------------------------
Route::get('/401','DashboardController@unauthorized');


Route::fallback(function(){
    $local=Request::segment(1);
    return view('admin.errors.404',compact('local'));
});

