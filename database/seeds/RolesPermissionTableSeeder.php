<?php

use Illuminate\Database\Seeder;
use App\Models\RolePermission;
class RolesPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p=new RolePermission();
        $p->role_id=1;
        $p->permission_id=1;
        $p->save();

        $p=new RolePermission();
        $p->role_id=1;
        $p->permission_id=2;
        $p->save();
    }
}
