<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Models\Settings();
        $role->about_us = 'admin';
        $role->email = 'administrator';
        $role->save();
    }
}
