<?php

use Illuminate\Database\Seeder;
use App\Models\Api;

class ApisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = new Api();
        $pages->en_name = 'Registration';
        $pages->ar_name = 'تسجيل الاشتراك';
        $pages->method = 'POST';
        $pages->path = '/register';
        $pages->en_desc = 'User make register in our application';
        $pages->ar_desc = 'تسجيل المستخد للاشتراك';
        $pages->required_parameters = 'name-email-password-country_code-mobile';
        $pages->parameters = 'lang-image-bg-address-lat-long-birth_date-facebook-twitter-google-gender';
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Login';
        $pages->ar_name = 'تسجيل الدخول';
        $pages->method = 'POST';
        $pages->path = '/login';
        $pages->en_desc = 'User make Login in our application';
        $pages->ar_desc = 'تسجيل الدخول';
        $pages->required_parameters = 'email(can be email or mobile)-password';
        $pages->parameters = 'lang';
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Social Login';
        $pages->ar_name = 'تسجيل الدخول بمواقع التواصل';
        $pages->method = 'POST';
        $pages->path = '/social-login';
        $pages->en_desc = 'User make Login with (facebook or google) in our application';
        $pages->ar_desc = 'تسجيل دخول المستخدم باستجدام جوجل او الفيس بوك';
        $pages->required_parameters = 'provider(can be "facebook" or "google") - provider_id';
        $pages->parameters = 'lang-provider_image (If login with google) ';
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Account activation';
        $pages->ar_name = 'تفعيل الحساب';
        $pages->method = 'POST';
        $pages->path = '/active-account';
        $pages->en_desc = 'User confirm his mobile number by code that send to him as sms';
        $pages->ar_desc = 'تاكيد ملكيه رقم الهاتف ';
        $pages->required_parameters = 'email(can be email or mobile)-verification_code';
        $pages->parameters = 'lang';
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();

        $pages = new Api();
        $pages->en_name = 'User profile ';
        $pages->ar_name = 'صفحه المستخدم';
        $pages->method = 'POST';
        $pages->path = '/profile';
        $pages->en_desc = 'User profile ';
        $pages->ar_desc = 'صفحة المستخدم الشخصية';
        $pages->required_parameters = 'user_id';
        $pages->parameters = 'lang';
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Update profile ';
        $pages->ar_name = 'تعديل الحساب';
        $pages->method = 'POST';
        $pages->path = '/profile/update';
        $pages->en_desc = 'Update Profile';
        $pages->ar_desc = 'تعديل بيانات الحساب الخاصه بالمستخدم';
        $pages->required_parameters = 'user_id';
        $pages->parameters = "lang-name-email-password-country_code-mobile-,'image','bg','address','lat','long','facebook','twitter','instagram','google','gender',";
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Countries';
        $pages->ar_name = 'البلدان';
        $pages->method = 'GET';
        $pages->path = '/countries';
        $pages->en_desc = 'List of countries';
        $pages->ar_desc = 'قائمه البلدان';
        $pages->required_parameters = 'user_id';
        $pages->parameters = "lang";
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();

        $pages = new Api();
        $pages->en_name = 'Categories';
        $pages->ar_name = 'الاقسام';
        $pages->method = 'GET';
        $pages->path = '/categories';
        $pages->en_desc = 'List of Parent categories with 2 level children';
        $pages->ar_desc = 'قائمه بالاقسام الرئيسية والفرعية';
        $pages->required_parameters = '';
        $pages->parameters = "lang";
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


        $pages = new Api();
        $pages->en_name = 'Home Categories';
        $pages->ar_name = 'الاقسام الرئيسيه';
        $pages->method = 'GET';
        $pages->path = '/home-categories';
        $pages->en_desc = 'List of Parent categories with 1 with his products to 3 level';
        $pages->ar_desc = 'قائمة بالاقسام الرئيسيه بمنتجاتها';
        $pages->required_parameters = '';
        $pages->parameters = "lang";
        $pages->and_is_active = 1;
        $pages->ios_is_active = 1;
        $pages->save();


    }
}
