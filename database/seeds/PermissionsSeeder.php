<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Permission = new Permission();
        $Permission->name = 'اضافه بلد';
        $Permission->key='add_country';
        $Permission->page_id=2;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل بلد';
        $Permission->key='update_country';
        $Permission->page_id=2;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح بلد';
        $Permission->key='delete_country';
        $Permission->page_id=2;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه البلدان';
        $Permission->key='list_country';
        $Permission->page_id=2;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل البلدان';
        $Permission->key='show_country';
        $Permission->page_id=2;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه مدينه';
        $Permission->key='add_city';
        $Permission->page_id=1;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل مدينه';
        $Permission->key='update_city';
        $Permission->page_id=1;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح مدينه';
        $Permission->key='delete_city';
        $Permission->page_id=1;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه المدن';
        $Permission->key='list_city';
        $Permission->page_id=1;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل المدن';
        $Permission->key='show_city';
        $Permission->page_id=1;
        $Permission->permission_type_id=4;
        $Permission->save();




        $Permission = new Permission();
        $Permission->name = 'اضافه المديرين';
        $Permission->key='add_admin';
        $Permission->page_id=3;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل المديرين';
        $Permission->key='update_admin';
        $Permission->page_id=3;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح المديرين';
        $Permission->key='delete_admin';
        $Permission->page_id=3;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه المديرين';
        $Permission->key='list_admin';
        $Permission->page_id=3;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل المديرين';
        $Permission->key='show_admin';
        $Permission->page_id=3;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه المستخدمين';
        $Permission->key='add_user';
        $Permission->page_id=4;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل المستخدمين';
        $Permission->key='update_user';
        $Permission->page_id=4;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح المستخدمين';
        $Permission->key='delete_user';
        $Permission->page_id=4;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه المستخدمين';
        $Permission->key='list_user';
        $Permission->page_id=4;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل المستخدمين';
        $Permission->key='show_user';
        $Permission->page_id=4;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'اضافه الصلاحيات';
        $Permission->key='add_role';
        $Permission->page_id=5;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل الصلاحيات';
        $Permission->key='update_role';
        $Permission->page_id=5;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح الصلاحيات';
        $Permission->key='delete_role';
        $Permission->page_id=5;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه الصلاحيات';
        $Permission->key='list_role';
        $Permission->page_id=5;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل الصلاحيات';
        $Permission->key='show_role';
        $Permission->page_id=5;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'اضافه حساب';
        $Permission->key='add_account';
        $Permission->page_id=6;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل حساب';
        $Permission->key='update_account';
        $Permission->page_id=6;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح حساب';
        $Permission->key='delete_account';
        $Permission->page_id=6;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه حساب';
        $Permission->key='list_account';
        $Permission->page_id=6;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل حساب';
        $Permission->key='show_account';
        $Permission->page_id=6;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه صنف';
        $Permission->key='add_category';
        $Permission->page_id=7;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل صنف';
        $Permission->key='update_category';
        $Permission->page_id=7;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح صنف';
        $Permission->key='delete_category';
        $Permission->page_id=7;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه صنف';
        $Permission->key='list_category';
        $Permission->page_id=7;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل صنف';
        $Permission->key='show_category';
        $Permission->page_id=7;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه صنف';
        $Permission->key='add_store';
        $Permission->page_id=8;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل متجر';
        $Permission->key='update_store';
        $Permission->page_id=8;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح متجر';
        $Permission->key='delete_store';
        $Permission->page_id=8;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه متجر';
        $Permission->key='list_store';
        $Permission->page_id=8;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تفاصيل متجر';
        $Permission->key='show_store';
        $Permission->page_id=8;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'اضافه السلايدر';
        $Permission->key='add_slider';
        $Permission->page_id=10;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل السلايدر';
        $Permission->key='update_slider';
        $Permission->page_id=10;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح السلايدر';
        $Permission->key='delete_slider';
        $Permission->page_id=10;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه السلايدر';
        $Permission->key='list_slider';
        $Permission->page_id=10;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل  السلايدر';
        $Permission->key='show_slider';
        $Permission->page_id=10;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'قائمه لوج';
        $Permission->key='list_log';
        $Permission->page_id=11;
        $Permission->permission_type_id=5;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه المنتج';
        $Permission->key='add_product';
        $Permission->page_id=12;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل المنتج';
        $Permission->key='update_product';
        $Permission->page_id=12;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح المنتج';
        $Permission->key='delete_product';
        $Permission->page_id=12;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه المنتج';
        $Permission->key='list_product';
        $Permission->page_id=12;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل  المنتج';
        $Permission->key='show_product';
        $Permission->page_id=12;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه رصيد';
        $Permission->key='add_balance';
        $Permission->page_id=13;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل رصيد';
        $Permission->key='update_balance';
        $Permission->page_id=13;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح رصيد';
        $Permission->key='delete_balance';
        $Permission->page_id=13;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه رصيد';
        $Permission->key='list_balance';
        $Permission->page_id=13;
        $Permission->permission_type_id=5;
        $Permission->save();
        $Permission = new Permission();
        $Permission->name = 'تفاصيل  رصيد';
        $Permission->key='show_balance';
        $Permission->page_id=13;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'اضافه حزمه';
        $Permission->key='add_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل حزمه';
        $Permission->key='update_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح حزمه';
        $Permission->key='delete_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=3;
        $Permission->save();$Permission = new Permission();
        $Permission->name = 'اضافه حزمه';
        $Permission->key='add_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل حزمه';
        $Permission->key='update_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح حزمه';
        $Permission->key='delete_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه حزم';
        $Permission->key='list_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تفاصيل  حزمه';
        $Permission->key='show_package';
        $Permission->page_id=14;
        $Permission->permission_type_id=4;
        $Permission->save();



        $Permission = new Permission();
        $Permission->name = 'اضافه العرض';
        $Permission->key='add_offer';
        $Permission->page_id=20;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل العرض';
        $Permission->key='update_offer';
        $Permission->page_id=20;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح العرض';
        $Permission->key='delete_offer';
        $Permission->page_id=20;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه العروض';
        $Permission->key='list_offer';
        $Permission->page_id=20;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تفاصيل  العرض';
        $Permission->key='show_offer';
        $Permission->page_id=20;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'اضافه ماركة';
        $Permission->key='add_brand';
        $Permission->page_id=15;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل ماركة';
        $Permission->key='update_brand';
        $Permission->page_id=15;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح ماركة';
        $Permission->key='delete_brand';
        $Permission->page_id=15;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه الماركات';
        $Permission->key='list_brand';
        $Permission->page_id=15;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تفاصيل  براند';
        $Permission->key='show_brand';
        $Permission->page_id=15;
        $Permission->permission_type_id=4;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = ' الاعدادات';
        $Permission->key='update_setting';
        $Permission->page_id=9;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'اضافه صفحات الموقع';
        $Permission->key='add_web_page';
        $Permission->page_id=16;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تعديل صفحات الموقع';
        $Permission->key='update_web_page';
        $Permission->page_id=16;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'مسح صفحات الموقع';
        $Permission->key='delete_web_page';
        $Permission->page_id=16;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'قائمه صفحات الموقع';
        $Permission->key='list_web_page';
        $Permission->page_id=16;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'تفاصيل  صفحات الموقع';
        $Permission->key='show_web_page';
        $Permission->page_id=16;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = ' الاعدادات moble';
        $Permission->key='update_setting_mobile';
        $Permission->page_id=17;
        $Permission->permission_type_id=2;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'text slider';
        $Permission->key='add_text_slider';
        $Permission->page_id=19;
        $Permission->permission_type_id=1;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'text slider';
        $Permission->key='update_text_slider';
        $Permission->page_id=19;
        $Permission->permission_type_id=2;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name ='text slider';
        $Permission->key='delete_text_slider';
        $Permission->page_id=19;
        $Permission->permission_type_id=3;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'text slider';
        $Permission->key='list_text_slider';
        $Permission->page_id=19;
        $Permission->permission_type_id=5;
        $Permission->save();

        $Permission = new Permission();
        $Permission->name = 'text slider';
        $Permission->key='show_text_slider';
        $Permission->page_id=19;
        $Permission->permission_type_id=4;
        $Permission->save();


        $Permission = new Permission();
        $Permission->name = 'api';
        $Permission->key='update_api';
        $Permission->page_id=18;
        $Permission->permission_type_id=2;
        $Permission->save();



    }

}
