<?php

use Illuminate\Database\Seeder;
use App\Models\PropertyType;
class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new=new PropertyType(); //1
        $new->en_name="list";
        $new->ar_name="قائمة خيارات";
        $new->save();

        $new=new PropertyType(); // 2
        $new->en_name="Text";
        $new->ar_name="مقاليه";
        $new->save();

        $new=new PropertyType(); // 3
        $new->en_name="Number";
        $new->ar_name="رقم";
        $new->save();

        $new=new PropertyType(); // 4
        $new->en_name="date";
        $new->ar_name="تاريخ";
        $new->save();


    }
}
