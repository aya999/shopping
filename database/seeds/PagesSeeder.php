<?php

use Illuminate\Database\Seeder;
use App\Models\Pages;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = new Pages();
        $pages->id = '1';
        $pages->name = 'City';
        $pages->save();

        $pages = new Pages();
        $pages->id = '2';
        $pages->name = 'Country';
        $pages->save();

        $pages = new Pages();
        $pages->id = '3';
        $pages->name = 'Admin';
        $pages->save();

        $pages = new Pages();
        $pages->id = '4';
        $pages->name = 'User';
        $pages->save();

        $pages = new Pages();
        $pages->id = '5';
        $pages->name = 'Roles';
        $pages->save();

        $pages = new Pages();
        $pages->id = '6';
        $pages->name = 'Account';
        $pages->save();

        $pages = new Pages();
        $pages->id = '7';
        $pages->name = 'Category';
        $pages->save();

        $pages = new Pages();
        $pages->id = '8';
        $pages->name = 'Store';
        $pages->save();

        $pages = new Pages();
        $pages->id = '9';
        $pages->name = 'Setting';
        $pages->save();

        $pages = new Pages();
        $pages->id = '10';
        $pages->name = 'Slider';
        $pages->save();

        $pages = new Pages();
        $pages->id = '11';
        $pages->name = 'Log';
        $pages->save();

        $pages = new Pages();
        $pages->id = '12';
        $pages->name = 'Product';
        $pages->save();

        $pages = new Pages();
        $pages->id = '13';
        $pages->name = 'Balance';
        $pages->save();

        $pages = new Pages();
        $pages->id = '14';
        $pages->name = 'Package';
        $pages->save();

        $pages = new Pages();
        $pages->id = '15';
        $pages->name = 'Brand';
        $pages->save();

        $pages = new Pages();
        $pages->id = '16';
        $pages->name = 'WebsitePage';
        $pages->save();


        $pages = new Pages();
        $pages->id = '17';
        $pages->name = 'Mobile Settings';
        $pages->save();

        $pages = new Pages();
        $pages->id = '18';
        $pages->name = 'Mobile Api';
        $pages->save();


        $pages = new Pages();
        $pages->id = '19';
        $pages->name = 'Mobile Tutorial';
        $pages->save();


        $pages = new Pages();
        $pages->id = '20';
        $pages->name = 'Offers';
        $pages->save();
    }
}
