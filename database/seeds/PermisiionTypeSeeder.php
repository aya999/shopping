<?php

use Illuminate\Database\Seeder;
use App\Models\PermissionType;

class PermisiionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PermissionType = new PermissionType();
        $PermissionType->id = 1;
        $PermissionType->name = 'ADD';
         $PermissionType->save();

        $PermissionType = new PermissionType();
        $PermissionType->id = 2;
        $PermissionType->name = 'Update';
        $PermissionType->save();

        $PermissionType = new PermissionType();
        $PermissionType->id = 3;
        $PermissionType->name = 'Delete';
        $PermissionType->save();

        $PermissionType = new PermissionType();
        $PermissionType->id = 4;
        $PermissionType->name = 'Show';

        $PermissionType->save();
        $PermissionType = new PermissionType();
        $PermissionType->id = 5;

        $PermissionType->name = 'List';
        $PermissionType->save();
    }
}
