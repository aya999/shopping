<?php

use Illuminate\Database\Seeder;
use App\Admin;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=new \App\Company();
        $admin->name="Shipment Comp.";
        $admin->email="comp@dalili.com";
        $admin->password="password";
        $admin->mobile="1111";
        $admin->is_active=1;
        $admin->country_id=1;
        $admin->save();
    }
}