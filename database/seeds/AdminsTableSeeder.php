<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=new Admin();
        $admin->name="Dalili Shop Admin";
        $admin->email="admin@dalili.com";
        $admin->password="password";
        $admin->mobile="1111";
        $admin->role_id=1;
        $admin->save();
    }
}
