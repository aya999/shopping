<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p=new Permission();
        $p->name="List Admin";
        $p->key="list_admin";
        $p->save();

        $p=new Permission();
        $p->name="Add Admin";
        $p->key="add_admin";
        $p->save();


        $p=new Permission();
        $p->name="Edit Admin";
        $p->key="edit_admin";
        $p->save();


        $p=new Permission();
        $p->name="Delete Admin";
        $p->key="delete_admin";
        $p->save();
    }
}
